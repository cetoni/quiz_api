#!/bin/bash
set -x

echo "taking backup"
docker exec -i quiz_api python manage.py dbbackup
docker exec -i quiz_api python manage.py mediabackup

echo "copying dbbackup and media to server"
docker cp quiz_api:/src/var/backups/ /root/quiz_api_prod

echo "pushing backup file to space"
s3cmd put -r quiz_api_prod s3://daphne-backups

echo "remove files from droplet after backup"
rm -rf quiz_api_prod/backups
rm -rf quiz_api/src/var/backups/*

# Add to crontab
# * 0 * * * ./quiz_api/tools/db_backup.sh > ./quiz_api/tools/logs/db_backup.log 2>&1
