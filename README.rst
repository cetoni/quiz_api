===============================
Quiz
===============================

Quiz website

* Local development setup

     #. create a env folder inside the project with files (local.env , cms.env , db.env , api.env)
        ::

            mkdir env && cd env && touch local.env && touch cms.env && touch db.env  && touch api.env
            (or create manually)

     #. local.env file contents
        ::

            ALLOWED_HOSTS=['*']
            DATABASE_HOST=db
            DATABASE_NAME=database_name
            DATABASE_PASSWORD=database_password
            DATABASE_PORT=database_port
            DATABASE_USER=database_user
            MODE=DEV 

     #. cms.env file contents
        ::

            ALLOWED_HOSTS=['*']
            DATABASE_HOST=db
            DATABASE_NAME=database_name
            DATABASE_PASSWORD=database_password
            DATABASE_PORT=database_port
            DATABASE_USER=database_user
            MODE=DEV 
     #. api.env file contents
        ::

            ALLOWED_HOSTS=['*']
            DATABASE_HOST=db
            DATABASE_NAME=database_name
            DATABASE_PASSWORD=database_password
            DATABASE_PORT=database_port
            DATABASE_USER=database_user
            MODE=DEV 
     #. db.env file contents
         ::

            MYSQL_ROOT_PASSWORD=root_password
            MYSQL_DATABASE=database_name
            MYSQL_USER=database_user
            MYSQL_PASSWORD=user_password

     #. Build the Docker files
        ::

            docker-compose -f dev.yml build 
    
     #. Run Containers
        ::

            docker-compose -f dev.yml up 
     #. Stop Containers  
        ::

            docker-compose -f dev.yml down
   
    

* Creating and Restoring Database Backup

     #. after getting into the droplet execute command
        ::

            docker exec -it django_quiz sh

     #. To create database backup file execute command
        ::

            python manage.py dbbackup

     #. To restore the database execute command
        ::

            python manage.py dbrestore


===============================
Migration - Mapping
===============================
Answer choices is always {"1": "V", "2": "F"}

CQC
quiz.questions.chapter_id
chapter_id 1 -> questioncategorymodel/12
chapter_id 2 -> questioncategorymodel/13
chapter_id 3 -> questioncategorymodel/14
chapter_id 4 -> questioncategorymodel/15
chapter_id 5 -> questioncategorymodel/16
chapter_id 6 -> questioncategorymodel/17
chapter_id 7 -> questioncategorymodel/18
chapter_id 8 -> questioncategorymodel/19
chapter_id 9 -> questioncategorymodel/20
chapter_id 10 -> questioncategorymodel/21
chapter_id 11 -> questioncategorymodel/22
chapter_id 12 -> questioncategorymodel/23
chapter_id 13 -> questioncategorymodel/24
chapter_id 14 -> questioncategorymodel/25
chapter_id 15 -> questioncategorymodel/26
chapter_id 16 -> questioncategorymodel/27

text -> Question

answer ->

===============================
setup env - Deployment
===============================

Process to run quiz_api

* Please clone this repository first and follow the readme with attention, Not just 
  copy and paste of commands

 
   ::
      
      https://bitbucket.org/Majid-Rehman/nginx-proxy/src/master/



* After this, Run the mysql db container above or equal to 5.7

   ::

    docker run -d -p 3309:3306 -e MYSQL_USER=user -e MYSQL_PASSWORD=password -e 
    MYSQL_DATABASE=db-name -e MYSQL_ALLOW_EMPTY_PASSWORD="yes" mysql:5.7


* And than go to **quiz_api** directory and run 

   ::
    
    docker-compose up -d


 Here your app is up and running 

* Make sure there is virtual host environmet in compose file with value 
  **api.editricetoni.it**.

you don't need to change this just add this url in your `/etc/hosts` like this 

::
   
   192.168.0.9 api.editricetoni.it


* Go to your browser and type **api.editricetoni.it**

* Calendar spa has to be built before (npm run-script build inside templates/calendar_spa folder) before deploying

===============================
Adaptive quiz
===============================

Question partitioning (table in markdown syntax)

| Level |  Interval of correct %    |  Points for right answer |  Points for wrong answer |
|-------|---------------------------|--------------------------|--------------------------|
| 1     | [100,100/Z*(Z-1))         | 1                        | -Z                       |
| 2     | [100/Z*(Z-1),100/Z*(Z-2)) | 2                        | -Z+1                     |
| ...   | ...                       | ...                      | ...                      |
| Z     | [100/Z,0]                 | Z                        | -1                       |

Questions sequence (mermaid)
To be copy pasted to https://mermaid-js.github.io/mermaid-live-editor/#/

graph LR
subgraph App config 
    id1[(NB of partitions Z 3<br/>Answer horizon h 90<br/>Starting difficulty level?<br/>Stability coefficient S 3<br/>User level span U 2)]
end
	A[Start] --> C{First question<br/>Starting level?}
	C -->|Right answer<br/>Add points<br/>for the level| D[Question]
	C -->|Wrong answer<br/>Subtract points<br/>for the level| E[Question]

  D[Question for<br/>the new level] -->|Right answer<br/>Add points<br/>for the level| F[Question for<br/>the new level]
	D[Question for<br/>the new level] -->|Wrong answer<br/>Subtract points<br/>for the level| G[Question for<br/>the new level]

  E[Question for<br/>the new level] -->|Right answer<br/>Add points<br/>for the level| H[Question for<br/>the new level]
  E[Question for<br/>the new level] -->|Wrong answer<br/>Subtract points<br/>for the level| I[Question for<br/>the new level]
  
  
User level (table in markdown syntax)

|  Level |  Answer points intervals |
|--------|--------------------------|
| 1      | [inf,U))                 |
| 2      | [U,0)                    |
| 3      | [0,-U)                   |
| 4      | [-U,-inf]                |

==================================================
Implicit Oauth authentication (using Google OAuth)
==================================================

https://sequencediagram.org/index.html?presentationMode=readOnly#initialData=A4QwTgLglgxloDsIAICqBnApmAUKSs8ISyAigK5QBeyAygAoCCe40ciKA4gPbcDmAG0zJG9AJIsC7YigrUR4nEpwgY0AG4gIwnvyEKJAE0yqNWnb0HDRE6BH26ryAPKNyEABbIQ7j5iSwWlDcCMgAZgLcAO7KShjYALQAfHI0DIwAXDACsADWyCHe6OjccObIfJb6qjDc5Eg4qXRMyY76NhkASpgAjuSY6CgQ3MjkWBVVwtw+ngA6CFgIhujI2VD+EAD6UIYqAEbcAB7IYFB8HijcYcht1uIZAGrYUGGB0CHzTenIUCtQoTMPNxTlRMIZkIZuABbED-ebEcEeEArEDITQ5cFrDbbQzzRqUNJMAA8CVuBgyZLAmGAISw80Wy28MBgAxWw1y-hw8TAJK+TAylT0wkifH+yGAYGhwAgXKwYFak3J3OQIr+CAyyEwMKgAm8S3FyPQUWBu32RxOZwuBWuZI6T1OrxgQQ+CDJ6merwGozlqypxgCIAE6HmWthAgA9KBisawLs+YwSbb7pSBjSFph6f5Gez-HrMTlsTt5gAKHOhJErdC1YDCDXzBKa7UCevhKBgQbIBAgKEZhANgTIlBdnvzACUOHQoBZ+Pk6WSTQ69G4HapfQGQxGfXkIGAUEzSzZ3A5AP1WKQOJLZeQFeQVe4NeQdb7jbDLde7aH3d7-cHna-Y4nKdMDNY5TnOS5rgXe5UGAQxykFKxNibVtMAEOMCWaBMEigsQMmcfIqUnWlgO5XkMPSDIgR7A0+EwIA