from django import template

register = template.Library()


@register.filter
def get_page_data(paginator, page_no):
    page = paginator.get_page(page_no)
    return page.object_list
