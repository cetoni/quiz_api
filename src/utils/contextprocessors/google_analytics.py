from django.conf import settings
from django.http import HttpRequest

def get_google_analytics_key(HttpRequest):
    return dict(
        GOOGLE_ANALYTICS_KEY=settings.GOOGLE_ANALYTICS_KEY
    )