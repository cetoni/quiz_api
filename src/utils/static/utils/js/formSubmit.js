function showSnackbar(message) {
    var x = document.getElementById("snackbar");
    if (!x){
        x = document.createElement('div');
    }
    x.innerText = message;
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

$(document).ready(function () {
    $("form").submit(function (e) {
        e.preventDefault();
        var form = $('form');
        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            method: 'POST',
            success: function (response) {
                showSnackbar(response.message);
                form.trigger("reset");
            },
            error: function (response) {
                    showSnackbar(response.responseJSON.message);
            },

        });
    });
});
