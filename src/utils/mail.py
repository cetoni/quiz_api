from constance import config
from mailjet_rest import Client

mailjet = Client(auth=(config.MAILJET_API_KEY, config.MAILJET_SECRET), version='v3.1')