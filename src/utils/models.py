import time
from datetime import datetime, date

from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

GENDER_CHOICE = (('M', _('Male')), ('F', _('Female')), ('O', _('Other')),)

DAY_OF_WEEK_CHOICE = (
    (1, _('Monday')),
    (2, _('Tuesday')),
    (3, _('Wednesday')),
    (4, _('Thursday')),
    (5, _('Friday')),
    (6, _('Saturday')),
    (7, _('Sunday')),
)


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TitleDescription(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return " id-{}, {}".format(self.id, self.title)


class ProfileModel(models.Model):
    first_name = models.CharField(_("First Name"), max_length=50)
    last_name = models.CharField(_("Last Name"), max_length=50)
    email = models.EmailField(_("Email"), blank=True)
    gender = models.CharField(_("Gender"), max_length=10, choices=GENDER_CHOICE)
    dob = models.DateField(_("DOB"), )
    phone_no = models.CharField(_("Contact No"), max_length=20, blank=True, null=True)
    image = models.ImageField(_("Image"), )
    religion = models.CharField(_("Religion"), max_length=50)
    date_joined = models.DateField(_("Date Joined"), )
    date_left = models.DateField(_("Date Left"), blank=True, null=True)

    class Meta:
        abstract = True

    def get_image_url(self):
        return self.image.url if self.image else None


class ContactInfo(models.Model):
    contact_1 = models.CharField(_("Contact No."), max_length=50)
    contact_2 = models.CharField(_("Alternate Contact"), max_length=50, blank=True, null=True)

    address_1 = models.TextField(_("Permanent Address"), )
    address_2 = models.TextField(_("Current Address"), blank=True, null=True)

    country = models.CharField(_("Country"), max_length=50)
    state = models.CharField(_("State"), max_length=50)
    postal_code = models.PositiveIntegerField(_("Postal Code"), )

    class Meta:
        abstract = True

    @property
    def address(self):
        return self.address_2 if self.address_2 else self.address_1


class AbstractModel(models.Model):
    image = models.ImageField()
    title = models.CharField(max_length=30)
    description = models.TextField()

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def get_image_url(self):
        return self.image.url if self.image else None


class PublishingModelManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(publish=True)


class PublishModel(models.Model):
    publish = models.BooleanField(default=True)
    objects = PublishingModelManager

    class Meta:
        abstract = True


def get_model_simple_fields(model):
    return [i.name for i in model._meta.fields if not isinstance(i, models.FileField)]
