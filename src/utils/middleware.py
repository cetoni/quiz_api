import os
from constance import config

from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.conf import settings

from cms.utils.page import get_page_template_from_request
from utils.mail import mailjet
from user.models import SiteProfile

User = get_user_model()
class SiteMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            current_site = Site.objects.get(domain=request.get_host())
        except Site.DoesNotExist:
            not_found = request.get_host()
            message = {
                "Messages": [{
                    "From": {
                        "Email": config.EMAIL_SENDER,
                        "Name": config.EMAIL_SENDER_NAME
                    },
                    "To": [{
                        "Email": "software@editricetoni.it",
                    }],
                    "Cc": [{
                        "Email": "moad.dtr@gmail.com",
                    }],
                    "Subject": "Site not found alert !",
                    "TextPart": f"""Site {not_found} was not found in the database, 
                                this site may have been added as a new dns and was not added in Django Admin 
                                {os.environ.get('MODE')}
                    """
                }]
            }
            # result = mailjet.send.create(data=message)
            # print(result)
            current_site = Site.objects.first()

        request.current_site = current_site
        settings.SITE_ID = current_site.id

        return self.get_response(request)

class SiteAccess:
    """
        Check if the user cms page PagePermissions to access admin
    """
    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request):
        current_site = get_current_site(request)
        user = request.user 
        
        if request.method == 'POST' and (request.path == reverse('admin:login')):
            site_id = current_site.id
            try:
                user = User.objects.get(email=request.POST.get('username'))
                if user.is_superuser:
                    return self.get_response(request)
                SiteProfile.objects.get(user_id=user.id, site_id=site_id)
                response = self.get_response(request)
            except SiteProfile.DoesNotExist as e:
                messages.error(request, "You are not allowed to login as you are not associated with this domain")
                return HttpResponseRedirect(reverse('admin:login'))
            except User.DoesNotExist as e:
                messages.error(request, "Invalid credentials")
                return HttpResponseRedirect(reverse('admin:login'))

        return self.get_response(request)

class CmsAuth:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        print(request.data)
        page = get_page_template_from_request(request)
        if page == "_cms/video.html" and not request.user.is_authenticated:
            return HttpResponseRedirect('/login')
        return self.get_response(request)
