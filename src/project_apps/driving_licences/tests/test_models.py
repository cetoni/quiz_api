from django.test import TestCase

from user.models import User
from project_apps.driving_licences.models import DrivingLicence, DrivingLicenceType

class TestModels(TestCase):
    """Test Driving licence and driving licence types methods"""
    
    def setUp(self):
        """Set up user"""
        self.user = User(username='userfoo', password='userfoo')
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_user_can_have_many_licences(self):
        dl_A1 = DrivingLicenceType.objects.get(description='A1')
        dl_B = DrivingLicenceType.objects.get(description='B')

        for dl in [dl_A1, dl_B]:
            self.user.driving_licence.create(dl_type=dl)

        # Order of items is important, test would fail if the order is not maintained
        self.assertEqual([dl_A1.description, dl_B.description], [d.dl_type.description for d in self.user.driving_licence.all()])

