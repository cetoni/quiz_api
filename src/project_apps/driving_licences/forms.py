from django import forms

from project_apps.driving_licences.models import DrivingLicenceType, DrivingLicence

class DrivingLicenceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['dl_type'] = forms.ModelChoiceField(queryset=DrivingLicenceType.objects.all())

    class Meta:
        model = DrivingLicence
        fields = ('dl_type',)