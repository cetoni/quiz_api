from django.contrib import admin

from .models import DrivingLicenceType

admin.site.register(DrivingLicenceType)