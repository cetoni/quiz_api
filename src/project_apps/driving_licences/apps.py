from django.apps import AppConfig

class DrivingLicenceConfig(AppConfig):
    """Configuration class for DrivingLicences app"""
    name = 'driving_licences'