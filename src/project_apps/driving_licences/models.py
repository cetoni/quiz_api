from django.db import models

from user.models import User

class DrivingLicenceType(models.Model):
    """
        Driving Licence Types

        Attributes:
            id : (int) unique identifier
            description: (string) driving licence description
    """
    description = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return f"Patente {self.description}"

class DrivingLicence(models.Model):
    """
        Driving Licence

        Attributes:
            id: (int) unique identifier
            dl_type: (string) driving licence type
    """
    dl_type = models.ForeignKey(DrivingLicenceType, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='driving_licence')
    expiration_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return f"{self.dl_type.description}"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'dl_type'], name='user_driving_licence_type_unique')
        ]