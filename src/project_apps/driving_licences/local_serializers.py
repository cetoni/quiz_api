
from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import DrivingLicence

User = get_user_model()

class DrivingLicenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrivingLicence
        exclude = ('user',)