from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError

from user.models import User

from project_apps.quiz_data.models import QuizModel
from project_apps.lessons_files import models as media
from project_apps.driving_lessons import models as lessons
from project_apps.adaptive_quiz import models as adaptive

apps = [(app.replace('project_apps.', ''), app.replace('project_apps.', '')) for app in settings.CUSTOM_APPS]
# quizzes = [(q.title, q.id) for q in QuizModel.objects.all()]
quizzes = []

class Service(models.Model):
    name = models.CharField(verbose_name=('Service name'), unique=True, max_length=200)
    app = models.CharField(verbose_name=('Module name'), choices=apps, max_length=100, default='quiz_data')
    instance = models.CharField(verbose_name=('Instance'), choices=quizzes, unique=True, max_length=200, null=True, blank=True)

    def __str__(self):
        return f"<Service {self.id} {self.name}>"

    def clean(self):
        quizzes = [q.title for q in QuizModel.objects.all()]
        if self.app == 'quiz_data' or self.app == 'adaptive_quiz':
            if self.instance not in quizzes or not self.instance:
                raise ValidationError({'instance':'The instance must be an instance of quiz'})
        elif self.instance:
            raise ValidationError({'instance': 'Support only quiz instances'})
        else:
            return True

class Subscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='subscriptions')
    service = models.ForeignKey(Service, on_delete=models.SET_NULL, null=True, blank=True, related_name='subscriptions')

    def __str__(self):
        return f"<Subscripton {self.id}> {self.user} to {self.service}"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'service'], name='unique_subscription')
        ]
