from django.urls import resolve
from rest_framework.permissions import BasePermission

from project_apps.services_handler.models import Subscription

class HasSubscription(BasePermission):
    """
        Checks if the user is subscribed to the service
    """
    def get_subscriptions(self, request, app):
        user = request.user
        subscriptions = user.subscriptions.filter(service__app=app)
        return subscriptions
        

    def has_permission(self, request, view):
        resolver = resolve(request.path)
        app = resolver.app_name
        user = request.user
        subscriptions = self.get_subscriptions(request, app)
        if not subscriptions:
            return False
        return True

    def has_object_permission(self, request, view, obj):
        user = request.user
        print(obj)
        subscriptions = self.get_subscriptions(request)
        print(subscriptions)
        