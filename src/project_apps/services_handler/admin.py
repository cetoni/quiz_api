from django.contrib import admin

from project_apps.services_handler.models import Service, Subscription

class ServiceAdmin(admin.ModelAdmin):
    pass

class SubscriptionAdmin(admin.ModelAdmin):
    search_fields = ('user',)
    autocomplete_fields = ('user',)

admin.site.register(Service)
admin.site.register(Subscription, SubscriptionAdmin)