from itertools import chain
from rest_framework.views import APIView, Response

from user.models import User
from project_apps.services_handler.serializers import SubscriptionSerializer

class SubscriptionList(APIView):
    """
        Subscriptions views
        
        * Requires authentication
    """
    def get(self, request, format=None):
        """Get user's subscriptions (list)"""
        user = request.user
        parent = request.user.parent
        subscriptions = user.subscriptions.all()
        p_subscriptions = parent.subscriptions.all()
        
        all_subscriptions = list(chain(subscriptions, p_subscriptions))
        serializer = SubscriptionSerializer(all_subscriptions, many=True)
        return Response(dict(user=user.id, subscriptions=serializer.data))