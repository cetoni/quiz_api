from rest_framework import serializers

from project_apps.quiz_data.models import QuizModel
from project_apps.services_handler.models import Subscription, Service

class SubscriptionSerializer(serializers.ModelSerializer):
    reference_id = serializers.SerializerMethodField()
    reference_type = serializers.SerializerMethodField()

    class Meta:
        model = Subscription
        fields = ['reference_id', 'reference_type']
    
    def get_reference_id(self, obj):
        service = obj.service
        if service.app == 'quiz_data':
            ref = QuizModel.objects.get(title=obj.service.instance)
            return ref.id
        
    def get_reference_type(self, obj):
        service = obj.service
        app = service.app
        if app == 'quiz_data':
            return obj.service.instance
        return obj.service.app

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = '__all__'