# Generated by Django 2.2.1 on 2020-04-14 09:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services_handler', '0003_auto_20200414_0940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='app',
            field=models.CharField(choices=[('utils', 'utils'), ('quiz_data', 'quiz_data'), ('driving_lessons', 'driving_lessons'), ('adaptive_quiz', 'adaptive_quiz'), ('driving_licences', 'driving_licences'), ('lessons_files', 'lessons_files'), ('services_handler', 'services_handler')], default='quiz_data', max_length=100, verbose_name='Module name'),
        ),
    ]
