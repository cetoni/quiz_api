from django.apps import AppConfig


class ServicesHandlerConfig(AppConfig):
    name = 'services_handler'
