from django.apps import AppConfig


class AutomatedDocConfig(AppConfig):
    name = 'automated_doc'
