from django.db import models
from django.utils.translation import ugettext_lazy as _
from filer.models.filemodels import File
from user.models import User
# Create your models here.
class DocTag(models.Model):
    tag = models.CharField(_('tag'), max_length=200)

    def __str__(self):
        return self.tag

class DocMapping(models.Model):
    user_input = models.CharField(_("user input"), max_length=200)
    output = models.TextField(_("doc output"))
    tag = models.ForeignKey(DocTag, on_delete=models.CASCADE, related_name='doc_mapping')

    def get_output(self):
        return self.output

    def __str__(self):
        return self.user_input

class Risk(models.Model):
    name = models.CharField(_("name"), max_length=200)
    details = models.CharField(_("details"), max_length=200)
    default_probability = models.IntegerField(_("risk probability"))
    default_impact = models.IntegerField(_("risk impact"))
    
    def __str__(self):
        return self.name

class WorkGroup(models.Model):
    name = models.CharField(_("name"), max_length=100)
    activities = models.TextField(_("activities"))
    user = models.ForeignKey(User, verbose_name=_("user"), on_delete=models.CASCADE, related_name="workgroups", blank=True, null=True)

    def __str__(self):
        return self.name

    def get_risks_name(self):
        risks = []
        for r in self.riskassessement_set.all():
            risks.append(r.risk.name)
        return risks

class CounterMeasure(models.Model):
    name = models.CharField(_("name"), max_length=300)

    def __str__(self):
        return self.name
        
class DocTemplate(File):
    pass

class RiskAssessement(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    risk = models.ForeignKey(Risk, on_delete=models.CASCADE, related_name='assessement')
    probability = models.IntegerField()
    impact = models.IntegerField()
    score = models.IntegerField(null=True)
    groups = models.ManyToManyField(WorkGroup)
    measures = models.ManyToManyField(CounterMeasure)

    def __str__(self):
        return f"{self.risk.name}"

    def save(self, *args, **kwargs):
        self.score = self.impact * self.probability
        super().save(*args, **kwargs)