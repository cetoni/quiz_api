from django import forms
from project_apps.automated_doc.models import DocMapping

class DocForm(forms.Form):
    def __init__(self, fields, *args, **kwargs):
        super(DocForm, self).__init__(*args, **kwargs)
        for key, value in fields.items():
            self.fields[key] = forms.CharField(max_length=100)
