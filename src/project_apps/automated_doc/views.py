import os
import requests
from mimetypes import MimeTypes
from copy import copy
from docxtpl import DocxTemplate
from django.shortcuts import render
from django.views import View
from django.http import HttpResponse
from django.db.utils import IntegrityError
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from project_apps.automated_doc.forms import DocForm
from project_apps.automated_doc.models import DocMapping, DocTag, DocTemplate, Risk, CounterMeasure, WorkGroup, RiskAssessement
from project_apps.automated_doc.helpers import clean_data, get_mappings, get_group_table, add_risks_to_group
from project_apps.automated_doc.serializers import DocTemplateSerializer, RiskSerializer, MeasureSerializer, WorkGroupSerializer, RiskAssessementSerializer

# Create your views here.

class DocViews(APIView):
    form_class = DocForm

    def post(self, request, *args, **kwargs):
        # TODO: will write every risk in data.risks (duplicates)
        context = {}
        mappings = []
        data = []
        risks = []
        user = request.user
        mime = MimeTypes()
        data = copy(request.data)
        
        try:
            template_id = data.pop('template_id')
            content = data.get('content')
            for c in content:
                if 'groups' in c.keys():
                    groups = c.get('groups')
            template = DocTemplate.objects.get(file_ptr_id=template_id)
        except KeyError as e:
            print(e)
            return Response(e.__cause__, status=status.HTTP_400_BAD_REQUEST)
        except DocTemplate.DoesNotExist as e:
            print(e)
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)
       
        res = requests.get(template.url, stream=True)

        if res.status_code == 200:
            with open('template.docx', 'wb') as f:
                f.write(res.content)
        else:
            return Response('Could not download the template', status=status.HTTP_404_NOT_FOUND)

        doc = DocxTemplate('template.docx')

        try:
            # data is modified !! 
            data = clean_data(data)
        except ValueError as e:
            print(e)
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)
        
        try:
            mappings = get_mappings(data)
        except ValueError as e:
            print(e)
        
        for mapping in mappings:
            if isinstance(mapping, DocMapping):
                context[mapping.tag.tag] = mapping.get_output()
            else:
                keys, values = mapping.keys(), mapping.values()
                key, value = list(keys)[0], list(values)[0]
                context[key] = value
                
        # old logic that used the data in db to generate the docx
        if False:
            try:
                table = get_group_table(user, groups_ids)
            except ValueError as e:
                return Response(str(e), status=status.HTTP_404_NOT_FOUND)
        
        if groups:
            try:
                for group in groups:
                    if not group.get('risks'):
                        return Response('No groups were given', status=status.HTTP_400_BAD_REQUEST)
                    group['risks_names'] = [r['name'] for r in group['risks']]
                    risks += [risk for risk in group['risks']]
              
                for risk in risks:
                    risk['groups'] = []
                    risk['default_score'] = risk.get('default_impact') * risk.get('default_probability')
                    risk['score'] = risk.get('impact') * risk.get('probability') 
                    for g in groups:
                        if risk['name'] in g['risks_names']:
                            risk['groups'] += [g['name']]
            except KeyError as e:
                return Response(f'KeyError {e.__cause__} not found in request body', status=status.HTTP_400_BAD_REQUEST)

        context['groups'] = groups
        context['risks'] = risks
        
        doc.render(context)
        doc.save("static/docs/generated_doc.docx")
        
        with open('static/docs/generated_doc.docx', 'rb') as f:
            response = HttpResponse(f, content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            response['Content-Disposition'] = 'attachment; filename=generated_doc.docx'

        return response

class DocTemplateList(APIView):
    def get(self, request, *args, **kwargs):
        templates = DocTemplate.objects.all()
        serializer = DocTemplateSerializer(templates, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class DocTemplateDetails(APIView):
    def get(self, request, *args, **kwargs):
        try:
            tpl_id = kwargs.pop('template_id')
        except KeyError as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        f = DocTemplate.objects.get(id=tpl_id)
        res = requests.get(f.url, stream=True)

        with open('template.docx', 'wb') as tpl:
            tpl.write(res.content)

        docx = DocxTemplate('template.docx')

        return Response(docx.get_undeclared_template_variables())

class RiskList(APIView):
    
    def get(self, request, *args, **kwargs):
        risks = Risk.objects.all()
        serializer = RiskSerializer(risks, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
class RiskAssessmentView(APIView):
    
    def get(self, request, *args, **kwargs):
        user = request.user
        risks_assessments = user.riskassessement_set.all()
        serializer = RiskAssessementSerializer(risks_assessments, many=True)
        return Response(serializer.data)
class WorkGroupView(APIView):

    def get(self, request, *args, **kwargs):
        user = request.user
        work_groups = user.workgroups.all()
        serializer = WorkGroupSerializer(work_groups, many=True)
        if not serializer.data:
            return Response('Could not find workgroups', status=status.HTTP_404_NOT_FOUND)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    def post(self, request, *args, **kwargs):
        user = request.user
        try:
            name = request.data.pop('name')
            activities = request.data.pop('activities')
            risks_data = request.data.get('risks')
            workgroup = WorkGroup(name=name, activities=activities, user=user)
            workgroup.save()
        except KeyError as e:
            print(e)
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        
        if risks_data:
            try:
                add_risks_to_group(risks_data, workgroup, user)
            except KeyError as e:
                print(e)
                return Response(e.__cause__, status=status.HTTP_400_BAD_REQUEST)
            
        serializer = WorkGroupSerializer(workgroup)
        return Response(serializer.data)

class WorkGroupDetails(APIView):

    def put(self, request, pk, format=None):
        user = request.user
        data = copy(request.data)
        group_id = pk
        try:
            risks = data.pop('risks')
            workgroup = user.workgroups.get(id=group_id)
            add_risks_to_group(risks, workgroup, user)

        except KeyError as e:
            print(e)
            return Response(e.__cause__, status=status.HTTP_400_BAD_REQUEST)
        except WorkGroup.DoesNotExist as e:
            print(e)
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)
        except IntegrityError as e:
            print(e)
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        serializer = WorkGroupSerializer(workgroup)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        user = request.user
        try:
            workgroup = user.workgroups.get(id=pk)
            workgroup.delete()
            
        except WorkGroup.DoesNotExist as e:
            print(e)
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

        return Response(pk, status=status.HTTP_200_OK)

class CounterMeasureView(APIView):
    
    def get(self, request, *args, **kwargs):
        user = request.user
        measures = CounterMeasure.objects.filter(risk_assessement__user=user).all()
        serializer = MeasureSerializer(measures, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        measures = []
        risks = []
        res_status = None
        user = request.user
        data = copy(request.data)
        try:
            counter_measures = data.pop('counter_measures')

            for obj in counter_measures:
                risk_id = obj.pop('risk_id')
                measures_a = list(obj.pop('measures'))
                risk_assessements = user.riskassessement_set.filter(risk__id=risk_id).all()
                
                for risk_assess in risk_assessements:
                    for m_id in measures_a:
                        measure = CounterMeasure.objects.get(id=m_id)
                        measures.append(measure)
                        risk_assess.measures.add(measure)
                    risk_assess.save()
                    risks.append(risk_assess)

            serializer = RiskAssessementSerializer(risks, many=True)
            if not serializer.data:
                res_status = status.HTTP_404_NOT_FOUND
            else:
                res_status = status.HTTP_200_OK
    
        except KeyError as e:
            return Response(str(e), status=status.HTTP_4OO_BAD_REQUEST)
        except (RiskAssessement.DoesNotExist, Risk.DoesNotExist, CounterMeasure.DoesNotExist) as e:
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)
        
        return Response(serializer.data, status=res_status)