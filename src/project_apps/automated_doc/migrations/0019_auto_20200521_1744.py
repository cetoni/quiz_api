# Generated by Django 2.2.1 on 2020-05-21 17:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('automated_doc', '0018_remove_workgroup_risks'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskassessement',
            name='groups',
            field=models.ManyToManyField(to='automated_doc.WorkGroup'),
        ),
        migrations.AlterUniqueTogether(
            name='riskassessement',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='riskassessement',
            name='group',
        ),
    ]
