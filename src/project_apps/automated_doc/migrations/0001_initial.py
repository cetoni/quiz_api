# Generated by Django 2.2.1 on 2020-05-06 21:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DocTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=200, verbose_name='tag')),
            ],
        ),
        migrations.CreateModel(
            name='DocMapping',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_input', models.CharField(max_length=200, verbose_name='user input')),
                ('output', models.TextField(verbose_name='doc output')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='doc_mapping', to='automated_doc.DocTag')),
            ],
        ),
    ]
