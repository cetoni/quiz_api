# Generated by Django 2.2.1 on 2020-05-19 22:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('automated_doc', '0017_auto_20200519_2204'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='workgroup',
            name='risks',
        ),
    ]
