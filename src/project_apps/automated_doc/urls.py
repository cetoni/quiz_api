from django.urls import path
from project_apps.automated_doc import views
from project_apps.automated_doc.apps import AutomatedDocConfig

app_name = AutomatedDocConfig.name
urlpatterns = [
    path('doc/', views.DocViews.as_view(), name='doc'),
    path('doc/templates/', views.DocTemplateList.as_view(), name='templates'),
    path('doc/templates/<int:template_id>/', views.DocTemplateDetails.as_view(), name='template_details'),
    path('doc/risks/', views.RiskList.as_view(), name='risks'),
    path('doc/user/risks/', views.RiskAssessmentView.as_view(), name='user_risk_assessements'),
    path('doc/counter_measures/', views.CounterMeasureView.as_view(), name='counter_measures'),
    path('doc/workgroups/', views.WorkGroupView.as_view(), name='user_workgroups'),
    path('doc/workgroups/<int:pk>/', views.WorkGroupDetails.as_view(), name='add_risk_worgroup')
]