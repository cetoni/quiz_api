from django.contrib import admin
from project_apps.automated_doc.models import DocMapping, DocTag, Risk, WorkGroup, CounterMeasure, DocTemplate, RiskAssessement
# Register your models here.

class DocMappingAdmin(admin.ModelAdmin):
    list_display = ('user_input', 'tag')

class WorkGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_risks')
    search_fields = ['user',]
    raw_id_fields = ('user',)

    def get_risks(self, obj):
        names = []
        for risk_assess in obj.riskassessement_set.all():
            names.append(risk_assess.risk.name)
        return names
        
    get_risks.short_description = "risks"

class DocTemplateAdmin(admin.ModelAdmin):
    fields = ('file', 'name')

class RiskAssessementAdmin(admin.ModelAdmin):
    fields = ('user', 'risk', 'probability', 'impact', 'score')
    raw_id_fields = ('user',)


admin.site.register(DocMapping, DocMappingAdmin)
admin.site.register(DocTag)
admin.site.register(Risk)
admin.site.register(RiskAssessement, RiskAssessementAdmin)
admin.site.register(WorkGroup, WorkGroupAdmin)
admin.site.register(CounterMeasure)
admin.site.register(DocTemplate, DocTemplateAdmin)