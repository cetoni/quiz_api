from rest_framework import serializers

from project_apps.automated_doc.models import DocTemplate, Risk, WorkGroup, CounterMeasure, RiskAssessement

class DocTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocTemplate
        fields = '__all__'

class MeasureSerializer(serializers.ModelSerializer):
    class Meta:
        model = CounterMeasure
        fields = '__all__'

class RiskSerializer(serializers.ModelSerializer):
    default_score = serializers.SerializerMethodField()
    class Meta:
        model = Risk
        fields = '__all__'
     
    def get_default_score(self, obj):
        return obj.default_probability * obj.default_impact
        
class WorkGroupSerializer(serializers.ModelSerializer):
    risk_assessements = serializers.SerializerMethodField()
    class Meta:
        model = WorkGroup
        exclude = ('user',)
        depth = 1

    def get_risk_assessements(self, obj):
        risk_assess = obj.riskassessement_set.all()
        serializer = RiskAssessementSerializer(risk_assess, many=True)
        return serializer.data

class RiskAssessementSerializer(serializers.ModelSerializer):
    counter_measures = serializers.SerializerMethodField()
    risk = serializers.SerializerMethodField()

    class Meta:
        model = RiskAssessement
        exclude = ('user', 'measures')
    
    def get_counter_measures(self, obj):
        measures = obj.measures.all()
        serializer = MeasureSerializer(measures, many=True)
        return serializer.data
    
    def get_risk(self, obj):
        serializer = RiskSerializer(obj.risk)
        return serializer.data