from copy import copy

from project_apps.automated_doc.models import DocTag, DocMapping, WorkGroup, Risk, RiskAssessement
from project_apps.automated_doc.serializers import RiskSerializer, RiskAssessementSerializer, WorkGroupSerializer

def clean_data(data):
    keys = []
    tags = DocTag.objects.all()
    if data:
        for pair in data['content']:
            # only keys in data['content'] will be substituted
            if 'groups' not in pair.keys():
                keys.append(pair)
    if not keys:
        raise ValueError('no tags with given data were found')
    return {'keys':keys, 'tags':tags}
    
def get_mappings(data):
    items = []
    if data:
        for i in data['keys']:
            for key, value in i.items():
                item = None
                try:
                    tag = data['tags'].get(tag=key)
                    item = DocMapping.objects.get(tag=tag, user_input=value)
                except (DocMapping.DoesNotExist, DocTag.DoesNotExist) as e:
                    print(e)
                    item = {key:value}
                # items holds both docmapping objects and subsituable values 
                items.append(item)
    if not items:
        raise ValueError('Could not find any mappings with given data')
    return items

def get_group_table(user, groups_ids):
    a = []
    table = []
    risks = []
    r_groups = []
    r_measures = []
    try:
        user_groups = user.workgroups.all()
        risk_a = [g.riskassessement_set.all() for g in user_groups if g.id in groups_ids]

        if not len(risk_a):
            return []
        for r in risk_a:
            for r_a in r:
                for g in r_a.groups.all():
                    r_groups.append(g.name)
                for m in r_a.measures.all():
                    r_measures.append(m.name)

                serializer = dict(risk=RiskSerializer(r_a.risk).data, groups=' - '.join(r_groups),
                        measures=r_measures, probability=r_a.probability, impact=r_a.impact,
                        score=r_a.score)
                a.append(serializer)
                r_groups = []
                r_measures = []
        risks+=a
        print(len(risks))
    except WorkGroup.DoesNotExist as e:
        print(e)
        groups = {}

    for group in [g for g in user_groups if g.id in groups_ids]:
        table.append({'label': group.name, 'activities': group.activities, 'risks': group.get_risks_name()})

    return {'groups': table, 'risks': risks}

def add_risks_to_group(risks_data, workgroup, user):
    if risks_data:
        for r_obj in risks_data:
            try:
                r_id = r_obj.pop('id')
                impact = r_obj.pop('impact')
                probability = r_obj.pop('probability')

                risk = Risk.objects.get(id=r_id)
                risk_assess = RiskAssessement(user=user, risk=risk, impact=impact, probability=probability)
                risk_assess.save()
                risk_assess.groups.add(workgroup)
            except Risk.DoesNotExist as e:
                print(e)
                pass

    return True