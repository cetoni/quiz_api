import re
import json

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from django import forms 
from django.db import models
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.utils.translation import gettext as _

from user.forms import RegistrationForm, LoginForm, CustomerForm
from project_apps.driving_licences.forms import DrivingLicenceForm

from user.models import UserLogin
from user.serializers import UserLoginSerializer

@plugin_pool.register_plugin
class QuizPlugin(CMSPluginBase):
    model = CMSPlugin
    name = 'Quiz CQC'
    render_template = "_cms/plugins/quiz.html"
    cache = False

@plugin_pool.register_plugin
class Registration(CMSPluginBase):
    """Plugin to render registration form"""
    model = CMSPlugin
    name = 'Registration form'
    # render_template = '_cms/legacy/plugins/registration.html'
    render_template = '_cms/plugins/registration.html'

    def render(self, context, instance, placeholder):
        request = context['request']

        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'user_form': RegistrationForm()
        })
        return context

@plugin_pool.register_plugin
class RegistrationCustomer(Registration):
    """Plugin to render customer registration form"""
    name = 'Customer registration form'
    render_template = '_cms/plugins/customer_registration.html'

    def render(self, context, instance, placeholder):
        request = context['request']

        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'customer_form': CustomerForm()
        })
        return context

# You need to register before using
class Login(CMSPluginBase):
    """Plugin to render login form"""
    model = CMSPlugin
    name = 'Login form'
    render_template = '_cms/plugins/login.html'

    def render(self, context, instance, placeholder):
        request = context['request']
        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'form': LoginForm(),
        })
        return context

class JitsiPlugin(CMSPlugin):
    domain = models.CharField(max_length=100, default="videoconf.editricetoni.it")
    room_name = models.CharField(max_length=100, blank=True) # TODO: Adda validation
    site = models.ForeignKey(Site, on_delete=models.SET_NULL, blank=True, null=True)
    android_app_package = models.CharField(max_length=100, default='org.jitsi.meet', blank=True, null=True)
    app_scheme = models.CharField(max_length=100, default='org.jitsi.meet', blank=True, null=True)
    mobile_download_link_android = models.CharField(max_length=150, default='https://play.google.com/store/apps/details?id=org.jitsi.meet')

class JitsiForm(forms.ModelForm):
    class Meta:
        model = JitsiPlugin
        fields = ('domain', 'room_name', 'android_app_package', 'app_scheme')

@plugin_pool.register_plugin
class JitsiPublisher(CMSPluginBase):
    name = 'VideoConf'
    model = JitsiPlugin
    render_template = '_cms/plugins/jitsi.html'
    form = JitsiForm

    def render(self, context, instance, placeholder):
        regex = re.compile('[^a-zA-Z]')
        request = context.get('request')
        page = context.get('current_page')
        site = get_current_site(request)
        if not instance.room_name:
            name = site.domain + page.get_title() + self.name
            instance.room_name = regex.sub('', name)
            instance.site = site
            instance.save()
        context = super(JitsiPublisher, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class LegacyQuizB(CMSPluginBase):
    model = CMSPlugin
    name ='Quiz Legacy B'
    render_template = '_cms/legacy/plugins/quizzes/quiz_B.html'
    cache = False

@plugin_pool.register_plugin
class LegacyQuizAM(CMSPluginBase):
    model = CMSPlugin
    name ='Quiz Legacy AM'
    render_template = '_cms/legacy/plugins/quizzes/quiz_AM.html'
    cache = False

@plugin_pool.register_plugin
class LegacyQuizRevisioni(CMSPluginBase):
    model = CMSPlugin
    name ='Quiz Legacy Revisioni'
    render_template = '_cms/legacy/plugins/quizzes/quiz_revisioni.html'
    cache = False

@plugin_pool.register_plugin
class LegacyQuizCDE(CMSPluginBase):
    model = CMSPlugin
    name ='Quiz Legacy CDE'
    render_template = '_cms/legacy/plugins/quizzes/quiz_CDE.html'
    cache = False

@plugin_pool.register_plugin
class CalendarSpa(CMSPluginBase):
    model = CMSPlugin
    name = 'Lessons Calendar'
    render_template = 'calendar_spa/src/index.html'

class SpaModulesPlugin(CMSPlugin):
    link = models.CharField(max_length=100)
    
@plugin_pool.register_plugin
class SpaPublisher(CMSPluginBase):
    model = SpaModulesPlugin
    name = 'Spa modules'
    render_template = '_cms/plugins/spa.html'

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'url': instance.link,
        })
        return context

@plugin_pool.register_plugin
class SessionPublisher(CMSPluginBase):
    model = CMSPlugin
    name = 'Sessions'
    render_template = '_cms/sessions/sessions.html'

    def render(self, context, instance, placeholder):
        logout_date = None
        logout_time = None
        serializer = []
        t_heads = ['utente', 'data di accesso', 'ora di accesso', 'data di disconnessione', 'ora di disconnessione']
        user = context['request'].user
        sessions = UserLogin.objects.all()
        
        for s in sessions:
            identifier = s.user.email
            if not identifier:
                identifier = s.user.username
            if s.logout_timestamp:
                logout_date = s.logout_timestamp.date
                logout_time= s.logout_timestamp.time
            
            login = s.login_timestamp

            obj = dict(name=identifier, login_date=login.date, login_time=login.time, 
                    logout_date=logout_date, logout_time=logout_time)

            serializer.append(obj)

        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'sessions': serializer,
            'theads': t_heads
        })
        return context

class FaceBookPlugin(CMSPlugin):
    link = models.CharField(max_length=250)

@plugin_pool.register_plugin
class FaceBookPluginPublisher(CMSPluginBase):
    model = FaceBookPlugin
    name = 'Facebook Frame'
    render_template = '_cms/plugins/facebook.html'