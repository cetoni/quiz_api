from django.apps import AppConfig


class QuizDataConfig(AppConfig):
    name = 'quiz_data'
