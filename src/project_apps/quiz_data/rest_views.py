import json
import random
import re
from collections.abc import Iterable
from datetime import datetime, timedelta
from mimetypes import MimeTypes

from django.contrib.auth import get_user_model
from django.db.models import F, Q
from django.http import HttpResponse
from pytz import UTC
from rest_framework import generics, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from docxtpl import DocxTemplate
from project_apps.lessons_files.models import Media
from project_apps.quiz_data.models import QuestionCategoryModel, QuizModel
from project_apps.services_handler.permissions import HasSubscription
from user import serializers
from user.utils import OdooManager

from . import local_serializers, models

User = get_user_model()

def get_child(category):
    children = category.child.all()
    if not children:
        return category
    return [get_child(child) for child in children] + [category]

def get_parent(category):
    parent = category.category
    if not parent:
        return category
    return get_parent(parent)

def depth(l):
    if isinstance(l, list):
        for value in l:
            for subvalue in depth(value):
                yield subvalue
    else: yield l

class SchoolStudentsMixin(object):
    def get_school_students(self, school: User, quiz: QuizModel) -> list:
        """Returns school students of the corresponding quiz"""
        students = []

        odoo = OdooManager().odoo_login()
        Certificate = odoo.env['odoo_partner.certificates']
        for student in school.child.filter(is_active=True):
            cert_id = Certificate.search([('template_id','=', quiz.odoo_id), ('partner_id', '=', student.odoo_id)])
            if cert_id:
                student_ids.append(student.id)
            if student.id in student_ids:
                students.append(student)

        return students

    def get_media_viewing(self, media: Media, user: User):
        completed = False
        total_watched = 0
        media_stats_count = media.mediastatscount_set.filter(user=user)
        for s in media_stats_count:
            total_watched += s.total_watched
        return total_watched/media.duration

    def get_intervals(self, media_stats: list):
        """Return intervals of datetime string and floats.

        Takes a list of tuples (media created at, watched minutes)
        Return a tuple (interval of datetime strings, total of seconds of viewing)
        """
        value = None
        result = {}
        for minute, duration in media_stats:
            total = result.get(minute, 0) + duration
            result[minute] = total
        minutes = [k for k,v in result.items()]
        duration = sum([v for k,v in result.items()])
        if minutes:
            intervals = [(minutes[0], minutes[-1])]
            value = (intervals, duration)
        return value

    def get_students_media_stats(self, student: User, quiz: QuizModel) -> list:
        """Return medias stats per user"""

        categories = []
        completed_list = []
        medias_list = []
        res_data = {}
        data = []

        nested = [child for category in quiz.categories.all() for child in get_child(category)]

        for c in nested:
            categories += depth(c)
        for category in categories:
            medias = category.media_set.all()
            for media in medias:
                values = []
                completed = False
                total_watched = 0

                medias_list.append(media)
                media_stats_count = media.mediastatscount_set.filter(user=student)
                media_stats = [(m.created_at.strftime('%d-%m-%Y %H:%M'), m.watched_duration) for m in media.media_stats.filter(user=student)]

                value = self.get_intervals(media_stats)
                if value:
                    values.append(value)

                for s in media_stats_count:
                    total_watched += s.total_watched
                completed = True if total_watched >= media.duration else False
                if completed:
                    completed_list.append(completed)
                data.append({'media_id': media.id, 'duration': media.duration, 'stats': values,
                             'watched': float(total_watched), 'completed': completed, 'media_title': media.name})

        res_data['watched_videos_completed'] = len(completed_list)/len(medias_list)
        return res_data, data

# returns the list of question categories
class QuestionCategoryListView(generics.ListAPIView):
    """
        Get a list of questions
    """
    serializer_class = local_serializers.QuestionCategorySerializer
    queryset = models.QuestionCategoryModel.objects.all()


# take categories and give questions from given categories
class StartExerciseAPIView(GenericAPIView):
    serializer_class = local_serializers.ExerciseInitializeSerializer
    answer_serializer = local_serializers.ToBeAnswerSerializer
    queryset = models.AnswerModel.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        categories = serializer.validated_data['categories']

        questions = []

        for c in categories:
            qs = models.AnswerModel.objects.filter(question__category=c, student=request.user,
                                                   given_answer__isnull=True, exam__isnull=True)
            if not qs:
                qs = []
                for q in c.question.all():
                    qs.append(models.AnswerModel.objects.create(student=request.user, question=q))
            answer_serializer = self.answer_serializer(qs, many=True)
            questions += answer_serializer.data
            random.shuffle(questions)
        return Response(questions[:40], status=status.HTTP_201_CREATED)


# post answer from anywhere, for exam, exercise, error_review
class ExercisePostAnswersView(GenericAPIView):
    exam_model = models.ExamModel
    serializer_class = local_serializers.PostAnswerSerializer
    response_serializer = local_serializers.AnsweredQuestionSerializer
    exam_object = None

    def get_queryset(self):
        kwargs = {'exam_id': self.kwargs.get('exam_id'), 'student': self.request.user}

        if kwargs.get('exam_id'):
            try:
                exam_object = self.exam_model.objects.get(**kwargs)
                self.exam_object = exam_object
                if exam_object.is_over:
                    return [], False, Response({'error': 'Exam is over'})

                if UTC.localize(datetime.utcnow()) > exam_object.created_at + timedelta(
                        minutes=exam_object.quiz.exam_duration):
                    return [], False, Response({'error': "Time Window to complete Exam is over"},
                                               status=status.HTTP_403_FORBIDDEN)

                qs = exam_object.exam_questions.all()
            except self.exam_model.DoesNotExist:
                return [], False, Response({'error': 'Exam is not associated with student'})
        else:
            qs = models.AnswerModel.objects.filter(Q(is_correct=False) | Q(is_correct__isnull=True),
                                                   student=self.request.user)
        return qs, True, None

    def get(self, request, *args, **kwargs):
        kwargs['data'] = request.data
        return self.post(request, *args, **kwargs)

    def post(self, request, *args, data=None, **kwargs):
        qs, proceed, response = self.get_queryset()
        if not proceed:
            return response
        data = kwargs.get('data')
        exam_id = kwargs.get('exam_id')
        if not data:
            data = request.data
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)
        is_over = request.data.get('is_over')
        questions = serializer.validated_data.get('questions')
        answers = serializer.validated_data.get('answers')
        response_answers = []

        if not answers:
            return Response({'error': 'No answers were provided'}, status=status.HTTP_204_NO_CONTENT)
        for k, v in zip(questions, answers):
            try:
                que = qs.get(id=int(k), student=request.user)
                if isinstance(v, int) or isinstance(v, str):
                    v = [v]
                v = list(map(str, v))
                q_str = json.dumps(v)
                que.given_answer = q_str
                que.is_correct = True if que.question.right_answers_json == v else False
                que.save()
                response_answers.append(que)
            except models.AnswerModel.DoesNotExist as e:
                print(e)
                pass
        for q in qs:
            if not q.given_answer and not q.exam_id:
                q.delete()
        if self.exam_object:
            right_answers = self.exam_object.exam_questions.filter(is_correct=True).count()
            self.exam_object.obtained_marks = right_answers
            if self.exam_object.obtained_marks >= self.exam_object.passing_marks:
                self.exam_object.result = 'Passed'
            else:
                self.exam_object.result = 'Failed'
            if is_over:
                self.exam_object.is_over = True
            self.exam_object.save()
            serializer = local_serializers.ExamResultSerializer(self.exam_object)
        else:
            serializer = self.response_serializer(response_answers, many=True)
        result = {'result': serializer.data}
        return Response(result, status=status.HTTP_201_CREATED)


# error_review api on basis of quiz
# or on basis of requested user and
# on running exam base
class ErrorReviewAPIView(generics.ListAPIView):
    serializer_class = local_serializers.AnsweredQuestionSerializer

    def get_queryset(self):
        kwargs = {}
        # for filtering given quiz questions
        quiz = self.kwargs.get('quiz')
        user_id = self.kwargs.get('user_id')
        if quiz:
            try:
                quiz = models.QuizModel.objects.get(id=quiz)
                parent_categories = quiz.categories.all()
            except models.QuizModel.DoesNotExist:
                return [], Response({'error': 'No exam found for this quiz'})

        # selection of answers belongs to particular exam
        if self.kwargs.get('exam_id'):
            try:
                kwargs['exam__exam_id'] = self.kwargs['exam_id']
                kwargs['given_answer__isnull'] = True
                exam_object = models.ExamModel.objects.get(exam_id=self.kwargs['exam_id'])
                if not exam_object.is_over:
                    if exam_object.created_at + timedelta(minutes=exam_object.quiz.exam_duration) < UTC.localize(
                            datetime.utcnow()):
                        return [], Response({'error': "Time Window to complete Exam is over"},
                                            status=status.HTTP_403_FORBIDDEN)
                else:
                    return [], Response({'error': 'Exam is over'})
                qs = exam_object.exam_questions.all()

            except models.ExamModel.DoesNotExist:
                return [], Response({'error': 'Exam is not associated with student'})

        # get user by given id or default to request user
        user = self.request.user
        if user_id:
            try:
                user = User.objects.get(pk=user_id)
                if not user.is_student:
                    return [], Response({'error': 'Given user is not a student'}, status=status.HTTP_400_BAD_REQUEST)
            except User.DoesNotExist as e:
                print(e)
                return [], Response({'error': 'Could no find user with given id'}, status=status.HTTP_404_NOT_FOUND)

        # filtering data for only user
        qs = models.AnswerModel.objects.filter(
            student=user, is_correct=False,
        ).order_by('updated_at')
        querys = []

        if self.kwargs.get('category'):
            category = models.QuestionCategoryModel.objects.get(id=self.kwargs.get('category'))
            querys += qs.filter(question__category=category)
            return querys, None

        if parent_categories:
            for i in parent_categories:
                child = []
                for c in models.QuestionCategoryModel.get_children(i):
                    if isinstance(c, Iterable):
                        for item in c:
                            child.append(item)
                    else:
                        child.append(c)
                querys += qs.filter(question__category__in=child)
            querys = (querys[::-1])[:40]

        return querys, None

    def get(self, request, *args, **kwargs):
        queryset, response = self.get_queryset()
        if not response:
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        return response


class ExamStartAPIView(generics.CreateAPIView):
    serializer_class = local_serializers.ExamInitializerSerializer
    queryset = models.ExamModel.objects.all()

    def perform_create(self, serializer):
        quiz = serializer.validated_data['quiz']

        exam = serializer.save(
            student=self.request.user,
            result='Not Decleared',
            passing_marks=quiz.passing_marks,
            obtained_marks=0
        )
        exam_sets = quiz.exam_set.all()
        exam_paper = []
        ids = []
        aq_list = []
        questions = []
        q = None
        if exam_sets:
            exam_set = random.choice(exam_sets)
            if exam_set.question_chapter.all() == 1:
                c = exam_set.question_chapter.first()
                questions = chapter.question.all()
            else:
                for sequence in exam_set.question_chapter.all():
                    categories = models.QuestionCategoryModel.get_children(sequence.chapter)
                    if len([categories]) <= 1:
                        categories = [categories]
                    random.shuffle(categories)
                    for c in categories:
                        if isinstance(c, Iterable):
                            for item in c:
                                qs = item.question.all()
                                q = random.choice(qs)
                                break
                        else:
                            qs = c.question.all()
                            q = random.choice(qs)
                            break
                    if q:
                        questions.append(q)
        for i in questions:
            models.AnswerModel.objects.create(exam=exam, question_id=i.id, student=self.request.user)
        return exam

    def create(self, request, *args, **kwargs):
        categories = []
        completed = []
        to_watch = []
        media_stats_count = []
        incomplete_videos = []
        user = request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        quiz = serializer.validated_data.get('quiz')

        if quiz.requires_video_completion:
            nested = [child for category in quiz.categories.all() for child in get_child(category)]
            for c in nested:
                categories += depth(c)

            for category in categories:
                medias = category.media_set.all()
                for media in medias:
                    to_watch.append(media)
                    # find entirely watched medias
                    media_stats_count = media.mediastatscount_set.filter(user=user, watched_entirely=True)

                    if media_stats_count:
                        completed.append(media)
                    else:
                        incomplete_videos.append(media.name)

            if len(to_watch) != len(completed):
                message = "Non puoi ancora accedere alla valutazione, devi completare la visione dei seguenti video:"
                return Response(dict(error=message, medias=incomplete_videos), status=status.HTTP_403_FORBIDDEN)

        exam_qs = models.ExamModel.objects.filter(student=request.user, quiz=serializer.validated_data['quiz'])
        exam_obj = None
        for exam in exam_qs:
            if not exam.is_over:
                if exam.created_at + timedelta(minutes=exam.quiz.exam_duration) > UTC.localize(datetime.utcnow()):
                    exam_obj = exam
        if not exam_obj:
            exam_obj = self.perform_create(serializer)
        serializer = local_serializers.ExamSerializer(instance=exam_obj)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class StatsAPIView(APIView):

    def error_calc(self, category, id):
        user = None
        today = datetime.today()
        today = UTC.localize(datetime(year=today.year, month=today.month, day=today.day))
        if id:
            user = User.objects.get(id=id)
            total = models.AnswerModel.objects.filter(student=user, question__category=category)
        else:
            user = self.request.user
            total = models.AnswerModel.objects.filter(student=self.request.user, question__category=category)
        questions_qs = models.AnswerModel.objects.filter(student=user, is_correct=False,
                                                         question__category=category)
        today_wrongs = questions_qs.filter(updated_at__gte=today)

        week_wrongs = questions_qs.filter(updated_at__lte=today, updated_at__gte=today - timedelta(days=7))
        previous_data = questions_qs.filter(updated_at__lte=today - timedelta(days=7))

        return today_wrongs, week_wrongs, previous_data, total

    def get_scores(self, categories, id):
        topic = []
        for c in categories:
            sample_topic = {}
            today_wrongs, week_wrongs, previous_data, total = self.error_calc(c, id)
            all_count = today_wrongs.count() + week_wrongs.count() + previous_data.count()
            sample_topic['text'] = c.title
            sample_topic['id'] = c.id
            sample_topic['error'] = {}
            sample_topic['error']['current'] = today_wrongs.count() / total.count()
            sample_topic['error']['week'] = week_wrongs.count() / total.count()
            sample_topic['error']['week3'] = previous_data.count() / total.count()
            sample_topic['error']['all'] = all_count / total.count()
            topic.append(sample_topic)

        return topic

    def get(self, request, *args, **kwargs):
        today = datetime.today()
        today = UTC.localize(datetime(year=today.year, month=today.month, day=today.day))

        user = self.request.user
        quiz = kwargs.get('quiz')
        id = request.GET.get('id')
        if id:
            try:
                user = User.objects.get(id=id)
            except User.DoesNotExist as e:
                print(e)
                pass

        if kwargs.get('quiz') and kwargs.get('category'):
            topic_error = {
                'text': None,
                'id': None,
                'error': {
                    'current': None,
                    'week': None,
                    'week3': None,
                    'all': None,
                }
            }
            category = models.QuestionCategoryModel.objects.get(id=kwargs.get('category'))
            today_wrongs, week_wrongs, previous_data, total = self.error_calc(category, id)
            all_count = today_wrongs.count() + week_wrongs.count() + previous_data.count()
            topic_error['text'] = category.title
            topic_error['id'] = category.id
            topic_error['error']['current'] = today_wrongs.count() / total.count()
            topic_error['error']['week'] = week_wrongs.count() / total.count()
            topic_error['error']['week3'] = previous_data.count() / total.count()
            topic_error['error']['all'] = all_count / total.count()
            return Response(topic_error)

        students = []
        scores = []
        res = []
        a = []

        parent_categories = models.QuizModel.objects.get(id=quiz).categories.all()
        categories = models.QuestionCategoryModel.get_child(user, parent_categories)
        if user.is_school:
            students = user.child.filter(is_student=True)

        if not students:
            scores = self.get_scores(categories, id)
            res = scores
        else:
            for student in students:
                score = []
                categories = models.QuestionCategoryModel.get_child(student, parent_categories)
                for category in categories:
                    today_wrongs, week_wrongs, previous_data, total = self.error_calc(category, student.id)
                    score = [category.id, today_wrongs.count(), week_wrongs.count(), previous_data.count(), total.count()]
                    if score:
                        scores.append(score)
            for arr in scores:
                total = 0
                current = 0
                week = 0
                week3 = 0
                c_id = arr[0]
                for arr in scores:
                    if arr[0] == c_id and (c_id not in a):
                        current += arr[1]
                        week += arr[2]
                        week3 += arr[3]
                        total += arr[4]
                if total:
                    res.append({
                        'text': models.QuestionCategoryModel.objects.get(pk=c_id).title,
                        'id': c_id,
                        'error': {
                            'current': current / total,
                            'week': week / total,
                            'week3': week3 / total,
                            'all': (current + week + week3) / total}
                    })
                a.append(c_id)
        return Response({ 'topic': res })


class CategoryStatsAPIView(generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        filter_kwargs = {
            'question__category': kwargs['category']
        }
        ids = models.AnswerModel.objects.filter(student=request.user, **filter_kwargs).values_list('question',
                                                                                                   flat=True)
        qs = models.QuestionModel.objects.filter(id__in=ids).distinct()
        serializer = local_serializers.QuestionAnswerSerializer(qs, many=True)
        return Response(data=serializer.data, )


class ExamStatsAPIView(generics.ListAPIView):

    serializer_class = local_serializers.ExamStatsSerializer

    def get_queryset(self):
        return models.ExamModel.objects.filter(student=self.request.user)


class StudentExamStatsAPIView(generics.GenericAPIView):
    serializer_class = local_serializers.ExamStatsSerializer

    def error_calc(self, quiz, id=None):
        today = datetime.today()
        today = UTC.localize(datetime(year=today.year, month=today.month, day=today.day))
        if id:
            try:
                exam_qs = models.ExamModel.objects.filter(student=User.objects.get(id=id), quiz=quiz)
            except User.DoesNotExist as e:
                raise ValueError(e)
        else:
            exam_qs = models.ExamModel.objects.filter(student=self.request.user, quiz=quiz)
        today_exam = exam_qs.filter(updated_at__gte=today)
        week_exam = exam_qs.filter(updated_at__lte=today, updated_at__gte=today - timedelta(days=7))
        previous_exams = exam_qs.filter(updated_at__lte=today - timedelta(days=7))
        return today_exam, week_exam, previous_exams, exam_qs

    def handler(self, id, quiz):
        qs = models.QuizModel.objects.get(id=quiz)
        passing_marks = local_serializers.PassingMarksSerializer(qs)
        try:
            today_exam, week_exam, previous_exams, exam_qs = self.error_calc(quiz, id=id)
            exam_set = models.ExamQuestionSet.objects.get(quiz__id=quiz)
        except ValueError as e:
            return Response('No user was found with the given id', status=status.HTTP_404_NOT_FOUND)
        except models.ExamQuestionSet.DoesNotExist as e:
            return Response('No exam question set was found', status=status.HTTP_404_NOT_FOUND)

        exam_len = len(exam_set.question_chapter.all())

        dummy_dict = {}
        sample = {'current': 0, 'week': 0, 'week3': 0, 'all': 0,}
        count = 0
        dummy_dict['current'] = local_serializers.ExamStatsSerializer(today_exam, many=True).data
        dummy_dict['week'] = local_serializers.ExamStatsSerializer(week_exam, many=True).data
        dummy_dict['week3'] = local_serializers.ExamStatsSerializer(previous_exams, many=True).data
        dummy_dict['all'] = local_serializers.ExamStatsSerializer(exam_qs, many=True).data
        for i in range(today_exam.count()):
            serializer = local_serializers.ExamStatsSerializer(today_exam, many=True)
            count += serializer.data[i]['obtained_marks'] / passing_marks.data['passing_marks']
        if count:
            sample['current'] = count / len(today_exam)
        count = 0
        for i in range(week_exam.count()):
            serializer = local_serializers.ExamStatsSerializer(week_exam, many=True)
            count += serializer.data[i]['obtained_marks'] / passing_marks.data['passing_marks']
        if count:
            sample['week'] = count / len(week_exam)
        count = 0
        for i in range(previous_exams.count()):
            serializer = local_serializers.ExamStatsSerializer(previous_exams, many=True)
            count += serializer.data[i]['obtained_marks'] / passing_marks.data['passing_marks']
        if count:
            sample['week3'] = count / len(previous_exams)
        sample['all'] = ((sample['current'] * len(today_exam)) + (sample['week'] * len(week_exam)) + (sample[
                                                                                                          'week3'] * len(
            previous_exams))) / 3
        # passed = local_serializers.StatSerializer(querys.filter(obtained_marks__gte=F('passing_marks')), many=True)
        return Response(
            {'passing_marks': passing_marks.data['passing_marks'], "exam_list": dummy_dict, "time_granular": sample, 'exam_questions_number': exam_len},
            status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        id = request.data.get('id')
        quiz = kwargs.get('quiz')
        return self.handler(id, quiz)

    def get(self, request, *args, **kwargs):
        id = request.GET.get('id')
        quiz = kwargs.get('quiz')
        return self.handler(id, quiz)

class SchoolStudentExamStatsAPIView(generics.RetrieveAPIView, SchoolStudentsMixin):

    def get(self, request, *args, **kwargs):
        student_ids = []
        media_stats = []
        quiz_id = kwargs.get('quiz')
        
        try:
            school = User.objects.get(is_school=True, id=request.user.id)
            if quiz_id:
                quiz = QuizModel.objects.get(id=quiz_id)
                odoo = OdooManager().odoo_login()
                Certificate = odoo.env['odoo_partner.certificates']
        except User.DoesNotExist:
            return Response({'error': 'Requested user is not school'})

        school_data = {'school': school.school_name, 'students': []}

        for student in school.child.filter(is_active=True):
            qs = models.ExamModel.objects.filter(student=student)
            serializer = local_serializers.ExamStatsSerializer(qs, many=True)
            passed = local_serializers.StatSerializer(qs.filter(obtained_marks__gte=F('passing_marks')), many=True)
            cert_id = Certificate.search([('template_id','=', quiz.odoo_id), ('partner_id', '=', student.odoo_id)])
            if cert_id:
                student_ids.append(student.id)
            if student.id in student_ids:
                exam_passed = False
                if passed.data:
                    exam_passed = True
                if quiz.requires_video_completion:
                    media_stats = self.get_students_media_stats(student, quiz)[0]
                school_data['students'].append(
                    {'id': student.id, 'username': student.username, 'first_name': student.first_name, 'last_name': student.last_name,
                    'result': {'appeared': serializer.data, 'passed': passed.data, 'exam_passed_once': exam_passed}, 'media_stats': media_stats})
        return Response(school_data, status=status.HTTP_200_OK)


class CategoryHierarchyAPIView(generics.RetrieveAPIView):

    queryset = models.QuizModel.objects.all()
    serializer_class = local_serializers.QuizCategoryHierarchySerializer
    lookup_url_kwarg = 'id'
    lookup_field = 'id'


class GetExam(generics.RetrieveAPIView):
    serializer_class = local_serializers.AnsweredQuestionSerializer
    student_serializer = serializers.StudentListSerializer

    def get(self, request, *args, **kwargs):
        exam_id = kwargs.get('exam_id')
        student = models.ExamModel.objects.filter(exam_id=exam_id).values_list('student', flat=True)
        try:
            student_data = User.objects.get(id__in=list(student))
            querys = models.AnswerModel.objects.filter(exam=exam_id)
            student_serializer = self.student_serializer(student_data)
            serializer = self.serializer_class(querys, many=True)
            return Response({"exam": exam_id, "Student": student_serializer.data, "question": serializer.data})
        except:
            return Response({"Error": "No Exam exist with this id"})


class CategoryStatsPerUserAPIView(APIView):

    def get(self, request, *args, **kwargs):
        categories = []
        res = []

        user = request.user
        if not user.is_school and (not user.is_superuser):
            return Response({'error': 'User must be a school or a staff user'}, status=status.HTTP_403_FORBIDDEN)
        student_id = kwargs.get('user')
        category_id = kwargs.get('category')
        try:
            student = User.objects.get(pk=student_id, parent__id=user.pk)
            category= models.QuestionCategoryModel.objects.get(pk=category_id)
        except (User.DoesNotExist, models.QuestionCategoryModel.DoesNotExist) as e:
            print(e)
            return Response({'error': 'User or category is not valid or not of this school'}, status=status.HTTP_400_BAD_REQUEST)

        children = [get_child(category)]
        parents = [get_parent(category)]

        for c in children+parents:
            categories += depth(c)

        questions = models.AnswerModel.objects.filter(
            student=user, is_correct=False, question__category__in=categories
        ).order_by('updated_at')

        for q in questions:
            _q = local_serializers.AnsweredQuestionSerializer(q)
            if _q:
                res.append(_q.data)

        return Response({'questions': res})


class StudentsDocumentViews(APIView, SchoolStudentsMixin):

    def get_stats(self, students, quiz, data):
        for student in students:
            exam = student.exammodel_set.all().order_by('-created_at')
            if not exam:
                continue
            exam = exam[0]
            qa = []
            for q in exam.exam_questions.all():
                correct_answer = None
                text = q.question.text
                if q.question:
                    if q.question.answer_choices:
                        text = f"""
                            {q.question.text} : {q.question.answer_choices}
                        """
                    answers = json.loads(q.question.answer_choices)
                    correct = re.sub('[\[""\]]', '', q.question.right_answers)
                    correct_answer = answers[correct]
                    if q.given_answer:
                        a = re.sub('[\[""\]]', '', q.given_answer)
                        if a == "1":
                            correct = "2"
                        elif a == "2":
                            correct = "1"
                        qa.append((text, answers.get(a), correct_answer))
                    else:
                        qa.append((text, None, correct_answer))

            if quiz.requires_video_completion:
                completed, media_stats = self.get_students_media_stats(student, quiz)
            else:
                media_stats = []

            data['students'].append(
                {
                    'pk': student.pk,
                    'data':
                    {
                        'first_name': student.first_name,
                        'last_name': student.last_name,
                        'exam': {
                            'id': exam.pk,
                            'score': exam.obtained_marks,
                            'total': exam.passing_marks,
                            'questions': qa
                        },
                        'medias': media_stats
                    }
                }
            )
        return data

    def post(self, request, *args, **kwargs):
        data = {}
        data['students'] = []
        user = request.user
        quiz_id = kwargs.get('quiz_id')
        if not user.is_school:
            return Response({'error': 'User must be a school'}, status=status.HTTP_403_FORBIDDEN)
        try:
            quiz = QuizModel.objects.get(pk=quiz_id)
            ids = request.data.get('students')
            students = user.child.filter(pk__in=ids)
            if not students:
                return Response([], status=status.HTTP_404_NOT_FOUND)
            data['quiz'] = local_serializers.QuizSerializer(quiz).data
            data['school_name'] = user.school_name
        except QuizModel.DoesNotExist as e:
            return Response('No quiz was found', status=status.HTTP_404_NOT_FOUND)

        doc = DocxTemplate('project_apps/quiz_data/document/template.docx')
        data = self.get_stats(students, quiz, data)

        doc.render(data)
        doc.save("static/docs/generated_doc.docx")
        with open('static/docs/generated_doc.docx', 'rb') as f:
            response = HttpResponse(f, content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            response['Content-Disposition'] = 'attachment; filename=generated_doc.docx'

        return response

    def get(self, request, *args, **kwargs):
        data = {}
        data['students'] = []
        user = request.user
        quiz_id = kwargs.get('quiz_id')
        student_id = request.GET.get('student_id')
        mime = MimeTypes()
        if not user.is_school:
            return Response({'error': 'User must be a school'}, status=status.HTTP_403_FORBIDDEN)
        try:
            quiz = QuizModel.objects.get(pk=quiz_id)
            student = user.child.get(pk=student_id)
            data['quiz'] = local_serializers.QuizSerializer(quiz).data
            data['school_name'] = user.school_name
        except (QuizModel.DoesNotExist, User.DoesNotExist) as e:
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

        doc = DocxTemplate('project_apps/quiz_data/document/template.docx')
        data = self.get_stats([student], quiz, data)

        doc.render(data)
        doc.save("static/docs/generated_doc.docx")
        with open('static/docs/generated_doc.docx', 'rb') as f:
            response = HttpResponse(f, content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            response['Content-Disposition'] = 'attachment; filename=generated_doc.docx'

        return response
