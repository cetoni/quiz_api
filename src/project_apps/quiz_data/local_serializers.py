from django.contrib.auth import get_user_model
from django.db.models import F
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from . import models

User = get_user_model()


def check_quiz(id):
    try:
        return models.QuizModel.objects.get(id=id)
    except models.QuizModel.DoesNotExist:
        raise ValidationError('No Quiz Object exists with given quiz id')


class QuestionCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.QuestionCategoryModel
        # fields = '__all__'
        exclude = ('created_at', 'updated_at')


class ExerciseInitializeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.QuizModel
        fields = ['categories']


class QuestionAnswerSerializer(serializers.ModelSerializer):
    right_answers = serializers.SerializerMethodField('get_right_answer_json_choices')
    answer_choices = serializers.SerializerMethodField('get_answer_json_choices')

    def get_answer_json_choices(self, obj):
        return obj.answer_choices_json

    def get_right_answer_json_choices(self, obj):
        return obj.right_answers_json

    class Meta:
        model = models.QuestionModel
        depth = 1
        fields = ('id', 'text', 'image', 'answer_choices', 'right_answers', 'explanation', 'difficulty')


class ToBeAnswerSerializer(serializers.ModelSerializer):
    question = QuestionAnswerSerializer()
    class Meta:
        model = models.AnswerModel
        fields = ('id', 'question', 'exam',)


class AnsweredQuestionSerializer(serializers.ModelSerializer):
    question = QuestionAnswerSerializer()
    given_answer = serializers.SerializerMethodField('get_json_given_answer')

    def get_json_given_answer(self, obj):
        return obj.given_answer_json

    class Meta:
        model = models.AnswerModel
        exclude = ('created_at', 'updated_at', 'exam', 'student')
        # fields = ('question', 'exam', 'given_answer', 'is_correct')


class ExamSerializer(serializers.ModelSerializer):
    exam_questions = ToBeAnswerSerializer(many=True)
    exam_duration = serializers.SerializerMethodField()

    def get_exam_duration(self, obj):
        return obj.get_exam_duration_seconds

    class Meta:
        model = models.ExamModel
        fields = ('exam_id', 'student', 'exam_questions', 'quiz', 'created_at', 'exam_duration', 'is_over')
        read_only_fields = ['exam_questions']


class ExamResultSerializer(serializers.ModelSerializer):
    exam_questions = AnsweredQuestionSerializer(many=True)

    class Meta:
        model = models.ExamModel
        exclude = ('created_at', 'updated_at')
        read_only_fields = ['exam_questions']


class ExamStatsSerializer(serializers.ModelSerializer):
    error = serializers.SerializerMethodField()

    # student = serializers.SerializerMethodField()

    def get_error(self, obj):
        return obj.exam_questions.all().annotate(given=F('given_answer'), right=F('question__right_answers')).exclude(
            given=F('right'), given__isnull=False).count()

    # def get_student(self, obj):
    #     return UserSerializer(instance=obj.student).data

    class Meta:
        model = models.ExamModel
        fields = ('exam_id', 'quiz', 'result', 'obtained_marks', 'error', 'created_at')


class PassingMarksSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.QuizModel
        fields = ('passing_marks',)


class StatSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ExamModel
        fields = ('exam_id', 'passing_marks', 'obtained_marks', 'created_at')
        # fields = '__all__'


class PostAnswerSerializer(serializers.Serializer):
    questions = serializers.ListField()
    answers = serializers.ListField()


class ExamInitializerSerializer(serializers.ModelSerializer):
    quiz = serializers.IntegerField(required=True, validators=[check_quiz])

    class Meta:
        model = models.ExamModel
        fields = ['quiz']

    def validate(self, attrs):
        attrs['quiz'] = check_quiz(attrs['quiz'])
        return attrs


class QuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.QuizModel
        fields = ['id', 'title', 'description']


class QuestionCategoryHierarchySerializer(serializers.ModelSerializer):
    child = serializers.SerializerMethodField()

    def get_child(self, obj):
        return self.__class__(obj.child.all(), many=True).data

    class Meta:
        model = models.QuestionCategoryModel
        fields = ['id', 'title', 'child', 'color']


class QuizCategoryHierarchySerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField(read_only=True)

    def get_categories(self, obj):
        return QuestionCategoryHierarchySerializer(obj.categories.filter(category__pk__isnull=True), many=True).data

    class Meta:
        model = models.QuizModel
        fields = ['id', 'title', 'description', 'categories']


class GetExamSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AnswerModel
        fields = '__all__'
