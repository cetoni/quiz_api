# Generated by Django 2.2.1 on 2020-03-12 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz_data', '0006_auto_20200310_2040'),
    ]

    operations = [
        migrations.AddField(
            model_name='jitsiplugin',
            name='room_name',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
