from rest_framework.routers import DefaultRouter
from django.urls import path
from . import rest_views
from project_apps.quiz_data.apps import QuizDataConfig

app_name = QuizDataConfig.name

router = DefaultRouter(trailing_slash=True)

urlpatterns = [
    path('quiz_list/', rest_views.QuestionCategoryListView.as_view(), name='quiz_list'),
    path('start_exercise/', rest_views.StartExerciseAPIView.as_view(), name='start_exercise'),
    path('start_exam/', rest_views.ExamStartAPIView.as_view(), name='start_exam'),
    path('error_review/<quiz>/', rest_views.ErrorReviewAPIView.as_view(), name='error_review_quiz'),
    path('error_review/category/<category>/', rest_views.ErrorReviewAPIView.as_view(), name='error_review_quiz'),
    path('error_review/category/<category>/<user_id>/', rest_views.ErrorReviewAPIView.as_view(), name='error_review_quiz_per_user'),
    path('ongoing_exam/<exam_id>/', rest_views.ErrorReviewAPIView.as_view(), name='error_review_exam'),
    path('post_answers/', rest_views.ExercisePostAnswersView.as_view(), name='post_answers'),
    path('post_answers/<exam_id>/', rest_views.ExercisePostAnswersView.as_view(), name='post_answers_reference'),
    path('stats/<quiz>/', rest_views.StatsAPIView.as_view(), name='stats'),
    path('stats/<quiz>/topic_error/<category>/', rest_views.StatsAPIView.as_view(), name='stats'),
    path('stats/<category>/', rest_views.CategoryStatsAPIView.as_view(), name='category_stats'),
    path('stats/<category>/<user>/', rest_views.CategoryStatsPerUserAPIView.as_view(), name='category_stats_per_user'),
    path('exam_stats/<quiz>/', rest_views.StudentExamStatsAPIView.as_view(), name='exam_stats'),
    path('school_students_stats/<int:quiz>/', rest_views.SchoolStudentExamStatsAPIView.as_view(), name='school_students_stats'),
    path('category_hierarchy/<id>/', rest_views.CategoryHierarchyAPIView.as_view(), name='category_hierarchy'),
    path('get_exam/<exam_id>/', rest_views.GetExam.as_view(), name='Get_exam'),
    path('student_doc/<quiz_id>/', rest_views.StudentsDocumentViews.as_view(), name='student_document')
]
