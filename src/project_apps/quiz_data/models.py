import json
from collections.abc import Iterable
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

# from project_apps.quiz_data.rest_views import get_child
from schedule.models import Event
from user.utils import OdooManager
from utils import models as util_models

User = get_user_model()

LEVEL_NAME = {
    1: 'area',
    2: 'chapter',
    3: 'topic',
    4: 'topic-child'
}


class QuestionCategoryModel(util_models.TimeStamp, util_models.TitleDescription):
    """
        Stores a single QuestionCategory, related to itself, each category can have one parent,
        a parent category can have multiple children (many-to-one relationship).

    """
    category = models.ForeignKey('self', verbose_name=_('Parent Category'), on_delete=models.CASCADE, blank=True,
                                 null=True, related_name='child')
    color = models.CharField(max_length=10)

    class Meta:
        verbose_name = _('Question Category')
        verbose_name_plural = _('Question Categories')

    @classmethod
    def get_child(cls, user, parent):
        """Return a QuerySet of QuestionCategoryModel, having AnswerModel instances belonging to the user.
        """
        return cls.objects.filter(question__answer__student=user, category_id__in=parent).distinct().order_by('id')

    def get_childs(self) -> list:
        """Return a list of child categories.
        """
        if not self.category:
            return []
        return self.category.all()

    @classmethod
    def get_children(cls, category):
        children = category.child.all()
        if not children:
            return category
        return [cls.get_children(child) for child in children] + [category]

    def __str__(self):
        return self.title


class QuestionModel(util_models.TimeStamp):
    """
        Stores a single QuestionModel, related to :model: `quiz_data.QuestionCategoryModel`,
        A question can be related to one category.
    """
    category = models.ForeignKey(QuestionCategoryModel, on_delete=models.PROTECT, verbose_name=_('Category'),
                                 related_name='question')
    text = models.TextField()
    explanation = models.TextField(blank=True, null=True)
    image = models.URLField(blank=True, null=True)
    answer_choices = models.TextField(help_text='For example - {"1": "2", "2": "2"}')  # a dictionary field
    right_answers = models.CharField(max_length=100,
                                     help_text='For example - ["1", "2"]')  # a list field containing keys from above dictionary
    class Meta:
        verbose_name = _('Question')

    def __str__(self):
        return str(self.category) + ', Question - ' + self.text

    @property
    def answer_choices_json(self):
        """Return self.answer_choices"""
        return json.loads(self.answer_choices)

    @property
    def right_answers_json(self):
        """Return self.right_answers"""
        return json.loads(self.right_answers)

    def clean(self):
        """
            Clean runs before self.save, make sure self.answer_choices and self.right_anwsers
            are correctly parsed.
            Raises ValidationError if json.JSONDecodeError.
        """
        try:
            answer_choices = json.loads(self.answer_choices)
        except json.JSONDecodeError as e:
            raise ValidationError('json dictionary conversion error in answer_choices')

        try:
            right_answers = json.loads(self.right_answers)
        except json.JSONDecodeError as e:
            raise ValidationError('json list conversion error in right_answers')

        if not set(right_answers).issubset(answer_choices.keys()):
            raise ValidationError('right_answers keys not in answer_choices')


class QuizModel(util_models.TimeStamp, util_models.TitleDescription):
    """
        Stores a single QuizModel, related to :model: `quiz_data.QuestionCategoryModel`,
        a quiz can have multiple categories (the relation is many-to-many).
        There is a post_save trigger that creates a Template in odoo whenever a quiz is created.
    """
    categories = models.ManyToManyField(QuestionCategoryModel, verbose_name=_('Question Category Set'),
                                        related_name='quiz')
    exam_duration = models.PositiveIntegerField(help_text=_('Exam Time in Minutes'))
    passing_marks = models.PositiveIntegerField()
    show_on_cms = models.BooleanField(_("Show on cms"), default=False)
    requires_video_completion = models.BooleanField(_("Video completion"), default=False)
    odoo_id = models.IntegerField(_("odoo id"), null=True, blank=True)

    class Meta:
        verbose_name = _('Quiz Definition')

    def __str__(self):
        return self.title

@receiver(post_save, sender=QuizModel)
def _post_save_receiver(sender, instance, created, **kwargs):
    """Creates a Template in odoo"""
    odoo = OdooManager().odoo_login()
    Template = odoo.env['odoo_partner.certificates.template']

    if instance and created:
        template_id = Template.create({
            "name": instance.title,
            "description": instance.description
        })
        instance.odoo_id = template_id
        instance.save()
        assert template_id

class SchoolQuizSubscriptionModel(util_models.TimeStamp):
    """
        Stores a single SchoolQuizSubscriptionModel, related to :model: `user.User`,
        and to :model: `QuizModel`, every user can have multiple subscriptions, a subscription is related to one quiz.
    """
    user = models.ForeignKey(User, models.CASCADE, verbose_name=_('School'))
    quiz = models.ForeignKey(QuizModel, models.CASCADE, verbose_name=_('Quiz'))

    class Meta:
        verbose_name = _('School Quiz Subscription')
        verbose_name_plural = _('School Quiz Subscriptions')

    def __str__(self):
        return "School - {}, Quiz - {}".format(self.user, self.quiz)


class ExamQuestionSet(util_models.TimeStamp, util_models.TitleDescription):
    """
        Stores a single ExamQuestionSet, related to :model: `quiz_data.QuizModel`,
        every quiz can have multiple ExamQuestionSet objects.
    """
    quiz = models.ForeignKey(QuizModel, on_delete=models.CASCADE, related_name='exam_set')

    def __str__(self):
        return "Quiz - %s, ExamSet %s " % (self.quiz, self.title)


class ExamSetQuestionSequence(models.Model):
    """
        Stores a single ExamSetQuestionSequence, related to :model: `quiz_data.ExamQuestionSet` (one ExamQuestionSet),
        and to one :model: `QuestionCategoryModel`.
    """
    exam_set = models.ForeignKey(ExamQuestionSet, on_delete=models.CASCADE, related_name='question_chapter')
    question = models.PositiveIntegerField()
    chapter = models.ForeignKey(QuestionCategoryModel, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('exam_set', 'question')

    def clean(self):
        categories = []
        all_categories = [child for category in self.exam_set.quiz.categories.filter(category__isnull=True) for child in QuestionCategoryModel.get_children(category)]
        for sublist in all_categories:
            if isinstance(sublist, Iterable):
                for item in sublist:
                    categories.append(item)
            else:
                categories.append(item)
        if not self.chapter in categories:
            raise ValidationError('Selected chapter should be a part of quiz')


class ExamModel(util_models.TimeStamp):
    """
        Stores a single ExamModel, related to :model: `user.User` and to :model: `quiz_data.QuizModel`.
    """
    exam_id = models.AutoField(primary_key=True)
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(QuizModel, on_delete=models.SET_NULL, null=True)  # in exam
    result = models.CharField(max_length=50)
    passing_marks = models.PositiveIntegerField()
    obtained_marks = models.IntegerField()
    is_over = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Exam Stat')

    def __str__(self):
        return self.student.get_full_name()

    @property
    def get_exam_duration_seconds(self):
        return self.quiz.exam_duration * 60


class AnswerModel(util_models.TimeStamp):
    """
        Stores a single AnswerModel, related to :model: `user.User`, to :model: `quiz_data.ExamModel`
        and to :model: `quiz_data.QuestionModel`. \n
    """
    student = models.ForeignKey(User, on_delete=models.CASCADE, related_name='answered_questions')
    exam = models.ForeignKey(ExamModel, on_delete=models.CASCADE, related_name='exam_questions', blank=True, null=True)
    question = models.ForeignKey(QuestionModel, on_delete=models.SET_NULL, blank=True, null=True, related_name='answer')
    given_answer = models.CharField(max_length=20, blank=True, null=True)
    is_correct = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Answered Questions')

    def __str__(self):
        return self.student.get_full_name()

    @property
    def given_answer_json(self):
        return json.loads(self.given_answer if self.given_answer else "[]")



class Booking(models.Model):
    event = models.OneToOneField(Event, on_delete=models.CASCADE, null=True)
    student = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
