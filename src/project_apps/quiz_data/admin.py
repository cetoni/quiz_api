from django import forms
from django.contrib import admin
from django.contrib.messages import constants as messages

from project_apps.adaptive_quiz.admin import DifficultyInline
from . import models


class QuestionCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'category', 'get_title', 'get_quiz')
    search_fields = ('category__id',)
    actions = ['duplicate_record']

    def get_title(self, obj):
        return obj.title

    def get_quiz(self, obj):
        quiz = models.QuizModel.objects.get(categories=obj.pk)
        return quiz.title

    get_title.short_description = 'Title'
    get_quiz.short_description = 'Quiz'


class QuestionAdminForm(admin.ModelAdmin):
    list_display = ('id', 'category', 'text')
    search_fields = ('text',)
    list_filter = ('category',)
    inlines = [
        DifficultyInline,
    ]
    actions = ['duplicate_record']


class ExamQuestionSequenceForm(admin.TabularInline):
    model = models.ExamSetQuestionSequence
    extra = 40
    list_filter = ('chapter', 'exam_set')
    actions = ['duplicate_record']


class AnsweredQuestions(admin.TabularInline):
    model = models.AnswerModel
    extra = 0
    actions = ['duplicate_record']


class ExamSetAdminForm(admin.ModelAdmin):
    list_display = ('quiz',)
    list_filter = ('quiz',)
    inlines = [
        ExamQuestionSequenceForm
    ]
    actions = ['duplicate_record']


class ExamAdminForm(admin.ModelAdmin):
    list_display = ('quiz', 'student')
    # list_filter = ('student', 'exam_id')
    # inlines = [
    #     AnsweredQuestions
    # ]
    actions = ['duplicate_record']


class ExamSetQuestionSequenceAdminView(admin.ModelAdmin):
    list_display = ('exam_set', 'question', 'chapter')
    list_filter = ('exam_set', 'chapter')
    actions = ['duplicate_record']


class AnswerAdminView(admin.ModelAdmin):
    list_display = ('id', 'question', 'student')
    raw_id_fields = ('question', 'student')
    # list_filter = ('student', 'exam', 'is_correct')
    search_fields = ('id',)
    actions = ['duplicate_record']


def duplicate_record(self, request, queryset):
    errors = []
    i = 0

    for obj in queryset:
        try:
            duplicate = obj
            obj.pk = None
            obj.save()
            i += 1
        except Exception as e:
            print(e)
            errors.append(dict(error=f'Could not duplicate {obj}, error = {e}'))
            pass
    if not len(errors):
        self.message_user(request, f"{i} records have been successfully duplicated", level=messages.SUCCESS)
    elif len(errors) == len(queryset):
        self.message_user(request, f"{i} / {len(queryset)} records have been duplicated, error = {errors}",
                          level=messages.ERROR)
    else:
        self.message_user(request, f"{i} / {len(queryset)} records have been duplicated, error = {errors}",
                          level=messages.WARNING)


duplicate_record.short_description = "Duplicate selected records"

admin.site.add_action(duplicate_record)
admin.site.disable_action('duplicate_record')

admin.site.register(models.ExamQuestionSet, ExamSetAdminForm)
admin.site.register(models.QuestionModel, QuestionAdminForm)
admin.site.register(models.ExamModel, ExamAdminForm)
admin.site.register(models.ExamSetQuestionSequence, ExamSetQuestionSequenceAdminView)
admin.site.register(models.AnswerModel, AnswerAdminView)
admin.site.register(models.QuestionCategoryModel, QuestionCategoryAdmin)
admin.site.register(models.QuizModel)
admin.site.register(models.SchoolQuizSubscriptionModel)
