import collections
import re

from cms.api import add_plugin, create_page, get_cms_setting, publish_page
from cms.models import Page, Placeholder, TreeNode, UserSettings
from cms.models.pluginmodel import CMSPlugin
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand, CommandError
from project_apps.quiz_data.cms_plugins import (CalendarSpa, JitsiPublisher,
                                                LegacyQuizAM, LegacyQuizB,
                                                LegacyQuizCDE,
                                                LegacyQuizRevisioni,
                                                QuizPlugin, JitsiPlugin)
from user.admin import SiteProfileAdmin
from user.models import SiteProfile, User


class Command(BaseCommand):

    def create_site_profile(self, obj):
        pages = []
        data = {}
        page = None
        regex = re.compile('[^a-zA-Z]')
        obj.save()
        site = obj.site
        pages_data = [
            {
                'data': {'title': 'Lezioni a Distanza', 'login_required': False, 'slug': 'lezioni-distanza', 'template': '_cms/video.html'},
                'plugin': JitsiPublisher,
                'placeholder': 'videoconf area'
            },
            {
                'data': {'title': 'Lezioni a Distanza con password', 'login_required': True, 'slug': 'lezioni-distanza-password', 'template': '_cms/video.html'},
                'plugin': JitsiPublisher,
                'placeholder': 'videoconf area'
            },
            {
                'data': {'title': 'Quiz CQC', 'slug': 'quiz-cqc', 'template': '_cms/legacy/quiz.html'},
                'plugin': QuizPlugin,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz AM', 'slug': 'quiz-am', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizAM,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz B', 'slug': 'quiz-b', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizB,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz Revizioni', 'slug': 'quiz-revizioni', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizRevisioni,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz CDE', 'slug': 'quiz-cde', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizCDE,
                'placeholder': 'content'
            },
            {
                'data': {'title': ' Calendario Guide', 'slug': 'calendario-guide', 'template': '_cms/legacy/quiz.html'},
                'plugin': CalendarSpa,
                'placeholder': 'content'
            }
        ]
        for p in pages_data:
            pages.append({
                'page': create_page(**p['data'], language='it', menu_title=p['data']['title'], published=True, site=site, in_navigation=True),
                'data': p
            })
        
        pages[0]['page'].set_as_homepage()
        try:
            for page in pages:
                template = page['page'].get_template()
                placeholder = page['page'].placeholders.get(slot=page['data']['placeholder'])
                plugin_class = page['data']['plugin']
                if plugin_class == JitsiPublisher:
                    room_name = site.domain + '' + 'Lezioni a Distanza' + 'VideoConf'
                    room_name_lower = regex.sub('', room_name)
                    data = {'room_name': room_name_lower, 'site': site}
                else:
                    data = {}
                add_plugin(placeholder=placeholder, plugin_type=page['data']['plugin'], language='it', **data)

                publish_page(page=page['page'], user=obj.user, language='it')
        except Placeholder.DoesNotExist as e:
            print(e)

    def handle(self, *args, **kwargs):
        domain_names = ['api.editricetoni.it','schoolcms-demo.daphne-solutions.com', 'new.scuolaguidafederica.it', 
            'new.autoscuolaalvolante.it', 'new.autoscuolaburnout.it', 'new.autoscuoladecar.it', 'new.autonauticaaurora.it', 
            'new.autoscuolabertolini.it', 'www.autoscuolamaria.it', 'new.autoscuolapegaso.it', 'new.autoscuolaamnia.it', 
            'new.autoscuolaceragioli.it', 'lezioni-a-distanza.editricetoni.it', 'new.autoscuolamarinelli.it', 'www.autoscuoladelprete.it', 
            'www.autoscuolaalpifontane.it', 'www.spezzina.it', 'www.autoscuoleducale.it', 'www.autoscuolalanciani.it', 'www.autoscuolanuova.it', 
            'www.autoscuolavaratella.it', 'www.autospiv.it', 'new.autoscuolabiondo.it', 'www.autoscuoladalesio.it', 'www.opilezioni.it', 
            'www.autoscuoletoni.it', 'cybersecurity.daphne-solutions.com', 'coop.imparo-da-casa.it', 'www.autoscuolafiniletti.it', 
            'www.autoscuolacentralevallecrosia.it', 'www.7lidilezionionline.it', 'www.autoscuola2000nonantola.it', 'www.zambonlezioni.it', 
            'www.autoscuolamarcellino.it', 'www.mantolezioni.it', 'anas-formapiemonte.imparo-da-casa.it', 'www.autoscuolafurno.it', 
            'www.autoscuolarossidoriafoligno.it', 'www.autoscuolapreti.it', 'www.autoscuolablucuneo.it', 'www.autoscuolaeuropa-lezioni.it']

        SiteProfile.objects.all().delete()
        Site.objects.all().delete()
        TreeNode.objects.all().delete()
        Page.objects.all().delete()
        UserSettings.objects.all.delete()
        JitsiPlugin.objects.all().delete()
        CMSPlugin.objects.all().delete()


        admin = User.objects.get(email='admin@example.com')
        for d in domain_names[:4]:
            site = Site.objects.create(domain=d, name=d)
            site_p = SiteProfile(user=admin, site=site)
            obj = self.create_site_profile(site_p)
            print(obj)