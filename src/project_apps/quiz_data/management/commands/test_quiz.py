from django.core.management.base import BaseCommand
from django.test import Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from project_apps.quiz_data.models import QuizModel

User = get_user_model()

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--test')

    def handle(self, *args, **kwargs):
        if kwargs.get('test') == 'quiz':
            quiz = QuizModel.objects.get(title='B 2016')
            print(quiz.pk)
            for category in quiz.categories.all():
                print(category.id, category.title)