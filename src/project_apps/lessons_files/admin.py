from django.contrib import admin
from filer.admin.fileadmin import FileAdmin

from project_apps.lessons_files.models import Media, MediaStats, MediaStatsCount

class MediaAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'owner')
    search_fields = ['category', 'owner']
    raw_id_fields = ('owner', 'category')
    readonly_fields = ('sha1',)
    fieldsets = (
        (None, {
            'fields': ('name', 'owner', 'category', 'file', 'duration')
        }),
        ('Folder', {
            'fields': ('folder',)
        }),
    )


class MediaStatsAdmin(admin.ModelAdmin):
    list_display = ('user', 'media', 'watched_duration', 'created_at')
    list_filter = ('user',)
    search_fields = ('user', 'media')
    raw_id_fields = ('user', 'media')


class MediaStatsCountAdmin(admin.ModelAdmin):
    list_display = ('user', 'media', 'watched_entirely', 'total_watched')
    list_filter = ('user',)
    search_fields = ('user', 'media')
    raw_id_fields = ('user', 'media')


admin.site.register(Media, MediaAdmin)
admin.site.register(MediaStats, MediaStatsAdmin)
admin.site.register(MediaStatsCount, MediaStatsCountAdmin)
