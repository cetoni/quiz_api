from django.db.models import Sum
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

from project_apps.lessons_files.models import MediaStats, Media, MediaStatsCount

User = get_user_model()

class Command(BaseCommand):
    help = "update media stats count"

    def handle(self, *args, **kwargs):

        medias = Media.objects.all()
        users = User.objects.all().order_by('id')

        for user in users:
            media_stats = user.media_stats.values('media_id').order_by('media_id').annotate(total_watched=Sum('watched_duration'))
            for media_stat in media_stats:
                entirely = False
                media_id = media_stat['media_id']
                user_watched = media_stat['total_watched']
                media = Media.objects.get(id=media_id)

                if user_watched >= media.duration:
                    entirely = True
                try:
                    media_stats_count = MediaStatsCount.objects.get(user=user, media=media)
                    media_stats_count.total_watched = user_watched
                    media_stats_count.watched_entirely = entirely

                except MediaStatsCount.DoesNotExist as e:
                    media_stats_count = MediaStatsCount(user=user, media=media, total_watched=user_watched, watched_entirely=entirely)

                media_stats_count.save()
                print(media_stats_count)
