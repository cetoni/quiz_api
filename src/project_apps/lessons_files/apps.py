from django.apps import AppConfig


class LessonsFilesConfig(AppConfig):
    name = 'lessons_files'
