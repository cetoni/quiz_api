# Generated by Django 2.2.1 on 2020-06-10 13:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lessons_files', '0004_auto_20200604_1926'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mediastats',
            name='media',
        ),
        migrations.AddField(
            model_name='mediastats',
            name='media',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_stats', to='lessons_files.Media', verbose_name='Media'),
        ),
        migrations.RemoveField(
            model_name='mediastats',
            name='user',
        ),
        migrations.AddField(
            model_name='mediastats',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_stats', to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
    ]
