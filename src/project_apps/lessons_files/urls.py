from django.urls import path

from project_apps.lessons_files.views import ListMedia, MediaStatsView
from project_apps.lessons_files.apps import LessonsFilesConfig

app_name = LessonsFilesConfig.name

urlpatterns = [
    path('<category_id>/', ListMedia.as_view(), name='media_list'),
    path('track/<media_id>/', MediaStatsView.as_view(), name='media_stats')
]