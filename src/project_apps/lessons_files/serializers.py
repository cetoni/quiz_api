from rest_framework import serializers

from project_apps.lessons_files.models import Media, MediaStats
from project_apps.quiz_data.rest_views import SchoolStudentsMixin

class MediaSerializer(serializers.ModelSerializer, SchoolStudentsMixin):
    extension = serializers.SerializerMethodField()
    watched_percentage = serializers.SerializerMethodField()

    class Meta:
        model = Media
        fields = '__all__'
        
    def get_extension(self, obj):
        return obj.extension

    def get_watched_percentage(self, obj):
        user = self.context['request'].user
        category = obj.category
        data = self.get_media_viewing(user=user, media=obj)
        return data
    
class MediaStatsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = MediaStats
        fields = '__all__'
