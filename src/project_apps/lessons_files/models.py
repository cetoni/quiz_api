
import os

from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication
from filer.models.filemodels import File

from project_apps.quiz_data.models import QuestionCategoryModel
from user.models import User

from filer import settings as filer_settings

jwt = JWTTokenUserAuthentication()

class Media(File):
    
    category = models.ForeignKey(QuestionCategoryModel, on_delete=models.SET_NULL, blank=True, null=True)
    duration = models.FloatField(_("duration"))

    def has_read_permission(self, request):
        header = jwt.get_header(request)
        raw_token = jwt.get_raw_token(header)
        validated_token = jwt.get_validated_token(raw_token)
        user = jwt.get_user(validated_token)
        
        if not user.is_authenticated():
            return False
        return True
    
class MediaStats(models.Model):

    media = models.ForeignKey(Media, verbose_name=_("Media"), related_name="media_stats", on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name='media_stats', on_delete=models.CASCADE, blank=True, null=True)
    watched_duration = models.FloatField(_("Watched duration"))
    created_at = models.DateTimeField(auto_now_add=True)


class MediaStatsCount(models.Model):

    user = models.ForeignKey(User, verbose_name=_("user"), on_delete=models.CASCADE, null=True)
    media = models.ForeignKey(Media, verbose_name=_("media"), on_delete=models.CASCADE, null=True)
    watched_entirely = models.BooleanField(_("watched entirely"))
    total_watched = models.FloatField(_("total watched"))

    class Meta:
        unique_together = ('media', 'user')