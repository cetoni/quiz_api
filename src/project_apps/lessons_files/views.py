from copy import copy
from django.shortcuts import render

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from project_apps.lessons_files.models import Media, MediaStats
from project_apps.lessons_files.serializers import MediaSerializer, MediaStatsSerializer
from project_apps.quiz_data.models import QuestionCategoryModel

class ListMedia(APIView):
    """
        View to list all media files

        * default: requires authentication
    """

    def get(self, request, *args, **kwargs):
        """Return a list of files"""
        try:
            category_id = kwargs.pop('category_id')
            category = QuestionCategoryModel.objects.get(id=category_id)
        except KeyError as e:
            print(e)
            return Response(error='Must provide a category id', status=status.HTTP_400_BAD_REQUEST)
        except QuestionCategoryModel.DoesNotExist as e:
            print(e)
            return Response(error='Could not find a category with the given id', status=status.HTTP_404_NOT_FOUND)

        medias = Media.objects.filter(category=category)
        serializer = MediaSerializer(medias, many=True, context={'request': request})

        return Response(serializer.data)

class MediaStatsView(APIView):

    def check_tracking(self, user):
        if user and user.user_tracking:
            return True
        return False


    def post(self, request, *args, **kwargs):
        reponse = None
        user = request.user
        if not user:
            return Response(dict(error='No user was provided'), status=status.HTTP_400_BAD_REQUEST)
        try:
            data = copy(request.data)
            media_id = kwargs.get('media_id')
            watched_duration = data.pop('watched_duration')

            if watched_duration < 0:
                watched_duration = 0

            if user.parent and user.parent.is_school:
                user = user.parent

            if not self.check_tracking(user):
                return Response(dict(error='Tracking disabled for user'), status=status.HTTP_403_FORBIDDEN)

            media = Media.objects.get(id=media_id)
            media_stats_data = dict(watched_duration=watched_duration)
            serializer = MediaStatsSerializer(data=media_stats_data)
            serializer.is_valid(raise_exception=True)
            media_stats = serializer.save()

            media_stats.user = request.user
            media_stats.media = media
            media_stats.save()
            response = Response(serializer.data)
        except KeyError as e:
            print(e)
            response = Response(dict(error=f'Missing field {e}'), status=status.HTTP_400_BAD_REQUEST)
        except Media.DoesNotExist as e:
            response = Response(dict(error=str(e), status=status.HTTP_404_NOT_FOUND))

        return response