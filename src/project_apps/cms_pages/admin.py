from django.contrib import admin

from cms.extensions import PageExtensionAdmin
from cms.admin.pageadmin import PageAdmin
from cms.models.pagemodel import Page

from .models import RestrictedExtension

admin.site.register(RestrictedExtension, PageExtensionAdmin)