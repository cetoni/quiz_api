from django.apps import AppConfig

class AdaptiveQuizConfig(AppConfig):
    name = 'cms_pages'
