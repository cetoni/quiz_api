import os
import re
import gzip
import requests
from hashlib import sha256
from django.core.management import BaseCommand
from oauth2client.service_account import ServiceAccountCredentials
from user.models import SiteProfile

email = 'yes@daphne-solutions.com'
password = '^]xo[>We#TLD'
certificate = 'quiz-b-dfb4c-68e83f08f636.json'

def _get_access_token():
    """Retrieve a valid access token that can be used to authorize requests.

    :return: Access token.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        certificate, ['https://www.googleapis.com/auth/firebase'])
    access_token_info = credentials.get_access_token()
    return access_token_info.access_token

def create_version(site_id, headers):
    """Create a deployment version of the site
    https://firebase.google.com/docs/reference/hosting/rest/v1beta1/sites.versions

    :return: site name (string)
    """
    url = f'https://firebasehosting.googleapis.com/v1beta1/sites/{site_id}/versions'
    data = {
        "config": {
            "headers": [{
                "glob": "**",
                "headers": {"Cache-Control": "max-age=1800"}
            }]
        }
    }
    print(url)
    res = requests.post(url, headers=headers, json=data)
    # sites/dev-static-site-3155d/versions/9317e78dba5ec499
    print(res.text)
    site_name = res.json()['name']
    return site_name

def update_status(site_unique_id, headers, data={'status': 'FINALIZED'}):
    """Marks the status of the Version as FINALIZED.

    :return: Response object
    """
    url = f'https://firebasehosting.googleapis.com/v1beta1/{site_unique_id}?update_mask=status'
    res = requests.patch(url, headers=headers, json=data)
    return res

def release_for_deploy(site_unique_id, site_id, headers):
    """Creates a release for the Version.

    :return: Response object
    """
    url = f'https://firebasehosting.googleapis.com/v1beta1/sites/{site_id}/releases?versionName={site_unique_id}'
    res = requests.post(url, headers=headers)
    return res

def upload_hashed_files(data, site_unique_id, headers, domain_name):
    """Uploads files from /src/{domain_name} to Firebase hosting.

    :return: Response object
    :raises: ValueError if any file upload fails
    """
    url = f'https://firebasehosting.googleapis.com/v1beta1/{site_unique_id}:populateFiles'
    res = requests.post(url, headers=headers, json=data)
    print(res.text)
    upload_url = res.json()["uploadUrl"]
    for k, v in data['files'].items():
        filename = k.replace('/', '')
        url = f'{upload_url}/{v}'
        filepath = f'/src/static_sites/{domain_name}/{filename}.gz'
        headers['Content-Type'] = 'application/octet-stream'
        with open(filepath, 'rb') as stream:
            data = stream.read()
            res = requests.post(url, headers=headers, data=data)
            if res.status_code != 200:
                print(res.text)
                raise ValueError(res.text)
            else:
                print(res)
    return res

def get_hashed_file_data(domain_name):
    """Hashes files from /src/{domain_name}.

    :return: dict of files: {fielname:hashed file gzip content}
    """
    data = {'files': {}}
    for f in os.listdir(f'/src/static_sites/{domain_name}'):
        file_name = f'/{f}'
        if '.gz' in file_name or '.hashed' in file_name:
            continue
        filepath = f'/src/static_sites/{domain_name}/{f}'
        os.system(f'gzip -c {filepath} > {filepath}.gz')
        os.system(f"cat {filepath}.gz | openssl dgst -sha256 | sed 's/^.* //' > {filepath}.hashed")
        hash_file = f'{filepath}.hashed'
        with open(hash_file, 'r') as hashed:
            content = hashed.read()
            content = content.replace('\n', '')
            data["files"][file_name] = content
        os.remove(hash_file)
    return data

def delete_version(site_unique_id, headers):
    url = f'https://firebasehosting.googleapis.com/v1beta1/{site_unique_id}'
    res = requests.delete(url, headers=headers)
    return res

def create_site(project_name, site_name, headers, i):
    if i >= 2:
        return
    url = f'https://firebasehosting.googleapis.com/v1beta1/projects/{project_name}/sites?siteId={site_name}'
    res = requests.post(url, headers=headers)
    data = res.json()
    print(data, site_name)
    if res.status_code != 200:
        i+=1
        err_string = data['error']['message']
        status = data['error']['status']
        if res.status_code == 409 and status == 'ALREADY_EXISTS':
            suggestion = re.search('(?<=Site `)(.*)', err_string)
            site_id = suggestion.group().replace('` already exists.', '').split('/')[3]
            return site_id
        else:
            suggestion = re.search('(?<=like `)([\w\d-]*)', err_string)
            site_id = suggestion.group()
            return create_site(project_name, site_id, headers, i)
    name = data['name'].split('/')[3]
    return name

def list_version(site_id, headers):
    url = f'https://firebasehosting.googleapis.com/v1beta1/sites/{site_id}/versions'
    res = requests.post(url, headers=headers)
    return res

def list_sites(project_id, headers):
    url = f'https://firebasehosting.googleapis.com/v1beta1/projects/{project_id}/sites'
    res = requests.get(url, headers=headers)
    if res.status_code != 200:
        raise ValueError(res.text)
    data = res.json()
    return [o['name'].split('/')[3] for o in data['sites']]

def download_static_site(domain_name):
    res = os.system(f'wget --recursive --domains --no-parent --page-requisites --html-extension\
            --convert-links --restrict-file-names=windows --no-clobber --directory-prefix=/src/static_sites {domain_name}')
    return res

class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        deployed = []
        project_id = 'quiz-b-dfb4c'
        token = _get_access_token()
        headers = {'Authorization': f'Bearer {token}'}
        sites_list = list_sites(project_id, headers=headers)
        for site in sites_list:
            if site != 'quiz-b-dfb4c' and site != 'quiz-spa-react':
                print(site)
        # for profile in SiteProfile.objects.all():
        #     # if profile.firebase_site_id not in sites_list:
        #     site_name = 'cms' + '-' + profile.site.domain.split('.')[1]
        #     try:
        #         site_id = create_site(project_id, site_name, headers, 0)
        #         profile.firebase_site_id = site_id
        #         profile.save()
        #         site_id = profile.firebase_site_id
        #         # print(site_id)
        #     except Exception as e:
        #         # print(e)
        #         pass

        # for profile in SiteProfile.objects.filter(firebase_site_id__isnull=False):
        #     domain = f'http://{profile.site.domain}'
        #     res = download_static_site(domain)
        #     if res != 0:
        #         continue
        #     site_id = profile.firebase_site_id
        #     domain_name = profile.site.domain
        #     try:
        #         site_unique_id = create_version(site_id, headers)
        #         data = get_hashed_file_data(domain_name)
        #         upload_hashed_files(data, site_unique_id, headers, domain_name)
        #         res = update_status(site_unique_id, headers)
        #         res = release_for_deploy(site_unique_id, site_id, headers)
        #         deployed.append(site_id)
        #     except ValueError as e:
        #         print(site_id, e)

        print(deployed)
