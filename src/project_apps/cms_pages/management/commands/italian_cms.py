from cms.api import create_page, get_cms_setting, add_plugin, publish_page
from cms.models import Page
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        pages = Page.objects.all()
        for page in pages:
            page.update_languages(['it', 'en'])
