from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.constants import PUBLISHER_STATE_DIRTY
from cms.extensions import PageExtension, extension_pool

class RestrictedExtension(PageExtension):
    """An extension to set the require login filed on a cms page"""
    restricted = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if kwargs.pop('mark_page', True):
            self.get_page().update(login_required=True)
            self.get_page().title_set.update(publisher_state=PUBLISHER_STATE_DIRTY)  # mark page dirty
        return super().save(*args, **kwargs)

extension_pool.register(RestrictedExtension)
    