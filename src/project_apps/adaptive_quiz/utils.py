from datetime import date, datetime, timedelta

from django.conf import settings
from django.apps import apps

from user.models import User
from project_apps.quiz_data import models as m
from project_apps.adaptive_quiz.models import Difficulty, Level

class QuestionLevel():

    def __init__(self, question, category):
        self.levels = settings.ADAPTIVE_QUIZ_LEVELS
        self.answers_history = settings.ADAPTIVE_QUIZ_ANSWERS_SPAN
        self.category = category
        self.question = question

        self.partitions = __class__.partitions(self.levels)

    @classmethod
    def partitions(self, n):
        """
            Compute the score ranges for each level
            i.e.

            1: 100/i, 100/n*(n-1)
            2: 100/n*(n-1), 100/n*(n-2)
            ... n

            >>> QuestionLevel.partitions(4)
            [(1, (100, 75.0)), (2, (75.0, 50.0)), (3, (50.0, 25.0)), (4, (25.0, 0.0))]
        """
        partitions = []
        
        for i in range(1, n+1):
            partition = (i, (100, round(100/n*(n-i))))
            if not i == 1:
                partition = (i, (round(100/n*(n-i+1)), round(100/n*(n-i))))
            partitions.append(partition)
        
        return partitions
    
    def _get_last_answers(self):
        time_span = datetime.today()-timedelta(days=settings.ADAPTIVE_QUIZ_ANSWERS_SPAN)
        answers = self.question.answer.filter(created_at__gte=time_span, is_correct=True)
        return answers
    
    def _question_score(self):
        """
            Updates the Difficulty model with the score and the difficulty level of the question,
            score ranges are determined by self._pertitions
            
            >>> c = m.QuestionCategoryModel.objects.get(id=12)
            >>> q = m.QuestionModel(category=c)
            >>> q.save()
            >>> u = User.objects.get(username='moad')
            >>> for i in [True, False, True, True, True, False]:
            ...     a = m.AnswerModel(question=q, student=u, is_correct=i)
            ...     a.save()
            >>> ql = QuestionLevel(question=q, category=q.category)
            >>> score = ql._question_score()
            >>> score == ((2, (75.0, 50.0)), 66.67)
            True
            >>> q.delete()
            (1, {'adaptive_quiz.Difficulty': 0, 'quiz_data.QuestionModel': 1})
        """
        score = None
        epsilon = 1
        score_range = ()
        right_answers = self._get_last_answers()
        
        if len(self.question.answer.all()) == 0:
            correct_percentage = 0.0
        else:
            correct_percentage = round(len(right_answers)/len(self.question.answer.all())*100, 2)

        if correct_percentage < 0:
            correct_percentage = 0.0
        
        for s in self.partitions:
            l = [i for i in range(s[1][1], s[1][0]+1)]
            for i in l:
                if abs(correct_percentage-i) <= epsilon:
                    score_range = s
        return score_range, correct_percentage
    
    def set_level(self):
        """
            Creates a Difficulty instance for self.question
            >>> c = m.QuestionCategoryModel.objects.get(id=12)
            >>> q = m.QuestionModel(category=c)
            >>> q.save()
            >>> u = User.objects.get(username='moad')
            >>> for i in [True, False, True, True, True, False]:
            ...     a = m.AnswerModel(question=q, student=u, is_correct=i)
            ...     a.save()
            >>> ql = QuestionLevel(question=q, category=q.category)
            >>> ql.set_level()
            True
            >>> q.difficulty.level == 2
            True
            >>> q.delete()
            (2, {'adaptive_quiz.Difficulty': 1, 'quiz_data.QuestionModel': 1})
        """
        level, score = self._question_score()
        
        try:
            difficulty = Difficulty.objects.get(question=self.question)
            difficulty.score = score
            difficulty.level = level[0]
        except Difficulty.DoesNotExist as e:
            print(e)
            difficulty = Difficulty(question=self.question, score=score, level=level[0])
        
        difficulty.save()
        return True

class Scoring:

    def __init__(self, user, category):
        self.user = user
        self.category = category
        self.levels = settings.ADAPTIVE_QUIZ_LEVELS
        self.partitions = __class__.partitions(self.levels, settings.ADAPTIVE_QUIZ_SPAN)
    
    @classmethod
    def set_level(self):
        pass

    @classmethod
    def partitions(self, n, u):
        """
        n: int
        >>> Scoring.partitions(6, 2)
        [(1, (-inf, -4)), (2, (-4, -2)), (3, (-2, 0)), (4, (0, 2)), (5, (2, 4)), (6, (4, inf))]
        """
        p = []
        levels = []
        j = 1
        for i in range(-n, n, u):
            if i == -n:
                p.append((-float("inf"), i+u))
            elif i == n-u:
                p.append((i, float("inf")))
            else:
                p.append((i, i+u))
        for x in p:
            levels.append((j, x))
            j+=1
        return levels

    
    def negative_score(self, level):
        """
            n = levels
            
            1 -> -n
            2 -> -n+1
            ...
        """
        if level == 1:
            return -1*self.levels
        else:
            return level-(self.levels+1)
    
    def score(self, answer):
        """
            TODO: Fix doctest, doctests are not passing

            # Create or update a Level instance with score from answer
            # >>> u = User.objects.get(username='moad')
            # >>> c = m.QuestionCategoryModel.objects.get(id=12)
            # >>> l = user.skill_level.filter(category=c).first()
            # >>> l.delete()
            # >>> l = Level(user=u, category=c)
            # >>> l.save()
            # >>> q = m.QuestionModel(category=c)
            # >>> d = Difficulty(question=q, level=1)
            # >>> q.save()
            # >>> a = m.AnswerModel(question=q, student=u, is_correct=False)
            # >>> a.save()
            # >>> s = Scoring(user=u, category=c)
            # >>> s.score(a)
            # >>> u.skill_level.filter(category_id=c).first().score
            # >>> a.delete()
            # >>> q.delete()
        """
        level = None
        difficulty = answer.question.difficulty
        
        try:
            level = self.user.skill_level.filter(category=self.category).first()
        except Exception as e:
            print(e)
            level = Level(user=self.user, category=self.category)
            level.save()
        
        if answer.is_correct:
            level.score += difficulty.level
        else:
            level.score += self.negative_score(difficulty.level)
        
        level.save()

    def update_level(self):
        """
            Updates the user's level according to last 3 answers,
            if all correct increment the users level,
            else decrement
            >>> u = User.objects.get(username='moad')
            >>> c = m.QuestionCategoryModel.objects.get(id=12)
            >>> q = m.QuestionModel(category=c)
            >>> d = Difficulty(question=q)
            >>> q.save()
            >>> s = Scoring(user=u, category=c)
            >>> for i in range(3):
            ...     a = m.AnswerModel(question=q, student=u, is_correct=False)
            ...     a.save()
            ...     s.score(a)
            >>> s.update_level()
            >>> q.delete()
            (1, {'adaptive_quiz.Difficulty': 0, 'quiz_data.QuestionModel': 1})
        """
        done = False
        level = self.user.skill_level.filter(category=self.category).first()
        
        lowest = self.partitions[0]
        highest = self.partitions[self.levels-1]

        for (p, interval) in self.partitions[1:-1]:
            if level.score in range(*interval):
                level.level = p
                done = True

        if not done:
            if level.score < lowest[1][1]:
                level.level = lowest[0]
            elif level.score > highest[1][0]:
                level.level = highest[0]

        level.save()