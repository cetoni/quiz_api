from django.contrib import admin
from .models import Difficulty, Level

from user.models import User

# Register your models here.
class DifficultyInline(admin.StackedInline):
    model = Difficulty
    readonly_fields = ('score', 'level')

class LevelInline(admin.StackedInline):
    model = Level
    readonly_fields = ('score', 'level', 'category')
    extra = 0