from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from project_apps.quiz_data import models as m
from project_apps.adaptive_quiz.utils import QuestionLevel

class Command(BaseCommand):

    def handle(self, *args, **options):
        questions = []

        for c in m.QuestionCategoryModel.objects.all():
            print(f'category.id : {c.id}, questions : {len(c.question.all())}')
