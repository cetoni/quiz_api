from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from project_apps.quiz_data import models as m
from project_apps.adaptive_quiz.utils import QuestionLevel

class Command(BaseCommand):
    help = f'Updates question levels using score of the last {settings.ADAPTIVE_QUIZ_ANSWERS_SPAN}'

    def handle(self, *args, **options):
        i = 0
        questions = m.QuestionModel.objects.all()

        self.stdout.write(f'Updating questions levels, {len(questions)} questions to process...')
        for q in questions:
            try:
                cl = QuestionLevel(question=q, category=q.category)
                cl.set_level()
                i+=1
            except Exception as e:
                print(e.with_traceback())
                self.stdout.write(f'Exception: {e} for question {q.id}')
                continue

        self.stdout.write(f'Finished updating questions: {i}/{len(questions)} updated.')