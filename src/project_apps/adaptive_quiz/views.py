import random
import json

from django.conf import settings
from django.shortcuts import render
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from project_apps.quiz_data.models import QuestionModel, AnswerModel, QuestionCategoryModel
from project_apps.adaptive_quiz.models import Level
from project_apps.quiz_data.local_serializers import QuestionAnswerSerializer, AnsweredQuestionSerializer
from project_apps.adaptive_quiz.utils import Scoring
from project_apps.adaptive_quiz.adaptive_serializers import LevelSerializer
# Create your views here.

class AdaptedQuestion(APIView):
    """
        View to get questions and post anwsers.

        * Only authenticated users can access
    """

    def get(self, request, *args, **kwargs):
        """
            Return a question with level matching user's level,
            
            Args:
                kwargs['category'] (int): The category id
            
            route is GET adaptive/category/<category id>
        """
        user = request.user
        q = None
        unanswered = []
        message = {}

        if not kwargs.get('category'):
            return Response(
                dict(error='Must provide a category'), 
                status=status.HTTP_404_NOT_FOUND)

        category = kwargs.get('category')
        
        try:
            c = QuestionCategoryModel.objects.get(id=category)
            level = user.skill_level.get(category=category)
        except QuestionCategoryModel.DoesNotExist as e:
            print(e)
            return Response(
                dict(error='Could not find category')
            )
        except Level.DoesNotExist as e:
            print(e)
            # The user has no level, we start on score 0, level 1
            level = Level(user=user, category=c, score=0, level=1)
            level.save()
            # Give the user a random question since he had no level
            q = QuestionCategoryModel.objects.filter(category=category).order_by('?').first()
            
        try:
            # We first look for matching level, if not found greater level if not found smaller level
            qs = QuestionModel.objects.filter(Q(category=level.category, difficulty__level=level.level) 
                | Q(category=level.category, difficulty__level__gte=level.level) 
                | Q(category=level.category, difficulty__level__lte=level.level)).exclude(answer__student=user)
            if not len(qs):
                return Response(
                    dict(error='Could not find unanswered questions')
                )
        except Exception as e:
            print(e)
            return Response(
                dict(error='Could not find questions with given category')
            )
        if not q:
            q = random.choice(qs)
            if q.difficulty.level != level.level:
                message = dict(warning='Could not find matching difficulty')
                message['level'] = dict(question_level=q.difficulty.level, user_level=level.level)
        
        ans = AnswerModel.objects.create(student=user, question=q)
        data = dict(question=QuestionAnswerSerializer(q).data, message=message)

        return Response(data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        """
            Post an answer to a question.

            Args:
                request.data['question'] (int): The question's id
                
            route is POST adaptive/category/<category id >
        """
        serializer_class = LevelSerializer
        user = request.user
        message = {}
        
        if not request.data.get('question'):
            return Response(
                dict(error='Must provide a question id'),
                status=status.HTTP_404_NOT_FOUND
            )
        if not request.data.get('answer'):
            return Response(
                dict(error='Must provide an answer'),
                status=status.HTTP_404_NOT_FOUND
            )
        
        try:    
            question = QuestionModel.objects.get(id=request.data.get('question'))
        except Exception as e:
            print(e)
            return Response(
                dict(error='Could not find question with the given id')
            )

        q = QuestionModel.objects.get(id=request.data.get('question'))
        scoring = Scoring(user=user, category=q.category)
        
        if not q.answer.filter(student=user).first():
            ans = AnswerModel.objects.create(student=user, question=q)
        elif q.answer.filter(student=user).first().given_answer:
            return Response(
                dict(error='Question already answered')
            )
        
        ans = q.answer.filter(student=user).first()
        answer = list(map(str, [request.data.get('answer')]))
        answer_str = json.dumps(answer)
        ans.given_answer = answer_str
        if ans.question.right_answers == ans.given_answer:
            ans.is_correct = True
            
        ans.save()
        # Compute score
        scoring.score(ans)
        scoring.update_level()
        
        # Get next question based on user level
        level = user.skill_level.get(category=question.category)
        qs = QuestionModel.objects.filter(Q(category=level.category, difficulty__level=level.level) 
                | Q(category=level.category, difficulty__level__gte=level.level) 
                | Q(category=level.category, difficulty__level__lte=level.level)).exclude(answer__student=user)
        
        if len(qs):
            next_q = random.choice(qs)
            if next_q.difficulty.level != level.level:
                message = dict(warning='Could not find matching difficulty')
                message['level'] = dict(question_level=next_q.difficulty.level, user_level=level.level)

        serializer = serializer_class(user.skill_level.filter(category=q.category).first())
        result = dict(result=serializer.data, next=QuestionAnswerSerializer(next_q).data, message=message)
        return Response(result, status=status.HTTP_201_CREATED)
