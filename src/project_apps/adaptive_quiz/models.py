import random

from django.conf import settings
from django.db import models
from django.apps import apps
from django.db.models.signals import post_save
from django.dispatch import receiver

from user.models import User
from project_apps.quiz_data.models import QuestionCategoryModel, QuestionModel

# Create your models here.
class Level(models.Model):
    """
        Association model, a user can have many levels but on different categories,
        only one level is permitted per category
    """
    LEVELS = [i for i in range(1, settings.ADAPTIVE_QUIZ_LEVELS)]
    
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='skill_level')
    category = models.ForeignKey(QuestionCategoryModel, on_delete=models.CASCADE, related_name='skill_level')
    level = models.IntegerField(null=False, default=1) #TODO: Fix
    score = models.IntegerField(null=True, default=0)

    def __str__(self):
        return f'{self.category}'

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'category'], name='user_category_unique')
        ]
        
class Difficulty(models.Model):
    """
        A question can have only one difficulty level based on its score
    """
    question = models.OneToOneField(QuestionModel, on_delete=models.CASCADE, related_name='difficulty')
    level = models.IntegerField(null=False, default=1)
    score = models.IntegerField(null=True, default=100)

    def __str__(self):
        return f'{self.question.text}'

@receiver(post_save, sender=User)
def set_level(sender, instance, created, **kwargs):
    """Give a random level to user when created"""
    if created:
        categories = QuestionCategoryModel.objects.all()
        for c in categories:
            level = Level(user=instance, category=c)
            level.save()
