from rest_framework.routers import DefaultRouter
from django.urls import path
from . import views

app_name = 'adaptive_quiz'
router = DefaultRouter(trailing_slash=True)

urlpatterns = [
    path('category/<category>/', views.AdaptedQuestion.as_view(), name='adapted_question')
]