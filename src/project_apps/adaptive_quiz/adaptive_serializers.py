from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Level, Difficulty

User = get_user_model()

class LevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = ('user', 'score', 'level', 'category')
    
class DifficultySerialize(serializers.ModelSerializer):
    class Meta:
        model = Difficulty
        fields = ('level', 'score')