import unittest
import doctest

from django.test import TestCase
from project_apps.adaptive_quiz import utils

# Create your tests here.
def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(utils))
    return tests