from django.apps import AppConfig


class AdaptiveQuizConfig(AppConfig):
    name = 'adaptive_quiz'
