import base64
from constance import config

from ics import Calendar as ICalendar
from ics import Event as IEvent

from rest_framework.response import Response
from rest_framework import status

from user.models import User
from .models import Lesson
from .lessons_serializers import LessonSerializer

from utils.mail import mailjet

def generate_feed(event):
    cal = ICalendar()
    e = IEvent()
    e.name = event.title
    e.begin = event.start
    e.end = event.end
    e.description = event.description
    e.organizer = event.creator.school_name
    
    cal.events.add(e)
    with open('calendar.ics', 'w') as ics_file:
        ics_file.writelines(cal)

    content = open('calendar.ics', 'r').read()
    return content.encode()

def send_mail(name, email, attachement, variables={}):
    # Send a mail
    status = variables.get('status')
    if status == 'confirm':
        subject = "Your driving lesson is confirmed :) !"
    elif status == 'cancel':
        subject = 'Your driving lesson was canceled :( !'
    message = {
        'Messages' : [
            {
                "From": {"Email": config.EMAIL_SENDER, "Name": config.EMAIL_SENDER_NAME },
                "To": [{ "Email": email, "Name": name }],
                "Subject": subject,
				"TextPart": f"Dear {email}, your driving lesson appointement has been {status}",
                'Attachments':
                [{
                    "ContentType": "text/calendar",
                    "Filename": "calendar.ics",
                    "Base64Content": attachement.decode()
                }]
            }
        ]
    }
    result = mailjet.send.create(data=message)
    print(result)
    return result

def has_parent_school(user):
    try:
        if not user.is_school and not user.is_student:
            parent = user.parent
            if not parent or not parent.is_school:
                raise ValueError('User parent is not a school or has no parent')
        return True
    except User.DoesNotExist as e:
        print(e)
        raise ValueError('User does not have a parent')

def update_lesson_status(user, lesson_id, lesson_status=0, confirm='confirm'):
    """
        update lesson and return json response
        ---
        parameters:
            -   lesson_id: int
            -   lesson_status: string
    """
    try:
        if not lesson_id:
            return Response(dict(error='No lesson id was given'), status=status.HTTP_400_BAD_REQUEST)
        if confirm == 'confirm' and not lesson_status:
            return Response(dict(error='Need a lesson status'), status=status.HTTP_400_BAD_REQUEST)
        
        event = Lesson.objects.get(id=lesson_id)
        events_id = [event.id for event in user.creator.all()]
        if user.child:
            events_id = [event.id for child in user.child.all() for event in child.creator.all()] + [event.id for event in user.creator.all()]

        if has_parent_school(user):
            if user.is_teacher:
                parent = user.parent
                events_id = [event.id for child in parent.child.all() for event in child.creator.all()] + [event.id for event in parent.creator.all()]
            if event.id in events_id:
                if lesson_status and confirm == 'confirm':
                    event.status = 'booked'
                if confirm == 'cancel':
                    event.status = 'open'
                    event.participants.clear()
                event.save()
            else:
                return Response(dict(error='You do not have rights to update record'), status=status.HTTP_400_BAD_REQUEST)

        participant = event.participants.first() #NOTE: event is supposed to have only one participant
        # Send a confirmantion mail
        
        if participant:
            ics = generate_feed(event)
            attachement = base64.b64encode(ics)
            send_mail(participant.first_name, participant.email, attachement, variables={'status': confirm})

        serializer = LessonSerializer(event)
        return Response(serializer.data, status=status.HTTP_200_OK)

    except ValueError as e:
        print(e)
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)
    except User.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)
    except Lesson.DoesNotExist as e:
        return Response(str(e), status=status.HTTP_404_NOT_FOUND)
