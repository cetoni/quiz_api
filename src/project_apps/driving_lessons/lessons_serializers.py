from django.contrib.contenttypes.models import ContentType

from schedule.models import Event, EventRelation
from rest_framework import serializers

from user.models import User
from user.serializers import UserInfoSerializer
from .models import Lesson

class LessonSerializer(serializers.ModelSerializer):
    participants = serializers.SerializerMethodField()
    class Meta:
        model = Lesson
        fields = '__all__'

    def get_participants(self, obj):
        serializer = UserInfoSerializer(obj.participants, many=True)
        return serializer.data

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['instructor'] = ret['teacher']
        ret.pop('teacher')
        return ret

class DeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = '__all__'