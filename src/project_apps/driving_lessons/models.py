from django.db import models
from schedule.models import Event
from django_ical.views import ICalFeed
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from user.models import User

jwt = JWTTokenUserAuthentication()

class Lesson(Event):
    STATUS_CHOICES = [
        ('open', 'open'),
        ('requested', 'requested'),
        ('booked', 'booked'),
        ('done', 'done'),
        ('canceled', 'canceled')
    ]

    max_participants = models.IntegerField(_('Max participants'))
    status = models.CharField(_('Status'), choices=STATUS_CHOICES, default='open', max_length=100)
    teacher = models.ForeignKey(User, verbose_name=_("Teachers"), related_name='lessons', on_delete=models.CASCADE, null=True, blank=True)
    participants = models.ManyToManyField(User, verbose_name=_("Participants"), related_name='events', blank=True)

class LessonFeed(ICalFeed):
    
    """
    Lesson calendar
    """
    # product_id = '-//example.com//Example//EN'
    timezone = 'UTC'
    file_name = "event.ics"

    def items(self):
        events = []
        header = jwt.get_header(self.request)
        raw_token = jwt.get_raw_token(header)
        validated_token = jwt.get_validated_token(raw_token)
        user_id = jwt.get_user(validated_token).id
        
        user = User.objects.get(id=user_id)
        if user.is_authenticated:
            events = user.events.all()
        return events.order_by('-start')

    def item_guid(self, item):
        return "{}{}".format(item.id, "global_name")

    def item_title(self, item): 
        return "{}".format(item.title)

    def item_description(self, item):
        return item.description

    def item_start_datetime(self, item):
        return item.start
    
    def item_link(self, item):
        return f"http://api.editricetoni.it/lessons/{item.id}/"

    def __call__(self, request, *args, **kwargs):
        self.request = request
        return super(LessonFeed, self).__call__(request, *args, **kwargs)