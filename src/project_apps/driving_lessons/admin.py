from django.contrib import admin

from .models import Lesson
# Register your models here.
class LessonAdmin(admin.ModelAdmin):
    model = Lesson
    search_fields = ('teacher', 'creator',)
    raw_id_fields = ('teacher', 'creator',)

admin.site.register(Lesson, LessonAdmin)