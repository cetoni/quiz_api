from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin

class CalendarPlugin(CMSPluginBase):
    model = CMSPlugin
    name = 'Calendar'
    render_template = 'calendar_spa/src/index.html'