from rest_framework.routers import DefaultRouter
from django.urls import path

from . import views
from .models import Lesson, LessonFeed
from project_apps.driving_lessons.apps import DrivingLessonsConfig

app_name = DrivingLessonsConfig.name
router = DefaultRouter(trailing_slash=True)

urlpatterns = [
    path('', views.LessonsList.as_view(), name='lessons'),
    path('user/', LessonFeed()),
    path('date/<str:date>/', views.LessonsList.as_view(), name='lessons_per_date'), # TODO: change into a get param
    path('<int:id>/', views.LessonDetail.as_view(), name='event'),
    path('confirm/<int:id>/', views.LessonConfirmation.as_view(), name='confirm_lesson'),
    path('cancel/<int:id>/', views.LessonCancel.as_view(), name='cancel_lesson'),
]