from datetime import date, datetime, timedelta, timezone
from urllib.parse import urlparse

from dateutil import parser

from constance import config
from django.contrib.sites.models import Site
from django.shortcuts import render
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView
from schedule.models import Calendar, Event, EventRelation, Rule
from user.models import SiteProfile, User
from user.serializers import UserCreateSerializer, UserInfoSerializer

from utils.mail import mailjet

from .helpers import update_lesson_status, send_mail
from .lessons_serializers import DeleteSerializer, LessonSerializer
from .models import Lesson, LessonFeed

# TODO
# - response as ics

class LessonsList(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        """
            lessons of the site\'s owner
            ---
            returns list of lessons of the site's owner (SiteProfile),
            or the list of all lessons if site's owner's lessons are empty
            
        """
        lessons = []
        user = request.user
        date_obj = None
        limit = kwargs.get('date')
        if limit:
            try:
                date_obj = datetime.strptime(limit, "%Y-%m-%d")
            except ValueError as e:
                return Response('Invalid date format', status=status.HTTP_400_BAD_REQUEST)
                
        current = urlparse(request.META.get('HTTP_REFERER')).netloc

        if user.is_authenticated:
            parent = user.parent
            if parent and parent.is_school:
                lessons = Lesson.objects.filter(status='open', creator=parent)
                if date_obj:
                    lessons = lessons.filter(start__date=date_obj)
        else:
            try:
                site_profile = SiteProfile.objects.get(site__domain=current)
                site_owner = site_profile.user
                lessons = Lesson.objects.filter(creator=site_owner, status='open')
                
                if date_obj:
                    lessons = lessons.filter(start__date=date_obj)
        
            except SiteProfile.DoesNotExist as e:
                print(e)
                response = Response(dict(error='Site profile could not be found'), status=status.HTTP_404_NOT_FOUND)
 
        data = LessonSerializer(lessons, many=True).data
        return Response(dict(lessons=data))
    
    def post(self, request):
        """
            Create a new lesson only if user is_school or is_teacher
            ---
            parameters:
                -   name: title
                    description: string representing the title of the lesson
                    required: true
                    type: string
                    paramType: form
                -   name: start
                    description: starting datetime for lesson
                    required: true
                    type: date-time
                    paramType: form
                -   name: end
                    required: true
                    type: date-time
                    paramType: form
        """
        if request.user.is_school or request.user.is_teacher:
            cal = None
          
            user = request.user
            teacher_id = user.id
            try:
                if request.user.is_teacher:
                    teacher_id = request.user.id
                    user = request.user.parent
                    if not user.is_school:
                        return Response(dict(error='Parent is not a school'), status=status.HTTP_404_NOT_FOUND)
                
                cal = Calendar.objects.get_calendar_for_object(user) # TODO: will raise an error if there is more than one calendar
            except Calendar.DoesNotExist as e:
                print(e)
                return Response({'error': 'Calendar does not exist'}, status=status.HTTP_404_NOT_FOUND)
            try:
                start_data = request.data.get('start')
                end_data = request.data.get('end')
                title_data = request.data.get('title')
                max_participants = request.data.get('max_participants')
            except KeyError as e:
                print(e)
                return Response(f"Missing data {e.__cause__}", status=status.HTTP_400_BAD_REQUEST)

            start = parser.parse(start_data)
            end = parser.parse(end_data)
            if start.date() < date.today():
                return Response({'error': 'Date must be in the future'}, status=status.HTTP_400_BAD_REQUEST)
            if end < start:
                return Response({'error': 'End date cannot be before start'}, status=status.HTTP_400_BAD_REQUEST)    
            data = {
                'creator': user.id,
                'teacher': teacher_id,
                'title': title_data,
                'start': start,
                'end': end,
                'calendar': cal.id,
                'max_participants': max_participants 
            }
            serializer = LessonSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        return Response({'error': 'User must be a school'}, status=status.HTTP_400_BAD_REQUEST)

class LessonDetail(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        """ 
            Get one lesson by id
        """
        id = None
        try:
            id = kwargs.get('id')
        except KeyError as e:
            print(e)
            pass
        if id:
            event = Lesson.objects.filter(id=id).first()
            data = LessonSerializer(event).data
            return Response(data)
        return Response({'error': 'No Lesson id provided'})
    
    def put(self, request, *args, **kwargs):
        """
            Edit a lesson by id, must be the owner

            ---
            parameters:
            -   name: id
                description: lesson's id
                required: true
                type: int   
        """
        updated = {}

        try:
            event_id = kwargs.get('id')
            print(event_id)
            event = Lesson.objects.get(id=event_id)
            updated = {
                'title': request.data.get('title', event.title),
                'status': request.data.get('status', event.status),
                'max_participants': request.data.get('max_participants', event.max_participants),
                'description': request.data.get('description')
            }
        except KeyError as e:
            print(e)
            return Response({'error': 'Must provide a valid event id'}, status=status.HTTP_400_BAD_REQUEST)
        except Lesson.DoesNotExist as e:
            print(e)
            return Response(dict(error='No Lesson was found'), status=status.HTTP_404_NOT_FOUND)
        
        if request.user.is_school or request.user.is_teacher:
            cal = None
            user = request.user
            try:
                if request.user.is_teacher:
                    user = request.user.parent
                cal = Calendar.objects.get_calendar_for_object(user) # teachers can edit all events in school calendar
            except Calendar.DoesNotExist as e:
                print(e)
                return Response({'error': 'Calendar does not exist'})
            except User.DoesNotExist as e:
                print(e)
                return Response({'error': 'Parent school does not exist'})

            new_start = event.start
            new_end = event.end

            if request.data.get('start'):
                new_start = parser.parse(request.data['start'])
            if request.data.get('end'):
                new_end = parser.parse(request.data['end'])
                
            if new_start.date() < date.today():
                return Response({'error': 'Date must be in the future'}, status=status.HTTP_400_BAD_REQUEST)
            if new_end < new_start:
                return Response({'error': 'Date cannot be before start'}, status=status.HTTP_400_BAD_REQUEST)
            
            updated['start'], updated['end'] = new_start, new_end
            if event.creator == user or event.creator.parent == user:
                serializer = LessonSerializer(event, data=updated, partial=True)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                return Response(serializer.data)
            return Response('Event does not belong to user or it\'s parent', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'error': 'Not the owner of the calendar'})

    def delete(self, request, *args, **kwargs):
        """
            Delete one lesson, must be the owner
        """
        try:
            event_id = kwargs.get('id')
        except KeyError as e:
            print(e)
            return Response({'error': 'Must provide an event id'})
        if request.user.is_school or request.user.is_teacher:
            cal = None
            user = request.user
            try:
                if request.user.is_teacher:
                    user = request.user.parent
                cal = Calendar.objects.get_calendar_for_object(user) # teachers can delete all events in calendar
                event = Lesson.objects.get(id=event_id)
            except Calendar.DoesNotExist as e:
                print(e)
                return Response(dict(error='Calendar does not exist'),  status=status.HTTP_404_NOT_FOUND)
            except Lesson.DoesNotExist as e:
                print(e)
                return Response(dict(error='Lesson does not exist'), status=status.HTTP_404_NOT_FOUND)
            
            if event.creator == user or event.creator.parent == user:
                event.delete()
                serializer = DeleteSerializer(event)
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(dict(error='Event does not belong to user on it\'s parent'), status=status.HTTP_400_BAD_REQUEST)

class BookingLesson(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        
        """
            Book a lesson
            ---
            parameters:
            -   name: email
                type: string
            -   name: name
                type: string
            -   name: lesson_id
                type: integer    
        """
        user_data = {}
        user = request.user

        phone = request.data.get('phone')
        email = request.data.get('email')
        name = request.data.get('name')

        if user.is_authenticated:
            phone = user.phone
            email = user.email
            name = user.first_name
            if not name:
                name = user.last_name
            if not name:
                name = email
        else:
            user_data = {'name': name, 'email': email, 
                        'password': name, 'phone': phone,
                        'driving_licence_type': 1, 'is_student': True}

        lesson_id = request.data.get('lesson_id')

        if not email or not name or not lesson_id:
            return Response(
                dict(error="Email, name and lesson_id are required"),
                status=status.HTTP_400_BAD_REQUEST
            )
        
        try:
            event = Lesson.objects.get(id=lesson_id)
        except Lesson.DoesNotExist as e:
            print(e)
            return Response(
                dict(error="Could not find an open lesson"),
                status=status.HTTP_404_NOT_FOUND
            )
        if event.status != 'open':
            return Response(dict(error=f'Event status is {event.status}'), status=status.HTTP_404_NOT_FOUND)

        if user_data:
            try:
                user = User.objects.get(email=user_data.get('email'))
            except User.DoesNotExist as e:
                print(e)
                serializer = UserCreateSerializer(data=user_data)
                serializer.is_valid(raise_exception=True)
                user = serializer.save()
                user.is_active = False
                user.save()
            
        event.participants.add(user)
        event.status = 'requested'
        event.save()

        message = {
            'Messages' : [{
                    "From": { "Email": config.EMAIL_SENDER, "Name": config.EMAIL_SENDER_NAME },
                    "To": [{ "Email": email, "Name": name }],
                    "TemplateID": config.LESSON_BOOKING_TEMPLATE,
                    "TemplateLanguage": True,
                    "Variables": {
                        "school_name": event.creator.school_name,
                        "student_name": name,
                        # "student_phone": phone,
                        "email": email,
                        "slot_start": event.start.strftime("%Y-%m-%d %H:%M"),
                        "slot_end": event.end.strftime("%Y-%m-%d %H:%M")
                    }
            }]
        }
       
        result = mailjet.send.create(data=message)
        print(result.json())
        
        serializer = LessonSerializer(event)
        user_serializer = UserInfoSerializer(user)
        return Response(
            dict(
                event=serializer.data,
                user=user_serializer.data
            )
        )
class LessonConfirmation(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    
    def post(self, request, *args, **kwargs):
        user = request.user
        lesson_id = kwargs.get('id')
        lesson_status = request.data.get('approved')
        response = update_lesson_status(user=user, lesson_id=lesson_id, lesson_status=lesson_status, confirm='confirm')
        return response

class LessonCancel(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    
    def post(self, request, *args, **kwargs):
        user = request.user
        lesson_id = kwargs.get('id')
        response = update_lesson_status(user=user, lesson_id=lesson_id, confirm='cancel')
        return response
