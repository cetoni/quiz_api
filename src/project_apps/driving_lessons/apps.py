from django.apps import AppConfig


class DrivingLessonsConfig(AppConfig):
    name = 'driving_lessons'
