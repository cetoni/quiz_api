var HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.[s]?css$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|gif|pdf)$/i,
        use: ['url-loader']
      },
      {
        test: /\.svg$/,
        use: ['svg-inline-loader']
      },
    ]
  },
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'build')
  },
  plugins: [new HtmlWebpackPlugin({
    template: './src/index.html'
  })]
};

