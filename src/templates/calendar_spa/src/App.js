import React, { Component } from "react";
// import logo from './logo.svg';
// import './App.css';
// import Backend from 'react-dnd-html5-backend'
// import { DndProvider } from 'react-dnd'

// import Scheduler, {SchedulerData, ViewTypes, DATE_FORMAT} from 'react-big-scheduler'
//include `react-big-scheduler/lib/css/style.css` for styles, link it in html or import it here
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
import "moment/locale/it";

import axios from "axios";
import Modal from "./Components/Modal/Modal";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";

moment.locale("it");
const localizer = momentLocalizer(moment);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModal: false,
      events: [],
      currentEvent: null,
      nameError: false,
      emailError: false,
      phoneError: false,
    };
  }

  componentDidMount() {
    // console.log(this.props)
    //   const data = {
    //     instructor: "luie-test@mail.com",
    //     title: "Driving Lesson 3",
    //     start: new Date( 2020, 2, 26, 14, 10 ),
    //     end: new Date( 2020, 2, 26, 16, 40 ),
    //   }
    //   const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTg0ODUzOTQzLCJqdGkiOiI2ZGNkMzJhMWUwNTM0Yzg3YTNlMmFiMmQ1MDk3MDNjZSIsInVzZXJfaWQiOjJ9.oe3KnBE4uJjsmuRwB1Ru9hPsapJuISIw3rHm9rvj-Mw"
    //   axios.post('https://api.editricetoni.it/lessons/', data, {headers: {Authorization: `Bearer ${token}`}}).then( res => {
    //     console.log(res)
    //   })

    axios.get("https://api.editricetoni.it/lessons/").then((res) => {
      this.handleConvertDate(res.data);
    });
  }

  handleConvertDate = (data) => {
    // const { events } = this.state
    // converts ISO date to Javascript

    const copyEvents = data.lessons.map((event) => {
      event.start = new Date(event.start);
      event.end = new Date(event.end);
      return event;
    });

    // console.log(copyEvents)

    this.setState({
      events: copyEvents,
    });
  };

  handleOpenModal = (event) => {
    this.setState({
      isModal: true,
      currentEvent: event,
    });
  };

  handleCloseModal = () => {
    const { userDetails } = this.state;

    this.setState({
      isModal: false,
      currentEvent: null,
      userDetails: {
        ...userDetails,
        name: null,
        email: "",
        phone: "",
      },
      nameError: false,
      emailError: false,
      phoneError: false,
    });
  };

  render() {
    const { currentEvent, isModal } = this.state;

    return (
      <div className="widget__container">
        <Calendar
          messages={{
            next: "Avanti",
            previous: "Indietro",
            today: "Oggi",
            month: "Mese",
            week: "Settimana",
            day: "Giorno",
            agenda: "Agenda",
          }}
          selectable
          localizer={localizer}
          events={this.state.events}
          defaultView={Views.MONTH}
          scrollToTime={new Date(1970, 1, 1, 6)}
          defaultDate={new Date()}
          onSelectEvent={(event) => this.handleOpenModal(event)}
          style={{ height: this.props.calendarHeight || "750px" }}
        />

        {isModal && (
          <Modal
            currentEvent={currentEvent}
            handleCloseModal={this.handleCloseModal}
          />
        )}
      </div>
    );
  }
}

export default App;
