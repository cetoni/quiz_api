import React from "react";

import check from "../../assets/images/tick.png";
import "./BookingSuccess.scss";
import { t } from "../../i18n";

const BookingSuccess = (props) => {
  return (
    <div className={props.className}>
      <div className="bookingSuccess__container">
        <img className="bookingSuccess__img" src={check} alt="success" />
        <div className="bookingSuccess__desc">{t("bookSuccessDesc")}</div>
      </div>
    </div>
  );
};

export default BookingSuccess;
