import React, { Component } from "react";

import axios from "axios";
import "./Modal.scss";
import svgIcon from "../../assets/icons/sprite.svg";
import Spinner from "./../common/Spinner/Spinner";
import BookingSuccess from "../BookingSuccess/BookingSuccess";
import { t } from "../../i18n";

class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetails: {
        name: null,
        email: "",
        phone: "",
      },
      nameError: null,
      emailError: null,
      phoneError: null,
      bookingSuccess: false,
    };
  }

  // componentDidMount() {
  //     console.log(this.props)
  // }

  handleValidateForm = () => {
    const { userDetails } = this.state;
    // const stringPhone = userDetails.phone.toString()
    //Supported Regex format
    // (123) 456-7890
    // (123)456-7890
    // 123-456-7890
    // 123.456.7890
    // 1234567890
    // +31636363634
    // 075-63546725
    let testPhone = null;
    if (userDetails.phone) {
      testPhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(
        userDetails.phone
      );
    }

    let testEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      userDetails.email
    );

    let checkNameError, checkEmailError, checkPhoneError;

    if (!userDetails.name || userDetails.name.length < 4) {
      checkNameError = true;
    } else {
      checkNameError = false;
    }

    if (!userDetails.email || testEmail === false) {
      checkEmailError = true;
    } else {
      checkEmailError = false;
    }

    if (testPhone === false) {
      checkPhoneError = true;
    } else {
      checkPhoneError = false;
    }

    this.setState(
      {
        nameError: checkNameError,
        emailError: checkEmailError,
        phoneError: checkPhoneError,
      },
      this.handleBooking
    );
  };

  handleFormData = (event, input) => {
    const { userDetails } = this.state;

    this.setState({
      userDetails: {
        ...userDetails,
        [input]: event.target.value,
      },
    });
  };

  handleBooking = () => {
    const { userDetails, nameError, emailError } = this.state;
    //name
    //email
    //lesson_id
    //phone

    if (nameError === false && emailError === false) {
      const bookingData = {
        ...userDetails,
        lesson_id: this.props.currentEvent.id,
      };

      // console.log(bookingData)
      this.setState({
        loading: true,
      });

      axios
        .post("https://api.editricetoni.it/booking/", bookingData)
        .then((res) => {
          console.log(res);

          this.setState({
            bookingSuccess: true,
            bookingResponse: res.data,
            loading: false,
          });
        });
    } else {
      return;
    }
  };

  handleSendBookingData = () => {
    const { nameError, emailError, phoneError } = this.state;
    if (nameError === false && emailError === false && phoneError === false) {
      this.handleBooking();
    }
  };

  render() {
    const {
      nameError,
      emailError,
      phoneError,
      bookingSuccess,
      loading,
    } = this.state;

    const months = [
      "gennaio",
      "febbraio",
      "marzo",
      "aprile",
      "maggio",
      "giugno",
      "luglio",
      "agosto",
      "settembre",
      "ottobre",
      "novembre",
      "dicembre",
    ];
    let startTimeHours = this.props.currentEvent.start.getHours();
    let startTimeMinutes = this.props.currentEvent.start.getMinutes();

    let endTimeHours = this.props.currentEvent.end.getHours();
    let endTimeMinutes = this.props.currentEvent.end.getMinutes();

    let startTime = null;
    if (startTimeHours > 12) {
      startTimeHours = startTimeHours - 12;

      if (startTimeMinutes === 0) {
        startTime = startTimeHours + ":00PM";
      } else {
        startTime = startTimeHours + ":" + startTimeMinutes + "PM";
      }
    } else {
      if (startTimeMinutes === 0) {
        startTime = startTimeHours + ":00AM";
      } else {
        startTime = startTimeHours + ":" + startTimeMinutes + "AM";
      }
    }

    let endTime = null;
    if (endTimeHours > 12) {
      endTimeHours = endTimeHours - 12;

      if (endTimeMinutes === 0) {
        endTime = endTimeHours + ":00PM";
      } else {
        endTime = endTimeHours + ":" + endTimeMinutes + "PM";
      }
    } else {
      if (endTimeMinutes === 0) {
        endTime = endTimeHours + ":00AM";
      } else {
        endTime = endTimeHours + ":" + endTimeMinutes + "AM";
      }
    }

    return (
      <div className="modal__backdrop">
        <div className="modal__container">
          <div
            className="modal__closeBtn"
            onClick={this.props.handleCloseModal}
          >
            <svg className="modal__closeIcon" xlinkTitle="none">
              <use xlinkHref={`${svgIcon}#icon-cross`}></use>
            </svg>
          </div>

          <div className="modal__content">
            <div className="modal__contentHeader">
              {/* {bookingSuccess ? t("modalHeader2") : t("modalHeader1")} */}
              {bookingSuccess ? t("modalHeader2") : null}
            </div>

            {!bookingSuccess && !loading && (
              <div className="modal__contentBody">
                <div className="modal__contentBody--container">
                  {/* <div className="modal__contentBody--identifier">
                    {t("title")}:
                  </div> */}
                  <div>{this.props.currentEvent.title}</div>
                </div>

                <div className="modal__contentBody--container">
                  {/* <div className="modal__contentBody--identifier">
                    {t("description")}:
                  </div> */}
                  <div>{this.props.currentEvent.description}</div>
                </div>

                <div className="modal__contentBody--container">
                  {/* <div className="modal__contentBody--identifier">
                    {t("eventStart")}:
                  </div> */}
                  <div>
                    {months[this.props.currentEvent.start.getMonth()] +
                      " " +
                      this.props.currentEvent.start.getDate() +
                      " " +
                      startTime}
                  </div>
                </div>

                <div className="modal__contentBody--container">
                  {/* <div className="modal__contentBody--identifier">
                    {t("eventEnd")}:
                  </div> */}
                  <div>
                    {months[this.props.currentEvent.end.getMonth()] +
                      " " +
                      this.props.currentEvent.end.getDate() +
                      " " +
                      endTime}
                  </div>
                </div>

                <div className="modal__contentBody--container">
                  {/* <div className="modal__contentBody--identifier">
                    {t("instructor")}:
                  </div> */}
                  <div>{this.props.currentEvent.instructor}</div>
                </div>

                <hr />

                <div className="modal__contentBody--booking">
                  <div className="modal__bookingCont">
                    <div className="modal__bookingCont--label">
                      <label htmlFor="name">{t("name")}: </label>
                      {/* <span className="modal__inputError">Invalid name</span> */}
                    </div>
                    <input
                      type="text"
                      id="name"
                      name="name"
                      onChange={(event) => this.handleFormData(event, "name")}
                      className={nameError ? "modal__inputInvalid" : null}
                    />
                  </div>
                  <div className="modal__bookingCont">
                    <div className="modal__bookingCont--label">
                      <label htmlFor="email">{t("email")}: </label>
                      {/* <span className="modal__inputError"> Invalid email</span> */}
                    </div>
                    <input
                      type="email"
                      id="email"
                      name="email"
                      onChange={(event) => this.handleFormData(event, "email")}
                      className={emailError ? "modal__inputInvalid" : null}
                    />
                  </div>
                  <div className="modal__bookingCont">
                    <div className="modal__bookingCont--label">
                      <label htmlFor="phone">{t("phone")}: </label>
                      {/* <span className="modal__inputError">Invalid phone number</span> */}
                    </div>
                    <input
                      type="tel"
                      id="phone"
                      name="phone"
                      onChange={(event) => this.handleFormData(event, "phone")}
                      className={phoneError ? "modal__inputInvalid" : null}
                    />
                  </div>
                </div>
              </div>
            )}

            {loading ? (
              <Spinner />
            ) : (
              bookingSuccess && <BookingSuccess className="modal__content" />
            )}

            {loading ? null : bookingSuccess ? (
              <div
                className="modal__contentBtn2"
                onClick={this.props.handleCloseModal}
              >
                <span>{t("gotIt")}</span>
                {/* <svg className="modal__contentBtn--icon" xlinkTitle="none">
                                                    <use xlinkHref={`${svgIcon}#icon-circle-right`}></use>
                                                </svg>                                                 */}
              </div>
            ) : (
              <div
                className="modal__contentBtn"
                onClick={() => this.handleValidateForm()}
              >
                <span>{t("bookNow")}</span>
                <svg className="modal__contentBtn--icon" xlinkTitle="none">
                  <use xlinkHref={`${svgIcon}#icon-circle-right`}></use>
                </svg>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
