import * as zoid from "zoid/dist/zoid.frameworks";

let MyWidget = zoid.create({
    tag: 'my-widget',
    url: 'https://booking-widget.netlify.com',
    dimensions: {
        width: '100%',
        height: '100%',
    },
    autoResize:true,
})

// console.log('have loaded my-widget from child: ')
// console.log(MyWidget)

export default MyWidget