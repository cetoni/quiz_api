import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import MyWidget from "./widget";
import "./i18n";

ReactDOM.render(<App {...window.xprops} />, document.getElementById("root"));
