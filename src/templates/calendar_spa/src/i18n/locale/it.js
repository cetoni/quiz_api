export default {
  default: {
    modalHeader1: "Riepilogo dell'evento",
    modalHeader2: "Prenotazione riuscita",
    title: "Titolo",
    description: "Descrizione",
    eventStart: "Inizio dell'evento",
    eventEnd: "Fine dell'evento",
    instructor: "Istruttore",
    name: "Nome",
    email: "E-mail",
    phone: "Telefono(Opzionale)",
    bookNow: "Prenota ora",
    bookSuccessDesc: "I tuoi dati sono stati inviati",
    gotIt: "Fatto",
  },
};
