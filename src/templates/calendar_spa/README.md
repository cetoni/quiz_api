# Calendar Scheduler React Widget

## Deploying as Host Widget

```bash
  1. Deploy it as an app.
  2. Use the domain where the widget is deployed inside the configuration script (see next section)
```

## Embedding a widget instance on HTML Pages
Add these dependency scripts:
```bash
    <!-- Take note of the version...should be the same as the zoid version on the react-widget -->
    <script src="https://unpkg.com/zoid@9.0.28/dist/zoid.min.js"></script>
    <script src="https://unpkg.com/zoid@9.0.28/dist/zoid.frameworks.min.js"></script>

    <!-- To use react and babel to compile ES6 to vanilla javascript -->
    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js" crossorigin></script>
```

Add the configuration script in a HTML page:
```bash
    <script type="text/babel">

        //Style Config
        //Each of these variables are attached to properties inside the widget
        //Leave blank to use default which is 750px
        // const calendarHeight = "";

        const calendarHeight = "700px"

        // create an iframe with configs
        let MyWidget = zoid.create({
            tag: 'my-widget',
            url: 'https://big-calendar-23.netlify.com/',
            dimensions: {
                width: '100%',
                height: calendarHeight
            },
            props: {
              // Define schema: height variable
              calendarHeight: {
                type: 'string'
              },
            },
        })
        // generate a zoid framework driver to render a react app
        // requires zoid.frameworks.js
        let MyReactWidget = MyWidget.driver('react', {
              React: React,
              ReactDOM: ReactDOM
          });

        ReactDOM.render(<MyReactWidget calendarHeight={calendarHeight}/>, document.querySelector('#widget'));

    </script>
```







This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
