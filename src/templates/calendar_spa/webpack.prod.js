const path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve('../../static/dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.[s]?css$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(png|jpe?g|gif|pdf)$/i,
                use: ['url-loader']
            },
            {
                test: /\.svg$/,
                use: ['svg-inline-loader']
            },
            {
                test: /\.html$/,
                use: ["html-loader"]
            }
        ]
    }
  };