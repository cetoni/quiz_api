from .base import *


DEBUG_VALUES = {
    'True': True,
    'False': False,
}

DEBUG = DEBUG_VALUES.get(environ.get('DEBUG', 'False'))

ALLOWED_HOSTS = environ.get('ALLOWED_HOSTS', ['*'])
DEV = DEBUG

SECRET_KEY = environ.get('SECRET_KEY', 'secret')

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = environ.get('EMAIL_HOST')
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = DEBUG_VALUES.get(environ.get('EMAIL_USE_TLS'), False)
EMAIL_PORT = environ.get('EMAIL_PORT')
DEFAULT_FROM_EMAIL = environ.get('DEFAULT_FROM_EMAIL')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': environ.get('DATABASE_NAME'),
        'USER': environ.get('DATABASE_USER'),
        'PASSWORD': environ.get('DATABASE_PASSWORD'),
        'HOST': environ.get('DATABASE_HOST'),
        'PORT': environ.get('DATABASE_PORT', 3306),
    }
}

SITE_ID = 2

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

GOOGLE_ANALYTICS_KEY = 'UA-135194248-1'

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME')
AWS_S3_REGION_NAME='nyc3'
AWS_S3_ENDPOINT_URL='https://nyc3.digitaloceanspaces.com'
AWS_LOCATION='video'
AWS_DEFAULT_ACL=None

FILER_STORAGES = {
    'public': {
        'main': {
            'ENGINE': DEFAULT_FILE_STORAGE,
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
            'UPLOAD_TO_PREFIX': 'filer',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': '/media/filer_thumbnails',
                'base_url': '/media/filer_thumbnails/',
            },
        },
    },
    'private': {
        'main': {
            'ENGINE': DEFAULT_FILE_STORAGE,
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
            'UPLOAD_TO_PREFIX': 'filer',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': '/media/filer_thumbnails',
                'base_url': '/media/filer_thumbnails/',
            },
        },
    },
}

sentry_sdk.init(
    dsn="https://fe9868fa8c8646f89eb871c71057c6ca@sentry.io/1856547",
    integrations=[DjangoIntegration()]
)
