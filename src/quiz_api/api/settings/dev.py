import sys
from .base import *


DEBUG_VALUES = {
    'True': True,
    'False': False,
}

DEBUG = DEBUG_VALUES.get(environ.get('DEBUG', 'True'))

ALLOWED_HOSTS = environ.get('ALLOWED_HOSTS', ['*'])
DEV = DEBUG

SECRET_KEY = environ.get('SECRET_KEY', 'secret')

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

INSTALLED_APPS += ('debug_toolbar',)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': environ.get('DATABASE_NAME'),
        'USER': environ.get('DATABASE_USER'),
        'PASSWORD': environ.get('DATABASE_PASSWORD'),
        'HOST': environ.get('DATABASE_HOST'),
        'PORT': environ.get('DATABASE_PORT'),
    }
}

MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

SITE_ID = 3

AUTH_PASSWORD_VALIDATORS = []

TESTING = 'test' in sys.argv[1:]

if TESTING:
    print('=========================')
    print('In TEST Mode - Disableling Migrations')
    print('=========================')

    class DisableMigrations(object):

        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return None

    MIGRATION_MODULES = DisableMigrations()