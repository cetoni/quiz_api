"""
Django settings for quiz_api project.

using Django 2.2.1.

"""

import os
import datetime
from os import environ, getenv
from datetime import datetime, timedelta
import subprocess
from os.path import abspath, basename, dirname, join, normpath
from sys import path
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
gettext = lambda s: s

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
PROJECT_SOURCE = dirname(BASE_DIR)
TEMPLATE_DIR = os.path.join(PROJECT_SOURCE, 'templates')

CUSTOM_APPS = [
    'utils',
    'project_apps.quiz_data',
    'project_apps.driving_lessons',
    'project_apps.adaptive_quiz',
    'project_apps.driving_licences',
    'project_apps.lessons_files',
    'project_apps.services_handler',
    'project_apps.automated_doc',
    'project_apps.cms_pages'
]

INSTALLED_APPS = [
    # Custom apps before installed apps for django cms
    # https://stackoverflow.com/questions/33630209/django-1-8-lookuperror-model-not-registered-auth-user-model#_=_
    'user',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admindocs',
    'treebeard',
    'storages',
    'rest_framework',
    'django_filters',
    'drf_multiple_model',
    'rest_framework_swagger',
    'corsheaders',
    'dbbackup',
    'oauth2_provider',
    'social_django',
    'django_extensions',
    'rest_framework_social_oauth2',
    'health_check',
    'health_check.db',
    'health_check.cache',
    'health_check.storage',
    'schedule',
    'constance',
    'constance.backends.database',
    'django.contrib.sites',
    'cms',
    'sekizai',
    'menus',
    'easy_thumbnails',
    'filer',
    'mptt',
] + CUSTOM_APPS

MIDDLEWARE = [
    # django cms
    'utils.middleware.SiteMiddleware',
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    # Django cms
    'django.middleware.locale.LocaleMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',

    'utils.middleware.SiteAccess',
]

ROOT_URLCONF = 'quiz_api.api.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATE_DIR],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',

                'sekizai.context_processors.sekizai',
                'cms.context_processors.cms_settings',
                'django.template.context_processors.i18n',
                'context_processors.processors.site_processor',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ]
        },
    },
]
ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'dashboard.CustomAppIndexDashboard'

WSGI_APPLICATION = 'quiz_api.api.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


def get_git_changeset(repo_dir):
    # Custom function to get git commit time based on DJANGO_ROOT
    git_log = subprocess.Popen(
        'git log --pretty=format:%ct --quiet -1 HEAD',
        stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        shell=True, cwd=repo_dir, universal_newlines=True,
    )
    timestamp = git_log.communicate()[0]
    try:
        timestamp = datetime.utcfromtimestamp(int(timestamp))
    except ValueError:
        return None
    return timestamp.strftime('%Y%m%d%H%M%S')


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
AUTH_USER_MODEL = 'user.User'

STATIC_ROOT = normpath(join(PROJECT_SOURCE, 'assets/api'))
MEDIA_ROOT = normpath(join(PROJECT_SOURCE, 'media'))

STATICFILES_DIRS = (
    normpath(join(PROJECT_SOURCE, 'static')),
)

MEDIAFILES_LOCATION = 'smedia'

REST_FRAMEWORK = {
    'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%S%z',
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework_social_oauth2.authentication.SocialAuthentication',
    ),
    # 'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    # 'PAGE_SIZE': 10
}

LOGIN_REDIRECT_URL = 'rest_framework_social_oauth2:token'
LOGIN_URL = 'rest_framework:login'
LOGOUT_URL = 'rest_framework:logout'

# DJANGO_REST_PASSWORDRESET_TOKEN_CONFIG = {
#     "CLASS": "django_rest_passwordreset.tokens.RandomStringTokenGenerator"
# }

# SWAGGER_SETTINGS = {
#     'SECURITY_DEFINITIONS': {
#         'api_key': {
#             'type': 'apiKey',
#             'in': 'header',
#             'name': 'Authorization'
#         }
#     },
# }
SWAGGER_SETTINGS = {
    'JSON_EDITOR': True,
    'SHOW_REQUEST_HEADERS': True
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(hours=1),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
}
CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'Access-Control-Allow-Origin',
]

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'

CONSTANCE_CONFIG = {
    'ODOO_EMAIL': ('', ''),
    'ODOO_PASSWORD': ('', ''),
    'ODOO_DB_NAME': ('', ''),
    'ODOO_DOMAIN_NAME': ('', ''),
    'EMAIL_HOST': ('in-v3.mailjet.com', ''),
    'EMAIL_HOST_USER': ('88334f2f23374eec0d41072e71d499c1', ''),
    'EMAIL_HOST_PASSWORD': ('6eb8564cabf66786eb3ce766944b4bf5', ''),
    'EMAIL_SENDER': ('yes@daphne-solutions.com', ''),
    'EMAIL_SENDER_NAME': ('Daphne', ''),
    'MAILJET_API_KEY': ('88334f2f23374eec0d41072e71d499c1', ''),
    'MAILJET_SECRET': ('6eb8564cabf66786eb3ce766944b4bf5', ''),
    'LESSON_BOOKING_TEMPLATE': (1324206, ''),
    'PASSWORD_RESET_TEMPLATE': (1325221, ''),
    'BOOKING_MAIL_SUBJECT': ('Trip Booking at {site}', 'don\'t miss variable site'), #TODO: cLean up as we'ren using mailjet templates
    'BOOKING_MESSAGE': ("None", "attributes in brackets are variables and no one should be skipped or miss-spelled"),
}

CONSTANCE_CONFIG_FIELDSETS = {
    'Email Configuration': ('EMAIL_HOST_USER', 'EMAIL_HOST_PASSWORD', 'EMAIL_SENDER', 'EMAIL_SENDER_NAME',
                            'BOOKING_MESSAGE', 'BOOKING_MAIL_SUBJECT', 'EMAIL_HOST', 'MAILJET_API_KEY', 'MAILJET_SECRET', 'LESSON_BOOKING_TEMPLATE',
                            'PASSWORD_RESET_TEMPLATE'),
    'Odoo': ('ODOO_EMAIL', 'ODOO_PASSWORD', 'ODOO_DB_NAME', 'ODOO_DOMAIN_NAME')
}

DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': join(PROJECT_SOURCE, 'var/backups')}

SOCIAL_AUTH_URL_NAMESPACE = 'social'

AUTHENTICATION_BACKENDS = (
    'social_core.backends.google.GoogleOAuth2',
    'social_core.backends.facebook.FacebookOAuth2',
    'django.contrib.auth.backends.ModelBackend',
    'social_core.backends.open_id.OpenIdAuth',
    'django-dual-authentication.backends.DualAuthentication',
    'social_core.backends.google.GoogleOpenId',

)

SOCIAL_AUTH_FACEBOOK_KEY = '832710913860074'
SOCIAL_AUTH_FACEBOOK_SECRET = 'b8436f6781a5254fa202f4a045b00690'

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_link']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'id, name, email, picture.type(large), link'
}
SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [
    ('name', 'name'),
    ('email', 'email'),
    ('picture', 'picture'),
    ('link', 'profile_url'),
]

AUTHENTICATION_METHOD = 'both'
AUTHENTICATION_CASE_SENSITIVE = 'both'

SOCIAL_AUTH_GOOGLE_OAUTH2_IGNORE_DEFAULT_SCOPE = True
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'
]

SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['username', 'first_name', 'email']
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

DRFSO2_PROPRIETARY_BACKEND_NAME = 'Google'
# DRFSO2_URL_NAMESPACE = 'fb_login'

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=1)
}

CMS_PLACEHOLDER_CONF = {
    'videoconf domain area': {
        'plugins': ['TextPlugin',],
        'name': gettext("Videoconf Domain Area"),
        'language_fallback': True,
        'default_plugins': [
            {
                'plugin_type': 'TextPlugin',
                'values': {
                    'body':'<p>meet.jit.si</p>',
                },
            },
        ],
    }
}

# Django CMS
LANGUAGE_CODE = 'en'

LANGUAGES = [
    ('en', 'English')
]

CMS_TEMPLATES = [
    ## Customize this
    ('_cms/legacy/plain.html', 'Plain'),
    ('_cms/legacy/home.html', 'Home Page'),
    ('_cms/legacy/quiz.html', 'Legacy Quiz iframe'),
    ('_cms/video.html', 'Video Conf'),
    ('_cms/video_conf/webinar.html', 'Webinar'),
    ('_cms/home.html', 'Home'),
    ('_cms/legacy/calendar.html', 'Calendar')
]

CMS_LANGUAGES = {
    ## Customize this
    1: [
        {
            'code': 'en',
            'name': gettext('en'),
            'redirect_on_fallback': True,
            'public': True,
            'hide_untranslated': False,
        },
    ],
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}

CMS_PERMISSION = True

ADAPTIVE_QUIZ_LEVELS = 4
ADAPTIVE_QUIZ_ANSWERS_SPAN = 90
ADAPTIVE_QUIZ_SPAN = 2
ADAPTIVE_QUIZ_UPDATE_LEVEL = 3

FILER_IS_PUBLIC_DEFAULT = False
FILER_ENABLE_PERMISSIONS = True

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME')
AWS_S3_REGION_NAME='nyc3'
AWS_S3_ENDPOINT_URL='https://nyc3.digitaloceanspaces.com'
AWS_LOCATION='video'
AWS_DEFAULT_ACL=None

FILER_STORAGES = {
    'public': {
        'main': {
            'ENGINE': DEFAULT_FILE_STORAGE,
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
            'UPLOAD_TO_PREFIX': 'filer',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': '/media/filer_thumbnails',
                'base_url': '/media/filer_thumbnails/',
            },
        },
    },
    'private': {
        'main': {
            'ENGINE': DEFAULT_FILE_STORAGE,
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
            'UPLOAD_TO_PREFIX': 'filer',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': '/media/filer_thumbnails',
                'base_url': '/media/filer_thumbnails/',
            },
        },
    },
}