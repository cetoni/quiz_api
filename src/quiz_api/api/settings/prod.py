from .base import *


DEBUG_VALUES = {
    'True': True,
    'False': False,
}

DEBUG = DEBUG_VALUES.get(environ.get('DEBUG', 'False'))

ALLOWED_HOSTS = environ.get('ALLOWED_HOSTS', ['*'])
DEV = DEBUG

SECRET_KEY = environ.get('SECRET_KEY', 'secret')

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = environ.get('EMAIL_HOST')
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = DEBUG_VALUES.get(environ.get('EMAIL_USE_TLS'), False)
EMAIL_PORT = environ.get('EMAIL_PORT')
DEFAULT_FROM_EMAIL = environ.get('DEFAULT_FROM_EMAIL')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': environ.get('DATABASE_NAME'),
        'USER': environ.get('DATABASE_USER'),
        'PASSWORD': environ.get('DATABASE_PASSWORD'),
        'HOST': environ.get('DATABASE_HOST'),
        'PORT': environ.get('DATABASE_PORT', 3306),
    }
}

SITE_ID = 3

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

sentry_sdk.init(
    dsn="https://fe9868fa8c8646f89eb871c71057c6ca@sentry.io/1856547",
    integrations=[DjangoIntegration()]
)