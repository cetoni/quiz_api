import debug_toolbar
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic.base import TemplateView
from django.contrib.admin.views.decorators import staff_member_required
from django.conf.urls.static import static

from django.contrib.auth.views import LoginView, LogoutView

# from rest_framework_swagger.views import get_swagger_view
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

from user.views import TokenVerifyview
from project_apps.driving_lessons.views import BookingLesson

# schema_view = get_swagger_view(title='Quiz API')

urlpatterns = [
	path('i18n/', include('django.conf.urls.i18n')),
	path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
	path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
	path('api/token/verify/', TokenVerifyview.as_view(), name='token_verify'),
	path('api/auth/oauth/', include('rest_framework_social_oauth2.urls')),
	path('quiz/', include('project_apps.quiz_data.rest_urls', namespace='quiz')),
	path('user/', include('user.urls', namespace='user')),
	path('lessons/', include('project_apps.driving_lessons.urls', namespace='lessons')),
	path('booking/', BookingLesson.as_view(), name='booking_lesson'),
	path('adaptive/', include('project_apps.adaptive_quiz.urls', namespace='adaptive')),
	path('social/', include('social_django.urls', namespace='social')),
	# path('document/', staff_member_required(schema_view)),
	url(r'^login/$', LoginView.as_view(template_name="_cms/auth/login.html"), name='login'),
	url(r'logout/$', LogoutView.as_view(), name='logout')
]

urlpatterns += i18n_patterns(
	path('admin/', admin.site.urls),
	re_path(r'^admin_tools/', include('admin_tools.urls')),
	path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	prefix_default_language=False
)

urlpatterns += [re_path(r'^', include('cms.urls'))]

if settings.DEBUG:
	urlpatterns += [
		path('__debug__/', include(debug_toolbar.urls)),
		# Testing 404 and 500 error pages
		path('404/', TemplateView.as_view(template_name='404.html'), name='404'),
		path('500/', TemplateView.as_view(template_name='500.html'), name='500'),
	]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
