import os
import multiprocessing

if os.environ.get('MODE') == 'dev':
    reload = True
    loglevel = 'info' # TODO: put back to debug
else:
    reload = False
    loglevel = 'info'
bind = '0.0.0.0:8000'
timeout = 60*60
workers = multiprocessing.cpu_count() * 2 + 1

