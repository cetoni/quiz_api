from django.urls import path

from .views import ContactView


urlpatterns = [
    path('send-mail/', ContactView.as_view(), name='contact'),
]
