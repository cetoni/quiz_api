from django.db import models
from django.core.mail import send_mail
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from utils.models import TimeStamp


class ContactModel(TimeStamp):
    name = models.CharField(_('Name'), max_length=50)
    email = models.EmailField(_('Email'), )
    contact_no = models.CharField(_('Phone No'), max_length=15, )
    address = models.TextField(_('Address'), )
    subject = models.CharField(_('Subject'), max_length=100)
    message = models.TextField(_('Additional Info'),)
    file = models.FileField(_('File'))
    SUBJECT_MESSAGE = 'VALLE SIGNS - Contact Mail'

    class Meta:
        ordering = ['-id']
        verbose_name = _('Contact Mail')
        verbose_name_plural = _('Contact Mails')

    def clean(self):
        if not self.subject or len(self.subject):
            self.subject = self.SUBJECT_MESSAGE

    def __str__(self):
        return "{} {} {}".format(self.name, self.created_at, self.message[:15]+'...')

    def dispatch_mail(self):
        send_mail(
            self.subject,
            self.message,
            self.email,
            list(dict(settings.ADMINS).values()),
        )
