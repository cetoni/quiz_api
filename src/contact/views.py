from django.views.generic import CreateView
from django.http import JsonResponse
from django.utils.translation import gettext_lazy as _

from .models import ContactModel
from .forms import ContactForm


class ContactView(CreateView):
    model = ContactModel
    form_class = ContactForm
    # fields = '__all__'
    template_name = 'simple_form.html'
    fail_message = _('Message not sent, please try again')
    success_message = _('Message Sent Successfully, We Will Contact With You Soon')

    def form_valid(self, form):
        self.object = form.save()
        self.object.dispatch_mail()
        data = {'result': True, 'message': self.success_message}
        return JsonResponse(data, status=200)

    def form_invalid(self, form):
        data = {'result': form.errors, 'message': self.fail_message}
        return JsonResponse(data, status=400)
