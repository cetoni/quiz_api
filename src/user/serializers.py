import datetime
from django.contrib.sites.models import Site
from django.contrib.auth import get_user_model
from django.core.exceptions import  ValidationError
from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, exceptions
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import raise_errors_on_nested_writes
from rest_framework.utils import model_meta
from rest_framework_simplejwt.tokens import RefreshToken, UntypedToken

from user import models
from user.utils import OdooManager
from project_apps.quiz_data.models import QuizModel

User = get_user_model()


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = (
            'id', 'email', 'is_student', 'first_name', 'last_name', 'parent', 'school_name', 'is_school', 'school_image', 'is_teacher', 'is_staff',
            'is_active', 'google_login_present', 'facebook_login_present', 'google_mail', 'facebook_mail')
            
class StudentCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ()


class TokenObtainSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    password = serializers.CharField()
    default_error_messages = {
        'no_active_account': _('No active account found with the given credentials'),
        'username_or_email': _('Please specify either username or email')
    }

    def get_user(self, **credentials):
        try:
            password = credentials.pop('password')
            user = User.objects.get(**credentials)
            if user.check_password(password):
                return user
            else:
                return None
        except User.DoesNotExist:
            return None

    def validate(self, attrs):
        email = attrs.get('email')
        username = attrs.get('username')

        if not (email or username):
            raise ValidationError(self.default_error_messages['username_or_email'])

        authenticate_kwargs = {
            'password': attrs['password'],
        }
        if email:
            authenticate_kwargs['email'] = email
        if username:
            authenticate_kwargs['username'] = username

        self.user = self.get_user(**authenticate_kwargs)
        # self.user = authenticate
        if self.user is None or not self.user.is_active:
            raise exceptions.AuthenticationFailed(
                self.error_messages['no_active_account'],
                'no_active_account',
            )

        return {}

    @classmethod
    def get_token(cls, user):
        raise NotImplementedError('Must implement `get_token` method for `TokenObtainSerializer` subclasses')


class TokenObtainPairSerializer(TokenObtainSerializer):
    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        return data


class UserTokenInfoSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        attrs = super().validate(attrs)
        attrs['user'] = UserInfoSerializer(self.user).data
        return attrs


class GoogleAuthSerializer(serializers.Serializer):

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        refresh = self.get_token(self.instance)

        attrs['refresh'] = str(refresh)
        attrs['access'] = str(refresh.access_token)
        attrs['user'] = UserInfoSerializer(instance=self.instance).data
        return attrs


class SocialSerializer(serializers.Serializer):
    provider = serializers.CharField(max_length=255, required=True)
    access_token = serializers.CharField(max_length=4096, required=True, trim_whitespace=True)
    # email


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()
    url = serializers.CharField(max_length=255, required=False)


class PasswordTokenSerializer(serializers.Serializer):
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})
    confirm_password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})
    email = serializers.CharField(label=_("enc_email"))
    token = serializers.CharField(label=_("token"))

class StudentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username',)


class CustomUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'}, trim_whitespace=False, write_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password', 'is_school', 'is_teacher','is_student', 'is_staff', 'first_name',
                  'last_name', 'parent']

    def __init__(self, *args, **kwargs):
        super(CustomUserSerializer, self).__init__(*args, **kwargs)

    def create(self, validated_data):
        user_obj = User.objects.create_user(**validated_data)
        return user_obj

    def update(self, instance, validated_data):
        password = validated_data.get('password')
        if password:
            instance.set_password(password)

        raise_errors_on_nested_writes('update', self, validated_data)
        info = model_meta.get_field_info(instance)

        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                field = getattr(instance, attr)
                field.set(value)
            elif attr != 'password':
                setattr(instance, attr, value)
        instance.save()
        return instance

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['email'] = instance.email
        return ret


class UserCreateSerializer(CustomUserSerializer):
    driving_licence_expiration = serializers.DateField(required=False)
    quiz_type = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password', 'is_school', 'is_teacher','is_student', 'is_staff', 'first_name',
                  'last_name', 'quiz_type', 'parent', 'phone', 'driving_licence_expiration', 'odoo_id']

    def validate(self, attrs):
        parent = None
        student = None
        quiz = None
        expiration = attrs.get('driving_licence_expiration')
        parent = attrs.get('parent')

        if not attrs.get('is_school') and not attrs.get('is_staff') and not attrs.get('is_teacher'):
            attrs['is_student'] = True
            student = True

        if not (attrs.get('username') or attrs.get('email')) and not self.instance:
            raise ValidationError('Username or Email on parameter is required')

        if student:
            try:
                if attrs.get('quiz_type') and not self.instance:
                    quiz_type = attrs.get('quiz_type')
                    quiz = QuizModel.objects.get(id=quiz_type)
            except QuizModel.DoesNotExist as e:
                print(e)
                raise ValidationError('This quiz does not exist')

            if not parent:
                site_profile = Site.objects.get_current().site.first()
                if site_profile:
                    attrs['parent'] = site_profile.user
                else:
                    raise ValidationError('No parent was given for the student')

            if attrs.get('quiz_type'):
                attrs.pop('quiz_type')
                attrs['quiz'] = quiz
        else:
            if attrs.get('quiz_type'):
                attrs.pop('quiz_type')
            if attrs.get('parent'):
                attrs.pop('parent')

        return attrs

    def create(self, validated_data):
        expiry = None
        user_data = {}
        parent_id = None
        quiz = validated_data.pop('quiz', None)
        expiration = validated_data.get('driving_licence_expiration')
        username = validated_data.get('username')
        phone = validated_data.get('phone')

        if expiration:
            dt = datetime.datetime.combine(expiration, datetime.datetime.min.time())
            expiry = dt.isoformat()

        f_name = validated_data.get('first_name')
        l_name = validated_data.get('last_name')

        if not username:
            username = f_name + ' ' + l_name

        for k, v in validated_data.items():
            if k != 'driving_licence_expiration':
                user_data[k] = v

        user = User(**user_data)

        if validated_data.get('odoo_id'):
            user.odoo_id = validated_data.get('odoo_id')
            user.save()
            return user

        # odoo accounts creation
        odoo = OdooManager().odoo_login()
        OdooCompany = odoo.env['res.company']
        Partner = odoo.env['res.partner']
        OdooUser = odoo.env['res.users']
        Certificate = odoo.env['odoo_partner.certificates']
        Template = odoo.env['odoo_partner.certificates.template']
        
        current_user = self.context.get('user')
        if not current_user:
            current_user = self.context.get('request').user

        # Student is created with the company id of parent
        odoo_company_id = None
        if user.is_student:
            if not current_user.is_anonymous and current_user.is_school:
                school = current_user.odoo_id
            else:
                school = validated_data.get('parent')
                if not school.is_school:
                    raise ValidationError('parent must be a school')
            company_id = OdooCompany.search([("id", "=", school.odoo_id)])
            odoo.env.user.company_id = company_id[0]
            if company_id:
                odoo_company = OdooCompany.browse(company_id[0])
                odoo_company_id = odoo_company.partner_id.company_id.id
                parent_id = odoo_company.partner_id.company_id.partner_id.id

            item_id = Template.search([("id", "=", quiz.odoo_id)])
            if len(item_id) > 1:
                raise ValueError('Found more than one odoo template')

            template_id = item_id[0]
            user_id = Partner.create(dict(firstname=f_name,
                                          lastname=l_name,
                                          name=username,
                                          email=user.email,
                                          phone=phone,
                                          is_customer=True,
                                          parent_id=parent_id,
                                          company_id=odoo_company_id
                                        ))
            data = {
                'name': quiz.title,
                'template_id': int(template_id),
                'partner_id': int(user_id)
            }
            if expiry:
                data['expiry_date'] = expiry

            certificate_id = Certificate.create(data)
            user.odoo_id = user_id
            user.save()
        elif user.is_school:
            company_id = OdooCompany.create(dict(name=user.school_name, email=user.email, is_company=True))

            user.odoo_id = company_id
            user.save()

            admin = odoo.env.user
            admin.company_id = company_id

            try:
                user_id = OdooUser.create({
                    'name': user.username,
                    'login': user.email,
                    'password': user.username,
                    'company_id': company_id,
                    'notification_type': 'inbox',
                    'sel_groups_1_9_10': 1,
                    'sel_groups_2_3': 2
                })
            except odoorpc.error.RPCError as er:
                print(er)
                user_id = User.search([('login', '=', user.email)])
                OdooUser.write(user_id, {'company_id': company_id})
        user.save()
        return user

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class UserUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'google_mail', 
            'google_login_present', 'facebook_mail', 'facebook_login_present', 'is_active')
        extra_kwargs = {
            'password': {'read_only': True}
        }

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            setattr(instance, key, value)
        instance.save()
        return instance


class VerifyTokenSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    token = serializers.CharField(max_length=255, required=True)


class TokenVerifySerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate(self, attrs):
        UntypedToken(attrs['token'])
        return attrs

class UserLoginSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.UserLogin
        fields = '__all__'
        depth = 1