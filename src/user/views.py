from django.contrib.auth import get_user_model
import facebook
from constance import config
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q
from google.auth.transport import requests as req
from google.oauth2 import id_token
from rest_framework import generics, status, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenViewBase
from social_django.models import UserSocialAuth

from user.utils import OdooManager
from . import serializers, models
from .serializers import EmailSerializer, PasswordTokenSerializer, VerifyTokenSerializer, UserUpdateSerializer
from .token import account_activation_token
# from .utils import IsUserRegistration
from .utils import IsUserRegistration
from utils.mail import mailjet
from project_apps.quiz_data.models import QuizModel
from project_apps.quiz_data.local_serializers import QuizSerializer

import requests
from health_check.views import MainView

User = get_user_model()


class UserInfoAPIView(APIView):

    def get(self, request, *args, **kwargs):
        user = request.user
        serializer = serializers.UserInfoSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserDetailAPIView(generics.RetrieveAPIView):
    queryset = models.User.objects.all()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = serializers.UserInfoSerializer(instance)
        return Response(serializer.data)


class CompanyLogoAPIView(generics.RetrieveAPIView):
    permission_classes = ()
    queryset = models.User.objects.all()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = serializers.UserInfoSerializer(instance)
        if serializer.data['school_image']:
            return Response(serializer.data['school_image'])
        else:
            return Response('No company logo found for the given id')


class GoogleLoginUrl(APIView):

    def get(self, request):
        return Response({'google_url': reverse('social:begin', None, ['google_oauth2'])}, status=status.HTTP_200_OK)


class FacebookLoginUrl(APIView):

    def get(self, request):
        return Response({'facebook_url': reverse('social:begin', None, ['google'])}, status=status.HTTP_200_OK)


class FacebookLogin(APIView):

    def post(self, request, *args, **kwargs):
        return Response({'status': 'worked', **request.POST})


@login_required
def settings(request):
    user = request.user

    try:
        facebook_login = user.social_auth.get(provider='facebook')
    except UserSocialAuth.DoesNotExist:
        facebook_login = None

    can_disconnect = (user.social_auth.count() > 1 or user.has_usable_password())

    return render(request, 'core/settings.html', {
        'facebook_login': facebook_login,
        'can_disconnect': can_disconnect
    })


class UserAPIView(viewsets.ModelViewSet):
    permission_classes = (IsUserRegistration,)
    serializer_class = serializers.UserCreateSerializer
    queryset = User.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('username',)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={'user': request.user})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save()

    

class UserActivation(APIView):

    def post(self, request, *args, **kwargs):
        try:
            user_id = kwargs.pop('user_id')
            user = User.objects.get(id=user_id)
            if request.user.is_superuser or request.user.is_school:
                if request.user.is_school:
                    children_ids = [user.id for user in request.user.child.all()]
                    if not user.id in children_ids:
                        return Response(dict(error='Do not have right on this user'), status=status.HTTP_400_BAD_REQUEST)
                user.is_active = False
                user.save()
                serializer = serializers.UserUpdateSerializer(user)
                return Response(serializer.data)

        except KeyError as e:
            return Response(dict(error=f'Missing {str(e)}'), status=status.status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist as e:
            return Response(dict(error=f'User not found'), status=status.HTTP_404_NOT_FOUND)
            
class SocialLoginView(generics.GenericAPIView):
    serializer_class = serializers.GoogleAuthSerializer
    permission_classes = []

    def post(self, request, *args, **kwargs):
        idtoken = self.request.data.get('id_token')
        client_id = self.request.data.get('client_id')

        try:
            idinfo = id_token.verify_oauth2_token(idtoken, req.Request(), client_id)

        except ValueError as e:
            return Response({'error': 'Invalid Token' + str(e)})

        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            return Response({'error': 'Wrong issuer of token'})

        user_info = {
            'google_mail': idinfo['email'],
            'first_name': idinfo['given_name'],
            'last_name': idinfo['family_name']
        }

        if request.user.is_authenticated:
            user_qs = request.user
            # TODO: update user record with google info 
        else:
            try:
                user_qs = User.objects.get(Q(google_mail=user_info['google_mail']) | Q(email=user_info['google_mail']))
                # user.models.User.MultipleObjectsReturned: get() returned more than one User -- it returned 2! in case of same email FIXME
            except User.DoesNotExist as e:
                print(e)
                user_qs = None

        # FIXME: missing password from user_info raises error
        if not user_qs:
            user = User.objects.create_user(**user_info)
            user.google_login_present = True
            user.google_mail = user_info['google_mail']
        else:
            user = user_qs
            data = dict(
                google_mail=user_info['google_mail'],
                google_login_present=True
            )
            updater = UserUpdateSerializer(user, data=data)
            try:
                updater.is_valid(raise_exception=True)
                updater.save()
            except Exception as e:
                print(e)
                return Response(dict(error=e.detail), status=status.HTTP_409_CONFLICT)

        serializer = self.serializer_class(instance=user, data={})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data)


class FacebookLoginView(generics.GenericAPIView):
    serializer_class = serializers.GoogleAuthSerializer
    permission_classes = []

    def post(self, request, *args, **kwargs):
        user_qs = None
        access_token = self.request.data.get('token')
        try:
            graph = facebook.GraphAPI(access_token)
            idinfo = graph.get_object('me', fields='email,first_name,last_name')
        except ValueError as e:
            return Response({'error': 'Invalid Token' + str(e)})

        user_info = {'facebook_email': idinfo['email']}
        print(user_info)
        if request.user.is_authenticated:
            user_qs = request.user
            # TODO: update user record with google info 
        else:
            try:
                user_qs = User.objects.get(
                    Q(facebook_mail=user_info['facebook_email']) | Q(email=user_info['facebook_email']))
                # user.models.User.MultipleObjectsReturned: get() returned more than one User -- it returned 2! in case of same email FIXME
            except User.DoesNotExist as e:
                print(e)
                user_qs = None

        # FIXME: missing password from user_info raises error
        if not user_qs:
            user = User.objects.create_user(**user_info)
            user.facebook_login_present = True
            user.google_mail = user_info['facebook_email']
        else:
            user = user_qs
            data = dict(
                facebook_mail=user_info['facebook_email'],
                facebook_login_present=True
            )
            updater = UserUpdateSerializer(user, data=data)
            try:
                updater.is_valid(raise_exception=True)
                updater.save()
            except Exception as e:
                print(e)
                return Response(dict(error=e.detail), status=status.HTTP_409_CONFLICT)

        serializer = self.serializer_class(instance=user, data={})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data)


class ResetPasswordRequestToken(generics.GenericAPIView):
    permission_classes = ()
    serializer_class = EmailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_url = None
        user_email = serializer.validated_data['email']
        if serializer.validated_data.get('url'):
            user_url = serializer.validated_data['url']
        try:
            user = User.objects.get(email=user_email)
        except:
            return Response({"Error": "No registered user exists with this email"})
        email = urlsafe_base64_encode(force_bytes(user.email))
        token = account_activation_token.make_token(user)

        if user:
            if user_url:
                url = f"{user_url}/{token}/{email}"
            else:
                current_site = get_current_site(request)
                url = f"{current_site.domain}/{reverse('user:reset_password', kwargs={'email': email, 'reset_token': token})}"
            data = {
                "student_name": user.get_full_name(),
                "password_reset_url": url,
            }
            to_email = user_email
            message = {
                "Messages": [
                    {
                        "From": {
                            "Email": config.EMAIL_SENDER,
                            "Name": config.EMAIL_SENDER_NAME
                        },
                        "To": [
                            {
                                "Email": to_email,
                                "Name": user.get_full_name()
                            }
                        ],
                        "TemplateID": config.PASSWORD_RESET_TEMPLATE,
                        "TemplateLanguage": True,
                        "Subject": "Reset della password",
                        "Variables": data
                    }
                ]
            }
            result = mailjet.send.create(data=message)
            if result.status_code != 200:
                return Response(
                    dict(
                        error='Could not send email',
                        reset_token=account_activation_token.make_token(user),
                        encrypt_email=email
                    )
                )

            return Response({"reset_token": account_activation_token.make_token(user), "encrypt_email": email})
        else:
            return Response({"Error": "No user found with this email"})


class ResetPasswordValidateToken(APIView):
    permission_classes = (AllowAny,)
    serializer_class = PasswordTokenSerializer

    def get(self, request, *args, **kwargs):
        token = kwargs.get('reset_token')
        encrypt_email = kwargs.get('email')
        try:
            email = force_text(urlsafe_base64_decode(encrypt_email))
            user = User.objects.get(email=email)
            return render(request, 'form-confirm-password.html', {"email": encrypt_email, "token": token})
        except:
            return render(request, 'error_message.html', {"Error": "No active user found with this email"})

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        password = serializer.validated_data.get('password')
        confirm_password = serializer.validated_data.get('confirm_password')
        enc_email = serializer.validated_data.get('email')
        token = serializer.validated_data.get('token')
        email = force_text(urlsafe_base64_decode(enc_email))
        user = User.objects.get(email=email)
        check = account_activation_token.check_token(user, token)
        if check:
            if password == confirm_password:
                user = User.objects.get(email=email)
                user.set_password(password)
                user.save()
                if request.POST.get('scan'):
                    return render(request, 'form-confirm-password.html', {'success': "Password Reset Successfully"})
                return Response({"Success": "Password Reset Successfully"})
            else:
                if request.POST.get('scan'):
                    return render(request, 'form-confirm-password.html', {'Error': "Password did not match"})
                return Response({"Error": "Passwords did not match"})
        else:
            if request.POST.get('scan'):
                return render(request, 'form-confirm-password.html', {'Error': "Token Expired"})
            return Response({"Error": "Token Expired"})


# class ResetPassword(generics.GenericAPIView):
#     permission_classes = ()
#     serializer_class = PasswordTokenSerializer
#
#     def post(self, request, *args, **kwargs):
#         new_token = kwargs.get('new_token')
#         encrypt_email = kwargs.get('email')
#         email = force_text(urlsafe_base64_decode(encrypt_email))
#         user = User.objects.get(email=email)
#         check = account_activation_token.check_token(user, new_token)
#         if check:
#             serializer = self.serializer_class(data=request.data)
#             serializer.is_valid(raise_exception=True)
#             password = serializer.validated_data['password']
#             confirm_password = serializer.validated_data['confirm_password']
#             if password == confirm_password:
#                 user.set_password(password)
#                 user.save()
#                 return Response({"Success": "Password Reset Successfully"})
#             else:
#                 return Response({"Error": "Passwords did not match"})
#         else:
#             return Response({"Error": "Token expired"})

# class UpdateUserAPIView(generics.UpdateAPIView):
#     serializer_class = serializers.UserUpdateSerializer
#
#     def patch(self, request, *args, **kwargs):
#         serializer = self.serializer_class(request.user, data=request.data, partial=True)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data, status=status.HTTP_200_OK)
#
#
# class UserDeleteAPIView(APIView):
#     permission_classes = ()
#
#     def post(self, request, *args, **kwargs):
#         username = request.data.get('username')
#         try:
#             User.objects.get(username=username).delete()
#             return Response({"Success": "User deleted successfully"})
#         except:
#             return Response({"Error": "User not found with this username"})


class verify_token(generics.GenericAPIView):
    serializer_class = VerifyTokenSerializer
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        enc_email = request.data.get('email')
        token = request.data.get('token')
        try:
            email = force_text(urlsafe_base64_decode(enc_email))
            if email:
                user = User.objects.get(email=email)
            verify = account_activation_token.check_token(user, token)
            if verify:
                return Response({"Success": "Token verified "}, status=status.HTTP_200_OK)
            return Response({"error": "Token invalid or expired"}, status=status.HTTP_410_GONE)
        except:
            return Response({"error": "invalid EmailId or token"}, status=status.HTTP_410_GONE)


class TokenVerifyview(TokenViewBase):
    permission_classes = ()
    serializer_class = serializers.TokenVerifySerializer

    def post(self, request, *args, **kwargs):
        serializer = serializers.TokenVerifySerializer(data=request.data)
        try:
            serializer.is_valid()
            return Response({"success": "Token Valid"})
        except:
            return Response({"Error": "Token Invalid or Expired"})

class QuizAccess(APIView):
    # /can-access/<int:quiz_id>/
    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_student and not user.parent:
            return Response('Student has no parent school', status=status.HTTP_400_BAD_REQUEST)

        try:
            quiz_id = kwargs.pop('quiz_id')
            quiz = QuizModel.objects.get(id=quiz_id)
        except KeyError as e:
            return Response({'error': 'Must provide a valid quiz id'}, status=status.HTTP_400_BAD_REQUEST)
        except QuizModel.DoesNotExist as e:
            return Response({'error': 'Could not find a valid id'}, status=status.HTTP_404_NOT_FOUND)

        # get user profile from odoo and certificates
        odoo = OdooManager().odoo_login()
        Partner = odoo.env['res.partner']
        Certificate = odoo.env['odoo_partner.certificates']
        Template = odoo.env['odoo_partner.certificates.template']

        # confirm that the quiz.odoo_id == template id
        template_id = Template.search([('id', '=', quiz.odoo_id)])
        if not template_id:
            return Response({'error': 'The quiz template id was not found in odoo'}, status=status.HTTP_404_NOT_FOUND)
        template = Template.browse(template_id[0])
        assert template.name == quiz.title, "Template name does not match quiz title"

        _user = odoo.env.user
        _user.company_id = user.parent.odoo_id

        # confirm the user matches the partner account
        partner_id = Partner.search([('id', '=', user.odoo_id)])
        if not partner_id:
            return Response({'error': 'The partner was not found in odoo'}, status=status.HTTP_404_NOT_FOUND)
        odoo_partner = Partner.browse(partner_id[0])
        assert odoo_partner.email == user.email, "Partner email does not match user email"

        # look for a certificate of that template id, grant access or refuse
        user_certificate_id = Certificate.search([('template_id', '=', quiz.odoo_id), ('partner_id', '=', user.odoo_id)])
        if not user_certificate_id:
            quizes = []
            user_certificate_ids = Certificate.search([('partner_id', '=', user.odoo_id)])
            for c_id in user_certificate_ids:
                try:
                    cert = Certificate.browse(c_id)
                    quiz = QuizModel.objects.get(odoo_id=cert.template_id.id)
                    serializer = QuizSerializer(quiz)
                    quizes.append(serializer.data)
                except QuizModel.DoesNotExist:
                    certificate = Certificate.browse(c_id)
                    cert_data = {'id': certificate.id, 'title': certificate.name, 'status': 'not found in quiz'}
                    quizes.append(cert_data)

            return Response({'error': 'Are you sure ?', 'available_certificates': quizes}, status=status.HTTP_400_BAD_REQUEST)
        
        serializer = QuizSerializer(quiz)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserQuiz(APIView):
    """User quizzes from odoo certificates"""

    def get(self, request, *args, **kwargs):
        quizes = []
        user = request.user
        if not user.parent:
            return Response('Student has no parent school', status=status.HTTP_400_BAD_REQUEST)
        # Following code is copied from quiz-access view TODO: make into a mixin
        # get user profile from odoo and certificates
        odoo = OdooManager().odoo_login()
        Partner = odoo.env['res.partner']
        Certificate = odoo.env['odoo_partner.certificates']
        Template = odoo.env['odoo_partner.certificates.template']

        # set odoo user company to the student's parent odoo_id
        _user = odoo.env.user
        _user.company_id = user.parent.odoo_id

        # confirm the user matches the partner account
        partner_id = Partner.search([('id', '=', user.odoo_id)])
        if not partner_id:
            return Response({'error': 'The partner was not found in odoo'}, status=status.HTTP_404_NOT_FOUND)
        odoo_partner = Partner.browse(partner_id[0])
        if user.email and (odoo_partner.email != user.email):
            return Response("Partner email does not match user email", status=status.HTTP_401_UNAUTHORIZED)

        user_certificate_ids = Certificate.search([('partner_id', '=', user.odoo_id)])
        for c_id in user_certificate_ids:
            try:
                cert = Certificate.browse(c_id)
                quiz = QuizModel.objects.get(odoo_id=cert.template_id.id)
                serializer = QuizSerializer(quiz)
                quizes.append(serializer.data)
            except QuizModel.DoesNotExist:
                pass

        res_status = status.HTTP_200_OK
        if not quizes:
            res_status = status.HTTP_404_NOT_FOUND
        return Response(quizes, status=res_status)