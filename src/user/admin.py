import re

from cms.api import create_page, get_cms_setting, add_plugin, publish_page
from cms.models import Placeholder, Page
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.sites.models import Site
from django import forms
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
from django.contrib.sessions.models import Session
from .models import User, SiteProfile, UserLogin

from project_apps.quiz_data.cms_plugins import JitsiPublisher, LegacyQuizB, LegacyQuizAM, \
    LegacyQuizRevisioni, LegacyQuizCDE, CalendarSpa, QuizPlugin
from project_apps.adaptive_quiz.admin import LevelInline

class UserLoginAdmin(admin.ModelAdmin):
    list_display = ('user', 'login_timestamp', 'logout_timestamp')

class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']

class UserAdminForm(forms.ModelForm):
    queryset = User.objects.filter(is_school=True)
    parent = forms.ModelChoiceField(queryset)

    class Meta:
        model = User
        fields = ['parent']


class CustomUserAdmin(UserAdmin):
    list_display = ('email', 'is_school', 'is_student', 'is_staff', 'is_superuser', 'is_active', 'google_login_present', 'facebook_login_present', 'username', 'odoo_id', 'user_tracking')
    ordering = ('date_joined',)
    # form =

    fieldsets = (
        (None, {'fields': ('email', 'username', 'password', 'parent', 'odoo_id')}),
        (_('School Info'), {'fields': ('is_school', 'school_name')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Social login'), {'fields': ('google_login_present', 'google_mail', 'facebook_mail', 'facebook_login_present')}),
        (_('Permissions'),
         {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_student', 'is_teacher', 'user_tracking', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2', 'is_school', 'school_image', 'school_name', 'is_staff', 'is_teacher', 'parent',
                       'is_student', 'first_name', 'last_name'),
        }),
    )
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'is_school', 'is_student', 'groups', 'google_login_present', 'facebook_login_present')
    search_fields = ('email', 'school_name', 'username')
    inlines = [
        LevelInline,
    ]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "parent":
            kwargs["queryset"] = User.objects.filter(is_school=True)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

class SiteProfileForm(forms.ModelForm):
    class Meta:
        model = SiteProfile
        fields = ['firebase_site_id',]

    user = forms.ModelChoiceField(queryset=User.objects.filter(is_school=True).order_by('username'))
    site = forms.ModelChoiceField(queryset=Site.objects.all())

class SiteProfileAdmin(admin.ModelAdmin):
    form = SiteProfileForm
    list_display = ['site', 'user', 'firebase_site_id',]

    def save_model(self, request, obj, form, change):
        pages = []
        data = {}
        page = None
        regex = re.compile('[^a-zA-Z]')
        obj.save()
        site = obj.site
        pages_data = [
            {
                'data': {'title': 'Lezioni a Distanza', 'login_required': False, 'slug': 'lezioni-distanza', 'template': '_cms/video.html'},
                'plugin': JitsiPublisher,
                'placeholder': 'videoconf area'
            },
            {
                'data': {'title': 'Lezioni a Distanza con password', 'login_required': True, 'slug': 'lezioni-distanza-password', 'template': '_cms/video.html'},
                'plugin': JitsiPublisher,
                'placeholder': 'videoconf area'
            },
            {
                'data': {'title': 'Quiz CQC', 'slug': 'quiz-cqc', 'template': '_cms/legacy/quiz.html'},
                'plugin': QuizPlugin,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz AM', 'slug': 'quiz-am', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizAM,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz B', 'slug': 'quiz-b', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizB,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz Revisioni', 'slug': 'quiz-revisioni', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizRevisioni,
                'placeholder': 'content'
            },
            {
                'data': {'title': 'Quiz CDE', 'slug': 'quiz-cde', 'template': '_cms/legacy/quiz.html'},
                'plugin': LegacyQuizCDE,
                'placeholder': 'content'
            },
            {
                'data': {'title': ' Calendario Guide', 'slug': 'calendario-guide', 'template': '_cms/legacy/quiz.html'},
                'plugin': CalendarSpa,
                'placeholder': 'content'
            }
        ]
        for p in pages_data:
            pages.append({
                'page': create_page(**p['data'], language='it', menu_title=p['data']['title'], published=True, site=site, in_navigation=True),
                'data': p
            })
        
        pages[0]['page'].set_as_homepage()
        try:
            for page in pages:
                template = page['page'].get_template()
                placeholder = page['page'].placeholders.get(slot=page['data']['placeholder'])
                plugin_class = page['data']['plugin']
                if plugin_class == JitsiPublisher:
                    room_name = site.domain + '' + page['data']['data']['title'] + 'VideoConf'
                    room_name_lower = regex.sub('', room_name)
                    data = {'room_name': room_name_lower, 'site': site}
                else:
                    data = {}
                add_plugin(placeholder=placeholder, plugin_type=page['data']['plugin'], language='it', **data)

                publish_page(page=page['page'], user=request.user, language='it')
        except Placeholder.DoesNotExist as e:
            print(e)
            messages.add_message(request, messages.ERROR, str(e))

    def delete_model(self, request, obj):
        site = obj.site
        pages = Page.objects.filter(node__site=site)
        for page in pages:
            Page.objects.filter(id=page.id).delete()
        obj.delete()
    
    def delete_queryset(self, request, queryset):
        for obj in queryset:
            self.delete_model(request, obj)

admin.site.register(User, CustomUserAdmin)
admin.site.register(SiteProfile, SiteProfileAdmin)
admin.site.register(UserLogin, UserLoginAdmin)
