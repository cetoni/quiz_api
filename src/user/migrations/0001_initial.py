# Generated by Django 2.2.1 on 2020-01-22 11:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='OldCompanies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=225)),
                ('street', models.CharField(blank=True, max_length=225, null=True)),
                ('street_number', models.CharField(blank=True, max_length=20, null=True)),
                ('zip', models.IntegerField(blank=True, null=True)),
                ('phone', models.CharField(blank=True, max_length=225, null=True)),
                ('mail', models.CharField(max_length=225)),
                ('certified_mail', models.CharField(blank=True, max_length=225, null=True)),
                ('infomot_username', models.CharField(blank=True, max_length=225, null=True)),
                ('infomot_password', models.CharField(blank=True, max_length=225, null=True)),
                ('infomot_pin', models.CharField(blank=True, max_length=225, null=True)),
                ('infomot_codiceoperatore', models.CharField(blank=True, max_length=225, null=True)),
                ('infomot_ufficiooperativo', models.CharField(blank=True, max_length=225, null=True)),
                ('notes', models.TextField(blank=True, null=True)),
                ('avatar', models.CharField(blank=True, max_length=200, null=True)),
                ('production', models.IntegerField()),
                ('ai', models.IntegerField()),
            ],
            options={
                'db_table': 'old_companies',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='OldCustomers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=225)),
                ('login', models.CharField(max_length=100, unique=True)),
                ('fb_id', models.CharField(blank=True, max_length=28, null=True)),
                ('password', models.CharField(max_length=60)),
                ('surname', models.CharField(max_length=225)),
                ('email', models.CharField(blank=True, max_length=225, null=True, unique=True)),
                ('photo', models.CharField(blank=True, max_length=225, null=True)),
                ('signature', models.CharField(blank=True, max_length=225, null=True)),
                ('sex', models.CharField(blank=True, max_length=2, null=True)),
                ('street', models.CharField(blank=True, max_length=225, null=True)),
                ('street_number', models.CharField(blank=True, max_length=225, null=True)),
                ('zip', models.IntegerField(blank=True, null=True)),
                ('phone', models.CharField(blank=True, max_length=225, null=True)),
                ('certified_mail', models.CharField(blank=True, max_length=225, null=True)),
                ('birth_date', models.DateField(blank=True, null=True)),
                ('social_security_number', models.CharField(blank=True, max_length=20, null=True)),
                ('avatar', models.CharField(blank=True, max_length=200, null=True)),
                ('is_student', models.IntegerField()),
                ('gdpr_accepted', models.DateTimeField(blank=True, null=True)),
                ('ig_id', models.CharField(blank=True, max_length=28, null=True)),
            ],
            options={
                'db_table': 'old_customers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='OldUsers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=225, unique=True)),
                ('email', models.CharField(max_length=225, unique=True)),
                ('activation_key', models.CharField(blank=True, max_length=225, null=True)),
                ('create_at', models.DateTimeField()),
                ('update_at', models.DateTimeField(blank=True, null=True)),
                ('password', models.CharField(max_length=200)),
                ('role_code', models.SmallIntegerField()),
                ('status_code', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'old_users',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TokenTestData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('first_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='First name')),
                ('last_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Last name')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, unique=True, verbose_name='Email address')),
                ('google_login_present', models.BooleanField(default=False, verbose_name='google_login_present')),
                ('facebook_login_present', models.BooleanField(default=False, verbose_name='facebook_login_present')),
                ('username', models.CharField(blank=True, max_length=100, null=True, unique=True, verbose_name='Username')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=False, help_text='Designates whether this user should be treated as active.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date joined')),
                ('is_school', models.BooleanField(default=False)),
                ('school_name', models.CharField(blank=True, max_length=50, null=True)),
                ('is_student', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='child', to=settings.AUTH_USER_MODEL)),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'User',
                'verbose_name_plural': 'User',
                'ordering': ['-pk'],
            },
        ),
    ]
