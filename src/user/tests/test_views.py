from django.test import TestCase, Client
from user.models import User
from user.utils import OdooManager

class TestViews(TestCase):

    def setUp(self):
        self.odoo = OdooManager().odoo_login(odoo_db='test')
        self.school = User.objects.get(username='school1')
        self.OdooPartner = self.odoo.env['res.partner']
        self.OdooCompany = self.odoo.env['res.company']
        self.OdooUser = self.odoo.env['res.users']
        self.OdooCertificate = self.odoo.env['odoo_partner.certificates']
        self.OdooTemplate = self.odoo.env['odoo_partner.certificates.template']
        self.client = Client()
    
    def test_can_access_quiz(self):
        response = self.client.post('/user/token_info/', {'username': 'school1', 'password': 'school1'}).json()
        access_token = response.get('access')

        user_data = {
            "username": "testserver", "password": "testserver",
            "quiz_type": 1, "parent": self.school.id, "email": "testserver@example.com"
        }
        # login as school
        user = self.client.post('/user/', HTTP_AUTHORIZATION=f"Bearer {access_token}", data=user_data).json()
        self.assertEqual(user.get('email'), user_data['email'])

        # # login as student
        response = self.client.post('/user/token_info/', {'username': 'testserver@example.com', 'password': 'admin'}).json()
        access_token = response.get('access')

        # login as the created student
        access = self.client.get('/can-access-this-quiz/1/', HTTP_AUTHORIZATION=f"Bearer {access_token}")
        
        self.assertEqual(access.status_code, 200)


    def tearDown(self):
        try:
            user = User.objects.get(username='testserver')
            user.delete()
        except User.DoesNotExist as e:
            pass
        
        with self.assertRaises(User.DoesNotExist):
            user = User.objects.get(username='testserver')