from django.test import TestCase
from user.models import User
from user.utils import OdooManager
# Create your tests here.

class TestOdoo(TestCase):
    
    def setUp(self):
        self.odoo = OdooManager().odoo_login(odoo_db='test')
        self.student = User.objects.get(username='student1')
        self.school = User.objects.get(username='school1')
        self.OdooPartner = self.odoo.env['res.partner']
        self.OdooCompany = self.odoo.env['res.company']
        self.OdooUser = self.odoo.env['res.users']

    def test_parent(self):
        self.assertEqual(self.student.parent.id, self.school.id)

    def test_odoo_partner_matches_quiz_user(self):
        partner = self.OdooPartner.browse(self.student.odoo_id)
        self.assertEqual(self.student.email, partner.email)

    def test_odoo_parent_company_matches_quiz_user_parent(self):
        company = self.OdooCompany.browse(self.student.parent.odoo_id)
        self.assertEqual(self.student.parent.email, company.email)
        self.assertEqual(self.school.odoo_id, company.id)
