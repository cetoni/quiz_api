from django.test import TestCase, Client
from user.models import User
from user.utils import OdooManager
from project_apps.quiz_data.models import QuizModel

class TestUser(TestCase):

    def setUp(self):
        self.odoo = OdooManager().odoo_login(odoo_db='test')
        self.school = User.objects.get(username='school1')
        self.OdooPartner = self.odoo.env['res.partner']
        self.OdooCompany = self.odoo.env['res.company']
        self.OdooUser = self.odoo.env['res.users']
        self.OdooCertificate = self.odoo.env['odoo_partner.certificates']
        self.OdooTemplate = self.odoo.env['odoo_partner.certificates.template']
        self.client = Client()
        
    
    def test_create_child_student(self):
        response = self.client.post('/user/token_info/', {'username': 'school1', 'password': 'school1'}).json()
        access_token = response.get('access')

        user_data = {
            "username": "testserver", "password": "testserver",
            "quiz_type": 1, "parent": self.school.id, "email": "testserver@example.com"
        }
        user = self.client.post('/user/', HTTP_AUTHORIZATION=f"Bearer {access_token}", data=user_data).json()

        # check odoo partner creation
        partner = self.OdooPartner.browse(user.get('odoo_id'))
        self.assertEqual(partner.email, user.get('email'))

        # check company affiliation
        company = partner.company_id
        self.assertEqual(company.email, self.school.email)

        # check that partner has certificate 1
        quiz = QuizModel.objects.get(id=1)
        certificate = self.OdooCertificate.search([('template_id', '=', quiz.odoo_id), ('partner_id', '=', user.get('odoo_id'))])
        self.assertIsNot(certificate, [])

    def test_create_school(self):
        response = self.client.post('/user/token_info/', {'email': 'admin@example.com', 'password': 'admin'}).json()
        access_token = response.get('access')

        user_data = {
            "username": "testserver", "password": "testserver",
            "email": "testserver@example.com", "is_school": True
        }
        user = self.client.post('/user/', HTTP_AUTHORIZATION=f"Bearer {access_token}", data=user_data).json()

        # check odoo company and user creation
        company = self.OdooCompany.browse(user.get('odoo_id'))
        self.assertEqual(company.email, user.get('email'))
        

    def tearDown(self):
        try:
            user = User.objects.get(username='testserver')
            user.delete()
        except User.DoesNotExist as e:
            pass
        
        with self.assertRaises(User.DoesNotExist):
            user = User.objects.get(username='testserver')
