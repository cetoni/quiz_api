import odoorpc
from constance import config
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils import six
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from rest_framework import permissions

class OdooManager():
    _email = config.ODOO_EMAIL
    _password = config.ODOO_PASSWORD
    _odoo_db = config.ODOO_DB_NAME
    _odoo_domain = config.ODOO_DOMAIN_NAME

    def odoo_login(self, email=_email, password=_password, odoo_db=_odoo_db, odoo_domain=_odoo_domain):
        odoo = odoorpc.ODOO(odoo_domain, port=443, protocol='jsonrpc+ssl')
        odoo.login(odoo_db, email, password)
        return odoo
    
    def odoo_items(self, odoo_model):
        res = []
        odoo = self.odoo_login()
        Item = odoo.env[odoo_model]
        item_id = Item.search([])
        for item in Item.browse(item_id):
            res.append(item.name)
        return res

class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
                six.text_type(user.pk) + six.text_type(timestamp) +
                six.text_type(str(user)) + six.text_type(user.is_active)
        )


account_activation_token = AccountActivationTokenGenerator()


class IsUserRegistration(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method == "POST":
            return True
        else:
            return bool(request.user and request.user.is_authenticated)


def activation_email(view, form_serializer):
    user = form_serializer.save(commit=False)
    user.is_active = False
    user.save()

    current_site = get_current_site(view.request)
    message = render_to_string('account_activation_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
    })
    user.email_user(view.subject, message)
