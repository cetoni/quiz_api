from django import forms
from django.core.validators import RegexValidator

from user.models import User
from project_apps.quiz_data.models import QuizModel


class RegistrationForm(forms.ModelForm):
    """Registraion form based on user model"""

    first_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-group',
            'placeholder': 'Nome',
            'name': 'name'
            }), 
        label='')
    last_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-group',
            'placeholder': 'Cognome',
            'name': 'surname'
        }), 
        label='')
    email = forms.EmailField(
        widget=forms.TextInput(attrs={
            'class': 'form-group',
            'placeholder': 'Email',
            'name': 'email'
        }), 
        label='')
    phone = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-group',
            'placeholder': 'Telefono',
            'name': 'phone'
        }),
        label='')
    
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')
        required = ('first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.required:
            self.fields[field].required = True
        self.fields['quiz_type'] = forms.ModelChoiceField(
            queryset=QuizModel.objects.filter(show_on_cms=True),
            widget=forms.Select(attrs={
                'class': 'form-group',
                'style': 'width:100px'
            }),
            label='',
            empty_label='Quiz',
            required=True)

class CustomerForm(RegistrationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.required:
            self.fields[field].required = True

        self.fields['quiz_type'] = forms.ModelChoiceField(
            queryset=QuizModel.objects.filter(show_on_cms=True),
            widget=forms.Select(attrs={
                'class': 'form-group',
                'style': 'width:100px'
            }),
            label='',
            empty_label='Quiz',
            required=True)
        self.fields['driving_licence_expiration'] = forms.DateField(
            widget=forms.DateInput(attrs={
                'class': 'form-group',
                'name': 'expiration',
                'placeholder': 'Scadenza'
            }),
            label='',
            required=False)

class LoginForm(forms.Form):
    login_email = forms.CharField(max_length=100, required=True)
    login_password = forms.CharField(max_length=30, required=True)