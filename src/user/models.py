import uuid
import time
from datetime import datetime
from constance import config
from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from django_extensions.db.fields import AutoSlugField
from rest_framework.exceptions import ValidationError
from django.contrib.sites.models import Site

from schedule.models import Calendar

from . import utils


class UserAccountManager(BaseUserManager):
    def _create_user(self, **extra_fields):
        password = extra_fields.pop('password')
        user = self.model(**extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, **extra_fields):
        if extra_fields.get('is_school'):
            extra_fields.setdefault('is_active', True)
            extra_fields.setdefault('is_staff', True)
            extra_fields.setdefault('is_superuser', False)
            extra_fields.setdefault('school_name', extra_fields.get('username'))
        else:
            extra_fields.setdefault('is_staff', False)
            extra_fields.setdefault('is_superuser', False)
            extra_fields.setdefault('is_active', True)
        return self._create_user(**extra_fields)

    def create_superuser(self, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(**extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name='child')
    first_name = models.CharField(_('First name'), max_length=100, blank=True, null=True)
    last_name = models.CharField(_('Last name'), max_length=100, blank=True, null=True)

    phone = models.CharField(_('Phone'), max_length=30, blank=True, null=True)
    email = models.EmailField(_('Email address'), unique=True, blank=True, null=True)
    google_mail = models.EmailField(_('Google Email address'), unique=True, blank=True, null=True)
    facebook_mail = models.EmailField(_('Facebook Email address'), unique=True, blank=True, null=True)
    # TODO: trigger to set to false if mail fields are null
    google_login_present = models.BooleanField(_('Google Login'), default=False)
    facebook_login_present = models.BooleanField(_('Facebook Login'), default=False)

    username = models.CharField(_('Username'), max_length=100, unique=True, blank=True, null=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin site.'), )
    is_active = models.BooleanField(_('active'), default=True, help_text=_(
        'Designates whether this user should be treated as active.'), )
    date_joined = models.DateTimeField(_('Date joined'), default=timezone.now)

    is_school = models.BooleanField(default=False)
    school_image = models.ImageField(upload_to='images/', help_text=_('Only is_school can upload an image not student'), null=True, blank=True)
    school_name = models.CharField(max_length=50, blank=True, null=True)
    calendar = models.OneToOneField(Calendar, on_delete=models.CASCADE, null=True)
    odoo_id = models.IntegerField(_("odoo id"), null=True, blank=True)

    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    user_tracking = models.BooleanField(default=False)

    objects = UserAccountManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['is_school', 'is_student', 'is_teacher']

    # activation_email_subject = _("Account Activation Email from {}".format(config.SITE_NAME))

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('User')
        ordering = ['-pk']

    def __str__(self):
        if self.username:
            return self.username
        return self.email

    def clean(self):
        if self.is_school and self.is_student:
            raise ValidationError('A User can be a School or a Student, A User can not be both')
        if self.is_student:
            self.is_staff = False
        if self.is_school:
            self.user_tracking = True
        if not self.username:
            self.username = self.email
        if not self.is_school and self.school_image:
            raise ValidationError('A Student User cannot upload school image')

    def get_short_name(self):
        return self.first_name.strip() if self.first_name else ""

    def get_full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def email_user(self, subject, message):
        send_mail(subject, message, getattr(settings, 'EMAIL_HOST_USER', None), [self.email])

    # def activation_email(self, request):
    #     current_site = get_current_site(request)
    #     message = render_to_string('account_activation_email.html', {
    #         'user': self,
    #         'domain': current_site.domain,
    #         'uid': urlsafe_base64_encode(force_bytes(self.pk)),
    #         'token': utils.account_activation_token.make_token(self),
    #     })
    #     self.email_user(self.activation_email_subject, message)


@receiver(post_save, sender=User)
def create_calendar(sender, instance, created, **kwargs):
    if instance.is_school and created:
        school = instance
        school_name = school.school_name
        cal = Calendar(name=school_name, slug=school.id)
        cal.save()
        cal.create_relation(school)
        school.calendar_id = cal.id
        school.save()

# @receiver(post_save, sender=User)
def create_odoo_user(sender, instance, created, **kwargs):
    odoo = utils.OdooManager().odoo_login()    
    odoo_partner = odoo.env['res.partner']

    if instance.is_student and created:
        name = instance.username
        if not name:
            name = instance.email
        user_id = odoo_partner.create(dict(name=name, email=instance.email))
        if user_id:
            instance.odoo_id = user_id #TODO: Add a template with the quiz
            instance.save()

class SiteProfile(models.Model):
    user = models.ForeignKey('user.User', on_delete=models.CASCADE, related_name='user')
    site = models.ForeignKey(Site, on_delete=models.CASCADE, related_name='site')
    firebase_site_id = models.CharField(max_length=255, null=True, blank=True)

    def clean(self):
        if not self.user.is_school:
            raise ValidationError("Simple user can't store site")

    def __str__(self):
        return "{}".format(self.site)

    class Meta:
        db_table = 'site_profile'

class UserLogin(models.Model):
    """Represent users' logins, one per record"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True) 
    login_timestamp = models.DateTimeField()
    logout_timestamp = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name = 'User session'
        verbose_name_plural = 'User sessions'

@receiver(user_logged_in)
def post_login(sender, request, user, **kwargs):
    if user.last_login and user.user_tracking:
        user.userlogin_set.create(login_timestamp=user.last_login)

@receiver(user_logged_out)
def post_logout(sender, request, user, **kwargs):
    if user.last_login and user.user_tracking:
        last_login = user.userlogin_set.last()
        last_login.logout_timestamp = datetime.now()
        last_login.save()