from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

from user.models import OldCompanies as Parent, OldCustomers as Student, OldUsers as School

User = get_user_model()


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        # schools = School.objects.all()
        student = Student.objects.all()
        for user in student:
            try:
                u = User.objects.create_user(username=user.login, email=user.email, is_school=False,
                                             is_student=True, parent=User.objects.get(company_id=user.company.id),
                                             is_staff=False, is_active=True,
                                             password=None, first_name=user.name, last_name=user.surname)
                # print(user.name, user.email)
            except:
                print("---------except-------", user.name)
        print('all user migrations done')