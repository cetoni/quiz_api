import time
import odoorpc
from django.core.management.base import BaseCommand, CommandError

from project_apps.quiz_data.models import QuizModel
from user.utils import OdooManager

class Command(BaseCommand):
    help = 'Creates partner certificates templates from quizes'
    
    def add_arguments(self, parser):
        parser.add_argument('db_name')

    def handle(self, *args, **kwargs):
        odoo_db = kwargs.get('db_name')
        quizzes = QuizModel.objects.filter(odoo_id=None)
        odoo = OdooManager().odoo_login(odoo_db=odoo_db)

        for quiz in quizzes:
            template_id = None
            try:
                Template = odoo.env['odoo_partner.certificates.template']
                template_id = Template.create({
                    "name": quiz.title,
                    "description": quiz.description
                })
                quiz.odoo_id = template_id
                quiz.save()
            except Exception as e:
                print(e)
                time.sleep(5)
                continue
