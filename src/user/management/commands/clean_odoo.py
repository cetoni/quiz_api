import odoorpc
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from constance import config


User = get_user_model()

class Command(BaseCommand):
    help = "clean odoo users"

    def handle(self, *args, **kwargs):
        users = User.objects.exclude(odoo_id=None)
        q_ids = [u.id for u in users]
        
        try:
            email = config.ODOO_EMAIL
            password = config.ODOO_PASSWORD
            odoo_db = config.ODOO_DB_NAME
            odoo_domain = config.ODOO_DOMAIN_NAME
            
        except KeyError as e:
            self.stdout.write(self.style.ERROR(f"Missing argument {e}"))
            return

        odoo = odoorpc.ODOO(odoo_domain, port=443, protocol='jsonrpc+ssl')
        odoo.login(odoo_db, email, password)
        Partner = odoo.env['res.partner']

        ids = Partner.search([('id', 'not in', q_ids)])
        for id in ids:
            try:
                Partner.unlink(id)
            except odoorpc.error.RPCError:
                pass
