import odoorpc
import urllib
from datetime import datetime
from constance import config
from django.core.management.base import BaseCommand, CommandError

from user import models as m
from utils.colors import bcolors
from project_apps.quiz_data.models import QuizModel
from user.utils import OdooManager

class Command(BaseCommand):
    help = 'Creates odoo companies and partners from existing users'

    def add_arguments(self, parser):
        parser.add_argument('db_name')
        parser.add_argument('template')
        parser.add_argument('company')

    def handle(self, *args, **kwargs):
        odoo_db = kwargs.get('db_name')
        odoo = OdooManager().odoo_login(odoo_db=odoo_db)
        Partner = odoo.env['res.partner']
        Certificate = odoo.env['odoo_partner.certificates']
        # odoo.env.user.company_id = 4, we have to set the env.user.company_id to the current company
        ids = Partner.search([('parent_id', '=', int(kwargs.get('company')))])
        for partner in Partner.browse(ids):
            cert = Certificate.create({
                'name': 'Formazione Rischio Biologico',
                'template_id': int(kwargs.get('template')),
                'partner_id': partner.id,
            })
            print(cert)
        