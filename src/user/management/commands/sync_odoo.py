import odoorpc
import urllib
import json
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError

from user import models as m
from utils.colors import bcolors
from project_apps.quiz_data.models import QuizModel
from user.utils import OdooManager

class Command(BaseCommand):
    help = 'Creates odoo companies and partners from existing users'

    def add_arguments(self, parser):
        parser.add_argument('db_name')

    def handle(self, *args, **kwargs):
        odoo_db = kwargs.get('db_name')
        odoo = OdooManager().odoo_login(odoo_db=odoo_db)
        Company = odoo.env['res.company']
        Partner = odoo.env['res.partner']
        User = odoo.env['res.users']

        companies = m.User.objects.filter(is_school=True, odoo_id=None).order_by('id')
        students = m.User.objects.filter(is_student=True, odoo_id=None).order_by('id')

        print(datetime.today().date().strftime('%Y/%m/%d'))
        for company in companies:
            user_id = None
            found = None
            now = datetime.now()
            time = now.strftime("%H:%M:%S")
            try:
                found = Company.search([('email', '=', company.email), ('name', '=', company.school_name)])
                # if the user is found in odoo get the id
                if found:
                    company_id = found[0]
                else:
                    company_id = Company.create({
                        'name': company.school_name,
                        'email': company.email
                    })

                company.odoo_id = company_id
                company.save()

                _user = odoo.env.user
                _user.company_id = company_id

                try:

                    user_id = User.create({
                        'name': company.username,
                        'login': company.email,
                        'email': company.email,
                        'password': company.username,
                        'company_id': company_id,
                        'notification_type': 'inbox',
                        'in_group_7': True,
                        'lang': 'it_IT',
                        'sel_groups_1_9_10': 1,
                        'sel_groups_2_3': 0,
                        'lang': 'it_IT'
                    })

                except odoorpc.error.RPCError as er:
                    print(f'{time} : {bcolors.WARNING}{er}{bcolors.ENDC} , updating user ...')
                    user_id = User.search([('login', '=', company.email)])
                    User.write(user_id, { 'company_id': company_id })

            except odoorpc.error.RPCError as e:
                print(f'{time} : {bcolors.FAIL}failed for {company} | {e}{bcolors.ENDC}')
                continue

        for user in students:
            _user = odoo.env.user
            if not user.parent:
                continue
            elif not user.parent.odoo_id:
                continue
            else:
                _user.company_id = user.parent.odoo_id

            now = datetime.now()
            time = now.strftime("%H:%M:%S")

            try:
                company_id = None
                parent_id = None
                parent = user.parent
                name = user.last_name
                first_name = user.first_name
                email = user.email
                phone = user.phone or 0

                if not name:
                    name = f'{user.first_name}{user.last_name}'
                if not email:
                    email = name

                if parent:
                    odoo_parent_id = Company.search([('id', '=', parent.odoo_id)])
                    if odoo_parent_id:
                        odoo_parent = Company.browse(odoo_parent_id[0])
                        parent_id = odoo_parent.partner_id.company_id.partner_id.id
                        company_id = parent.odoo_id

                partner_id = Partner.create(dict(name=name, email=email, phone=phone, firstname=first_name, lastname=name,
                                                    company_id=company_id, customer=True,
                                                    quiz_api_id=user.id))
                user.odoo_id = partner_id
                user.save()
            except odoorpc.error.RPCError as e:
                print(f'{time} : {bcolors.FAIL}failed for {user} : cause {e}{bcolors.ENDC}')
                continue