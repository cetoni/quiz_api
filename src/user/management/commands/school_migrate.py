from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

from user.models import OldCompanies as Parent, OldCustomers as Student, OldUsers as School

User = get_user_model()


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        schools = School.objects.all()
        # student = Student.objects.all()
        for user in schools:
            try:
                u = User.objects.create_user(username=user.name, email=user.email, is_school=True,
                                             is_student=False, parent=None,
                                             is_staff=True, is_active=True,
                                             password=None, first_name=None, last_name=None, company_id=user.company_id)
                # print(user.name, user.email)
            except:
                print("---------except-------", user.name)
        print('all user migrations done')
