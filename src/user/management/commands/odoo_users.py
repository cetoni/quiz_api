import odoorpc
from django.db.models import Q
from datetime import datetime
from constance import config
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError

from user import models as m
from project_apps.quiz_data.models import QuizModel
from utils.colors import bcolors


class Command(BaseCommand):
    help = 'Creates quiz users from odoo partners'

    def handle(self, *args, **kwargs):
        missing_from_quiz = []
        users = m.User.objects.all()
        
        try:
            email = config.ODOO_EMAIL
            password = config.ODOO_PASSWORD
            odoo_db = config.ODOO_DB_NAME
            odoo_domain = config.ODOO_DOMAIN_NAME
            
        except KeyError as e:
            self.stdout.write(self.style.ERROR(f"Missing argument {e}"))
            return

        odoo = odoorpc.ODOO(odoo_domain, port=443, protocol='jsonrpc+ssl')
        odoo.login(odoo_db, email, password)
        Company = odoo.env['res.company']
        Partner = odoo.env['res.partner']
        User = odoo.env['res.users']

        odoo_partner_ids = Partner.search([['quiz_api_id', '=', None]])

        for partner in Partner.browse(odoo_partner_ids):
            now = datetime.now()
            time = now.strftime("%H:%M:%S")
            try:
                if partner.is_company:
                    user = m.User.objects.get(email=partner.email, is_school=True)
                else :
                    if partner.email:
                        user = m.User.objects.get(email=partner.email)
                    elif partner.name:
                        user = m.User.objects.get(username=partner.name)
                    user.odoo_id = partner.id
                user.save()
                
            except m.User.DoesNotExist as e:
                if partner.is_company:
                    user_data = {
                        'username': partner.name, 'email': partner.email,
                        'password': partner.name, 'is_school': True, 'odoo_id': partner.company_id.id
                    }
                else:
                    try:
                        parent = m.User.objects.get(odoo_id=partner.company_id.id)
                    except m.User.DoesNotExist as e:
                        print(f'{time} : {bcolors.WARNING}Did not find a parent user in quiz with odoo_id = {partner.company_id}{bcolors.ENDC}')
                        parent = None
                    user_data = {
                        'username': partner.name, 'email': partner.email, 'parent': parent,
                        'password': partner.name, 'is_student': True, 'odoo_id': partner.id
                    }
                try:
                    user = m.User.objects.create_user(**user_data)
                except IntegrityError as e:
                    print(f'{time} : {bcolors.FAIL}{e}{bcolors.ENDC}')
                    continue

            Partner.write([partner.id], {'quiz_api_id': user.id})