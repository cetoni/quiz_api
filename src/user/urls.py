from rest_framework.routers import DefaultRouter

from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView

from . import views
from .serializers import UserTokenInfoSerializer
from project_apps.services_handler.views import SubscriptionList

router = DefaultRouter(trailing_slash=True)
router.register('', views.UserAPIView, basename='crud_user')

app_name = 'user'

urlpatterns = [
    path('info/', views.UserInfoAPIView.as_view(), name='user_info'),
    path('info/<pk>', views.UserDetailAPIView.as_view(), name='user-detail'),
    path('company-logo/<pk>', views.CompanyLogoAPIView.as_view(), name='user-detail'),
    path('token_info/', TokenObtainPairView.as_view(serializer_class=UserTokenInfoSerializer), name='create_user_token'),
    path('create_student/', TokenObtainPairView.as_view(serializer_class=UserTokenInfoSerializer),
         name='user_token_info'),
    path('deactivate/<int:user_id>/', views.UserActivation.as_view(), name='activation'),
    # path('create_user/', views.UserAPIView.as_view(), name='user_create'),
    path('google_login_url/', views.GoogleLoginUrl.as_view(), name='user_token_info'),
    path('google_login/', views.SocialLoginView.as_view(), name='google_login'),
    path('facebook_login_url/', views.FacebookLoginUrl.as_view(), name='user_token_info'),
    path('facebook_login/', views.FacebookLoginView.as_view(), name='facebook_login'),
    path('reset-password-token/', views.ResetPasswordRequestToken.as_view(), name='reset_password_token'),
    # path('reset/<new_token>/<email>/', views.ResetPassword.as_view(), name='reset_password_new'),
    path('reset-password/<reset_token>/<email>/', views.ResetPasswordValidateToken.as_view(), name='reset_password'),
    path('reset-password/', views.ResetPasswordValidateToken.as_view(), name='reset_password_backend'),
    path('reset-password/verify-token/', views.verify_token.as_view(), name='verify_token'),
    path('subscriptions/', SubscriptionList.as_view(), name='user_subscriptions'),
    path('quiz_list/', views.UserQuiz.as_view(), name='user_quiz_list')
    # path('health-checks-home/', views.HealthChecksHome.as_view(), name='health_checks_home'), TODO: uncomment when fixed (missing view)
]

urlpatterns += router.urls
