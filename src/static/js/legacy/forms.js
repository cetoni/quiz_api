var registered = localStorage.getItem('registered');
var $loadingRegistration = $('#loading-registration').hide();
var registration = $('#registration-form');
var $loadingCustomerRegistration = $('#loading-customer').hide();
var customerRegistration = $('#registration-customer-form');

$("#registration")
    .ajaxStart(function () {
        registration.hide();
        $loadingRegistration.show();
    })
    .ajaxStop(function () {
        $loadingRegistration.hide();
});

$("#registration-customer")
    .ajaxStart(function () {
        customerRegistration.hide();
        $loadingCustomerRegistration.show();
    })
    .ajaxStop(function () {
        $loadingCustomerRegistration.hide();
});

$(document).ready(function() {
   
    $( "#id_driving_licence_expiration" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
      
    $("#registration").submit(function(e) {
        $("#notice").empty();
        
        e.preventDefault();
        
        let csrftoken = Cookies.get('csrftoken');
        let form = $(this);
        let url = form.attr('action');
        let data = {
            'first_name': $('#id_first_name').val(),
            'last_name': $('#id_last_name').val(),
            'email':  $('#id_email').val(),
            'password': $('#id_first_name').val() + '.' + $('#id_last_name').val(),
            'is_school': false,
            'quiz_type': $('#id_quiz_type').val(),
            'form': 'student',
            'phone': $('#id_phone').val()
        };
        console.log(data);

        function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        };

        $.ajax({
            url: url,
            beforeSend: function(xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function(data){
                $("#notice").attr("class", "alert alert-success")
                    .append("<p id='result'> Welcome "+ data.first_name + " " + data.last_name + "</p>");
                localStorage.setItem('registered', true);
                registration.hide();
                customerRegistration.hide();
            },
            error: function(xhr, text, err) {
                console.log(data)
                let m = JSON.stringify(xhr.responseJSON);
                $("#notice").attr("class", "alert alert-danger")
                    .append("<p id='result'> " + m + "</p>");
                registration.show();
            },
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
        });
    });
    $("#registration-customer").submit(function(e) {
        $("#notice").empty();
        
        e.preventDefault();
        
        let csrftoken = Cookies.get('csrftoken');
        let form = $(this);
        let url = form.attr('action');
        let data = {
            'first_name': $('#registration-customer > #id_first_name').val(),
            'last_name': $('#registration-customer > #id_last_name').val(),
            'email':  $('#registration-customer > #id_email').val(),
            'password': $('#registration-customer > #id_first_name').val() + '.' + $('#id_last_name').val(),
            'is_school': false,
            'quiz_type': $('#registration-customer > #id_quiz_type').val(),
            'driving_licence_expiration': $('#registration-customer > #id_driving_licence_expiration').val(),
            'form': 'customer',
            'phone': $('#registration-customer > #id_phone').val()
        };

        function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        };

        $.ajax({
            url: url,
            beforeSend: function(xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function(data){
                $("#notice").attr("class", "alert alert-success")
                    .append("<p id='result'> Welcome "+ data.first_name + " " + data.last_name + "</p>");
                registration.hide();
                customerRegistration.hide();
            },
            error: function(xhr, text, err) {
                console.log(data)
                let m = JSON.stringify(xhr.responseJSON);
                $("#notice").attr("class", "alert alert-danger")
                    .append("<p id='result'> " + m + "</p>");
                customerRegistration.show();
            },
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
        });
    });
    $('#login').submit(function(e) {
            e.preventDefault();
        
        let csrftoken = Cookies.get('csrftoken');
        let form = $(this);
        let url = form.attr('action');
        let data = {
            'email':  $('#id_login_email').val(),
            'password': $('#id_login_password').val(),
            'is_school': false
        }
        function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        $.ajax({
            url: url,
            beforeSend: function(xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function(data){
                $("#notice").attr("class", "alert alert-success")
                    .append("<p id='result'> Welcome "+ data.user.first_name + " " + data.user.last_name + "</p>");
            },
            error: function(xhr, text, err) {
                let m = JSON.stringify(xhr.responseJSON);
                $("#notice").attr("class", "alert alert-danger")
                    .append("<p id='result'> " + m + "</p>");
                formContent.show();
            },
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
        });
    });
});