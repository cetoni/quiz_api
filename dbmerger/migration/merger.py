import itertools

from abc import ABC, abstractmethod

from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import create_engine
from sqlalchemy.exc import DataError

from legacy.models import Question, BlackList
from models import QuizQuestion, QuestionCategory
from session import Session


class Merger(ABC):
    def __init__(self):
        self.session = Session().get()

    @abstractmethod
    def run(self) -> None:
        pass


def merge(merger: Merger):
    merger.run()


class MergeQuestion(Merger):
    """
    Merges data into `quiz_data_questionmodel`
    from `questions`. Inherits from abstract class
    Merger and implements the method run.
    """

    _session = Session().get_old_db()

    def run(self) -> None:
        def to_question(value) -> QuizQuestion:
            return QuizQuestion(
                text=value.text,
                image=value.image,
                answer_choices='{"1": "V", "2": "F"}',
                right_answers=f"[\"{value.answer}\"]", 
                category_id=assign_category(value),
            )

        # TODO: USE DICTIONARIES
        def assign_category(value) -> int:
            if value.quiz_type == 2:
                return value.chapter_id + 11
            if value.quiz_type == 50:
                return value.topic_id + 107
            if value.quiz_type == 4:
                return value.topic_id + 46
            raise Exception("Error Assigning Category")

        self.session.bulk_save_objects(
            list(
                map(
                    to_question,
                    self._session.query(Question).filter(
                        Question.quiz_type.in_([2, 50, 4])
                    ),
                )
            )
        )
        self.session.commit()


if __name__ == "__main__":
    merge(MergeQuestion())
