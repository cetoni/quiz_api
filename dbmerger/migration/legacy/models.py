from typing import List, AnyStr

from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Question(Base):

    id = Column("id", Integer, primary_key=True, nullable=False)
    text = Column("text", String(500), nullable=False)
    answer = Column("answer", Integer, nullable=False)
    image = Column("image", String(10), nullable=True)
    image_part = Column("image_part", String(10), nullable=True)
    explanation = Column("explanation", String(500), nullable=True)
    topic_id = Column("topic_id", Integer, nullable=False)
    quiz_type = Column("quiz_type", Integer, primary_key=True, nullable=False)
    chapter_id = Column("chapter_id", Integer, nullable=False)

    __tablename__ = "questions"


class BlackList(Base):
    id = Column("id", Integer, nullable=False, primary_key=True, autoincrement=True)
    quiz_type = Column("quiz_type", Integer, nullable=False, primary_key=True)

    __tablename__ = "blacklist"
