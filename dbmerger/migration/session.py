import os

from configparser import ConfigParser

from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker


class SessionMeta(type):
    _instance = None
    _config = ConfigParser()
    _config.read(os.path.join(os.path.dirname(os.getcwd()), "config.ini"))

    def __call__(self):
        if self._instance is None:
            self._instance = super().__call__()
        return self._instance


class Session(metaclass=SessionMeta):
    _config = ConfigParser()
    _config.read(os.path.join(os.path.dirname(os.getcwd()), "config.ini"))
    _host = _config["database"]["host"]
    _user = _config["database"]["user"]
    _password = _config["database"]["password"]
    _db = _config["database"]["db"]
    _db_new = _config["database"]["db_new"]

    def get_old_db(self):
        engine = create_engine(
            f"mysql+pymysql://{self._user}:{self._password}@{self._host}/{self._db}"
        )
        session = sessionmaker(bind=engine)
        return session()

    def get(self):
        engine = create_engine(
            f"mysql+pymysql://{self._user}:{self._password}@{self._host}/{self._db_new}"
        )
        session = sessionmaker(bind=engine)
        return session()
