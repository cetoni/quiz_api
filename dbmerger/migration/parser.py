from pandas import read_excel
from collections import defaultdict
from pprint import pprint

import itertools
import re

file = read_excel(
    io="/home/saadman/Downloads/QUIZ CFP ADR 2019.xls",
    sheet_name=[x for x in range(2, 37)],
    usecols="A,B,C,D",
    na_filter=False,
    keep_default_na=False,
    index_col=[0, 1, 2, 3],
    skip_rows=1,
    encoding='latin1'
)


def data_from_excel():
    for _, y in file.items():
        for i in set([id for id, q, a, s in y.index]):
            try:
                yield {
                    "id": i,
                    "question": [
                        qq
                        for qk, qq, qa, qs in y.index
                        if qk == i and qq != "" and len(qq) > 2
                    ],
                    "answer": {
                        v[2]:v[3]
                        for v in set([v for v in y.index if v[0] == i])
                    },
                    "image": [
                        re.search("(n.) (\d+)", sq).group(2)
                        for sg, sq, sa, ss in y.index
                        if re.search("(n.) (\d+)", sq) and sg == i
                    ],
                }
            except Exception as e:
                print(e)

