import os

from configparser import ConfigParser
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, MetaData

config = ConfigParser()
config.read(os.path.join(os.path.dirname(os.getcwd()), "../config.ini"))

class Migration(object):

    def __init__(self, db,
            classes: list=[],
            user=config['database']['user'],
            password=config['database']['password'],
            host=config['database']['host']
    ):
        engine = create_engine(
            f"mysql+pymysql://{user}:{password}@{host}/{db}"
        )
        if classes:
            metadata = MetaData()
            metadata.reflect(engine, only=classes)
            Base = automap_base(metadata=metadata)
            Base.prepare()
        else:
            Base = automap_base()
            Base.prepare(engine, reflect=True)
        self.engine = engine
        self.session = Session(self.engine)
        self.base = Base

    @property
    def classes(self):
        return self.base.classes