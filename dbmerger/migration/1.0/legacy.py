from core import Migration

legacy = Migration('legacy_quiz')
"""
    legacy.classes.users
+---------------+-------------------------+------+-----+---------------------+-------+
| Field         | Type                    | Null | Key | Default             | Extra |
+---------------+-------------------------+------+-----+---------------------+-------+
| id            | int(10) unsigned        | NO   | PRI | NULL                |       |
| type          | enum('student','guest') | NO   |     | NULL                |       |
| quiz_type     | smallint(5) unsigned    | NO   | PRI | NULL                |       |
| school_id     | int(10) unsigned        | NO   | PRI | NULL                |       |
| last_visit    | timestamp               | NO   | MUL | 0000-00-00 00:00:00 |       |
| progress_coef | float                   | NO   |     | -1                  |       |
+---------------+-------------------------+------+-----+---------------------+-------+

    legacy.classes.questions
+-------------+----------------------+------+-----+---------+-------+
| Field       | Type                 | Null | Key | Default | Extra |
+-------------+----------------------+------+-----+---------+-------+
| id          | int(10) unsigned     | NO   | PRI | NULL    |       |
| quiz_type   | smallint(5) unsigned | NO   | PRI | NULL    |       |
| text        | varchar(500)         | NO   |     | NULL    |       |
| text_fr     | varchar(500)         | YES  |     | NULL    |       |
| text_de     | varchar(500)         | YES  |     | NULL    |       |
| answer      | tinyint(1)           | NO   |     | NULL    |       |
| image       | varchar(10)          | YES  |     | NULL    |       |
| image_part  | varchar(10)          | YES  |     | NULL    |       |
| chapter_id  | smallint(5) unsigned | NO   | MUL | NULL    |       |
| topic_id    | int(10) unsigned     | NO   | MUL | NULL    |       |
| explanation | varchar(500)         | YES  |     | NULL    |       |
+-------------+----------------------+------+-----+---------+-------+

    legacy.classes.exams
+------------+----------------------+------+-----+---------+----------------+
| Field      | Type                 | Null | Key | Default | Extra          |
+------------+----------------------+------+-----+---------+----------------+
| id         | int(10) unsigned     | NO   | PRI | NULL    | auto_increment |
| quiz_type  | smallint(5) unsigned | NO   | PRI | NULL    |                |
| user_id    | int(10) unsigned     | NO   | MUL | NULL    |                |
| start_time | datetime             | NO   | MUL | NULL    |                |
| end_time   | datetime             | YES  | MUL | NULL    |                |
| err_count  | smallint(5) unsigned | NO   |     | 0       |                |
+------------+----------------------+------+-----+---------+----------------+
"""

accounts = Migration('legacy_accounts')
"""
    accounts.classes.companies
+--------------------------+--------------+------+-----+---------+----------------+
| Field                    | Type         | Null | Key | Default | Extra          |
+--------------------------+--------------+------+-----+---------+----------------+
| id                       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name                     | varchar(225) | NO   |     | NULL    |                |
| street                   | varchar(225) | YES  |     | NULL    |                |
| street_number            | varchar(20)  | YES  |     | NULL    |                |
| town_code                | varchar(5)   | YES  | MUL | NULL    |                |
| province_code            | varchar(4)   | YES  | MUL | NULL    |                |
| zip                      | int(11)      | YES  |     | NULL    |                |
| phone                    | varchar(225) | YES  |     | NULL    |                |
| mail                     | varchar(225) | NO   |     | NULL    |                |
| certified_mail           | varchar(225) | YES  |     | NULL    |                |
| infomot_username         | varchar(225) | YES  |     | NULL    |                |
| infomot_password         | varchar(225) | YES  |     | NULL    |                |
| infomot_pin              | varchar(225) | YES  |     | NULL    |                |
| infomot_codiceoperatore  | varchar(225) | YES  |     | NULL    |                |
| infomot_ufficiooperativo | varchar(225) | YES  |     | NULL    |                |
| notes                    | text         | YES  |     | NULL    |                |
| avatar                   | varchar(200) | YES  |     | NULL    |                |
| production               | tinyint(1)   | NO   |     | NULL    |                |
| ai                       | tinyint(1)   | NO   |     | 0       |                |
+--------------------------+--------------+------+-----+---------+----------------+

    accounts.classes.customers
+------------------------+--------------+------+-----+---------+----------------+
| Field                  | Type         | Null | Key | Default | Extra          |
+------------------------+--------------+------+-----+---------+----------------+
| id                     | int(11)      | NO   | PRI | NULL    | auto_increment |
| name                   | varchar(225) | NO   |     | NULL    |                |
| login                  | varchar(100) | NO   | UNI | NULL    |                |
| fb_id                  | varchar(28)  | YES  |     | NULL    |                |
| password               | varchar(60)  | NO   |     | NULL    |                |
| surname                | varchar(225) | NO   |     | NULL    |                |
| email                  | varchar(225) | YES  | UNI | NULL    |                |
| photo                  | varchar(225) | YES  |     | NULL    |                |
| signature              | varchar(225) | YES  |     | NULL    |                |
| sex                    | varchar(2)   | YES  |     | NULL    |                |
| street                 | varchar(225) | YES  |     | NULL    |                |
| street_number          | varchar(225) | YES  |     | NULL    |                |
| town_code              | varchar(5)   | YES  | MUL | NULL    |                |
| province_code          | varchar(4)   | YES  | MUL | NULL    |                |
| zip                    | int(11)      | YES  |     | NULL    |                |
| phone                  | varchar(225) | YES  |     | NULL    |                |
| certified_mail         | varchar(225) | YES  |     | NULL    |                |
| birth_date             | date         | YES  |     | NULL    |                |
| birth_province_code    | varchar(4)   | YES  | MUL | NULL    |                |
| birth_town_code        | varchar(5)   | YES  | MUL | NULL    |                |
| social_security_number | varchar(20)  | YES  |     | NULL    |                |
| avatar                 | varchar(200) | YES  |     | NULL    |                |
| company_id             | int(11)      | YES  | MUL | NULL    |                |
| is_student             | tinyint(1)   | NO   |     | 1       |                |
| gdpr_accepted          | timestamp    | YES  |     | NULL    |                |
| ig_id                  | varchar(28)  | YES  |     | NULL    |                |
+------------------------+--------------+------+-----+---------+----------------+
"""