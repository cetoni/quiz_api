from core import Migration

quiz_api = Migration('quiz',
    ['user_user', 'quiz_data_exammodel', 'quiz_data_quizmodel',
    'quiz_data_examquestionset', 'quiz_data_examsetquestionsequence',
    'quiz_data_answermodel', 'quiz_data_questionmodel', 'quiz_data_questioncategorymodel',
    'quiz_data_exammodel', 'quiz_data_quizmodel_categories'
    ])
"""
    quiz_api.classes.user_user
+------------------------+--------------+------+-----+---------+----------------+
| Field                  | Type         | Null | Key | Default | Extra          |
+------------------------+--------------+------+-----+---------+----------------+
| id                     | int(11)      | NO   | PRI | NULL    | auto_increment |
| password               | varchar(128) | NO   |     | NULL    |                |
| last_login             | datetime(6)  | YES  |     | NULL    |                |
| is_superuser           | tinyint(1)   | NO   |     | NULL    |                |
| first_name             | varchar(100) | YES  |     | NULL    |                |
| last_name              | varchar(100) | YES  |     | NULL    |                |
| email                  | varchar(254) | YES  | UNI | NULL    |                |
| is_staff               | tinyint(1)   | NO   |     | NULL    |                |
| is_active              | tinyint(1)   | NO   |     | NULL    |                |
| date_joined            | datetime(6)  | NO   |     | NULL    |                |
| is_school              | tinyint(1)   | NO   |     | NULL    |                |
| school_name            | varchar(50)  | YES  |     | NULL    |                |
| is_student             | tinyint(1)   | NO   |     | NULL    |                |
| parent_id              | int(11)      | YES  | MUL | NULL    |                |
| username               | varchar(100) | YES  | UNI | NULL    |                |
| facebook_login_present | tinyint(1)   | NO   |     | NULL    |                |
| google_login_present   | tinyint(1)   | NO   |     | NULL    |                |
| calendar_id            | int(11)      | YES  | UNI | NULL    |                |
| is_teacher             | tinyint(1)   | NO   |     | NULL    |                |
| facebook_mail          | varchar(254) | YES  | UNI | NULL    |                |
| google_mail            | varchar(254) | YES  | UNI | NULL    |                |
| phone                  | varchar(30)  | YES  |     | NULL    |                |
| school_image           | varchar(100) | YES  |     | NULL    |                |
| user_tracking          | tinyint(1)   | NO   |     | NULL    |                |
| odoo_id                | int(11)      | YES  |     | NULL    |                |
+------------------------+--------------+------+-----+---------+----------------+

    quiz_api.classes.quiz_data_exammodel
+----------------+------------------+------+-----+---------+----------------+
| Field          | Type             | Null | Key | Default | Extra          |
+----------------+------------------+------+-----+---------+----------------+
| created_at     | datetime(6)      | NO   |     | NULL    |                |
| updated_at     | datetime(6)      | NO   |     | NULL    |                |
| exam_id        | int(11)          | NO   | PRI | NULL    | auto_increment |
| result         | varchar(50)      | NO   |     | NULL    |                |
| passing_marks  | int(10) unsigned | NO   |     | NULL    |                |
| obtained_marks | int(11)          | NO   |     | NULL    |                |
| quiz_id        | int(11)          | YES  | MUL | NULL    |                |
| student_id     | int(11)          | NO   | MUL | NULL    |                |
| is_over        | tinyint(1)       | NO   |     | NULL    |                |
+----------------+------------------+------+-----+---------+----------------+

    quiz_api.classes.quiz_data_quizmodel
+---------------------------+------------------+------+-----+---------+----------------+
| Field                     | Type             | Null | Key | Default | Extra          |
+---------------------------+------------------+------+-----+---------+----------------+
| id                        | int(11)          | NO   | PRI | NULL    | auto_increment |
| created_at                | datetime(6)      | NO   |     | NULL    |                |
| updated_at                | datetime(6)      | NO   |     | NULL    |                |
| title                     | varchar(100)     | NO   |     | NULL    |                |
| description               | longtext         | YES  |     | NULL    |                |
| passing_marks             | int(10) unsigned | NO   |     | NULL    |                |
| exam_duration             | int(10) unsigned | NO   |     | NULL    |                |
| requires_video_completion | tinyint(1)       | NO   |     | NULL    |                |
| show_on_cms               | tinyint(1)       | NO   |     | NULL    |                |
| odoo_id                   | int(11)          | YES  |     | NULL    |                |
+---------------------------+------------------+------+-----+---------+----------------+
"""