import logging
import os
import unittest
from configparser import ConfigParser

from main import QUIZ_TYPES, create_categories, get_category, get_quiz_lookup
from sqlalchemy import MetaData, create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session

quiz_api_classes = ['quiz_data_examquestionset', 'quiz_data_examsetquestionsequence',
    'quiz_data_answermodel', 'quiz_data_questionmodel', 'quiz_data_questioncategorymodel',
    'quiz_data_exammodel', 'quiz_data_quizmodel_categories'
    ]

config = ConfigParser()
config.read(os.path.join(os.path.dirname(os.getcwd()), "../config.ini"))


def get_session(db, classes=[]):
    user = config['database']['user']
    password = config['database']['password']
    host = config['database']['host']
    engine = create_engine(f"mysql+pymysql://{user}:{password}@{host}/{db}")
    metadata = MetaData()
    if classes:
        metadata = MetaData()
        metadata.reflect(engine, only=classes)
        Base = automap_base(metadata=metadata)
        Base.prepare()
    else:
        Base = automap_base()
        Base.prepare(engine, reflect=True)
    session = Session(engine)
    return session, Base.classes

class TestMigration(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.quiz, self.quiz.classes = get_session('test_quiz_migration', classes=quiz_api_classes)
        self.legacy, self.legacy.classes = get_session('legacy_quiz')
        self.accounts, self.accounts.classes = get_session('legacy_accounts')

    def test_categories_creation(self):
        pass
        for quiz_type in QUIZ_TYPES:
            quiz_name, name = get_quiz_lookup(quiz_type)
            lookup = quiz_name or name
            quiz = self.quiz.query(self.quiz.classes.quiz_data_quizmodel).filter_by(title=lookup).first()
            self.assertIsNotNone(quiz)
            for q in [quiz_type]:
                topics = self.legacy.query(self.legacy.classes.topics).filter_by(quiz_type=q).all()
                chapters = self.legacy.query(self.legacy.classes.chapters).filter_by(quiz_type=q).all()
                for chapter in chapters:
                    category = self.quiz.query(self.quiz.classes.quiz_data_questioncategorymodel).filter(
                        self.quiz.classes.quiz_data_questioncategorymodel.title == f'({name}) {chapter.text}',
                    ).first()
                    self.assertIsNotNone(category)
                    quiz_relation = self.quiz.query(self.quiz.classes.quiz_data_quizmodel_categories).filter(
                        self.quiz.classes.quiz_data_quizmodel_categories.quizmodel_id == quiz.id,
                        self.quiz.classes.quiz_data_quizmodel_categories.questioncategorymodel_id == category.id
                    ).first()
                    self.assertIsNotNone(quiz_relation)

    def test_categories_parent_child_relation(self):
        pass
        for quiz_type in QUIZ_TYPES:
            quiz_name, name = get_quiz_lookup(quiz_type)
            lookup = quiz_name or name
            quiz = self.quiz.query(self.quiz.classes.quiz_data_quizmodel).filter_by(title=lookup).first()
            for q in [quiz_type]:
                chapters = self.legacy.query(self.legacy.classes.chapters).filter_by(quiz_type=q).all()
                for chapter in chapters:
                    topics = self.legacy.query(self.legacy.classes.topics).filter(
                        self.legacy.classes.topics.quiz_type == quiz_type,
                        self.legacy.classes.topics.chapter_id == chapter.id
                    ).all()
                    topic_titles = [f'({name}) {t.text}' for t in topics]
                    category = self.quiz.query(self.quiz.classes.quiz_data_questioncategorymodel).filter(
                        self.quiz.classes.quiz_data_questioncategorymodel.title == f'({name}) {chapter.text}',
                    ).first()
                    children = self.quiz.query(self.quiz.classes.quiz_data_questioncategorymodel).filter(
                        self.quiz.classes.quiz_data_questioncategorymodel.category_id == category.id,
                    ).all()
                    children = set(children)
                    if len(children) == len(topic_titles):
                        for child in children:
                            if not child.title in topic_titles:
                                self.assertIn(child.title, topic_titles)
                    self.assertGreaterEqual(len(children), len(topic_titles))

    def test_questions_should_match(self):
        pass
        for quiz_type in QUIZ_TYPES:
            quiz_name, name = get_quiz_lookup(quiz_type)
            topics = self.legacy.query(self.legacy.classes.topics).filter_by(quiz_type=quiz_type).all()
            for topic in topics:
                category = get_category(topic, prefix=name)
                questions = self.quiz.query(self.quiz.classes.quiz_data_questionmodel).filter(
                    self.quiz.classes.quiz_data_questionmodel.category_id == category.id
                ).all()
                for q in questions:
                    legacy_q = self.legacy.query(self.legacy.classes.questions).filter(
                        self.legacy.classes.questions.text == q.text,
                        self.legacy.classes.questions.quiz_type == quiz_type,
                        self.legacy.classes.questions.topic_id == topic.id
                    ).first()
                    self.assertIsNotNone(legacy_q)

    def test_exams_migration(self):
        quizes = QUIZ_TYPES
        exams = self.legacy.query(self.legacy.classes.exams).filter(
            self.legacy.classes.exams.quiz_type.in_(quizes),
            self.legacy.classes.exams.user_id.in_(
                [u.id for u in self.accounts.query(self.accounts.classes.customers).all()]
            )
        ).limit(10)
        for exam in exams:
            students = self.accounts.query(self.accounts.classes.customers).filter(
                self.accounts.classes.customers.id == exam.user_id,
                self.accounts.classes.customers.is_student == True
            ).all()
            if len(students) != 1:
                continue
            student = students[0]

            quiz_api_students = self.quiz.query(self.quiz.classes.user_user).filter(
                self.quiz.classes.user_user.username == student.login,
                self.quiz.classes.user_user.is_student == True
            ).all()
            self.assertEqual(len(quiz_api_students), 1)
            quiz_student = quiz_api_students[0]
            quiz_exam = self.quiz.query(self.quiz.classes.quiz_data_exammodel).filter_by(exam_id=exam.id).all()
            self.assertEqual(len(quiz_exam), 1)
            quiz_exam = quiz_exam[0]
            self.assertEqual(quiz_exam.student_id, quiz_student.id)

if __name__ == "__main__":
    unittest.main()
