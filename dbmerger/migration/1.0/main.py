import hashlib
import base64
import argparse
from datetime import datetime
from core import Migration
from legacy import accounts, legacy
from current import quiz_api
from sqlalchemy.exc import IntegrityError

QUIZ_MAP = [
    {"name": "B 2016",                           'id': 50, 'questions': 40, 'prefix': 'B'},
    {"name": "CQC",                              'id': 2},
    {"name": "AM 2016",                          'id': 4, 'questions': 30, 'prefix': 'AM'},
    {"name": "CDE C1 – C1E",                     'id': 5, 'questions': 20},
    {"name": "CDE C1 – C1E codice unionale 97",  'id': 6, 'questions': 10},
    {"name": "CDE C – CE",                       'id': 7, 'questions': 40},
    {"name": "CDE C – CE già C1",                'id': 8, 'questions': 20},
    {"name": "CDE D1 – D1E",                     'id': 9, 'questions': 20},
    {"name": "CDE D – DE",                       'id': 10, 'questions': 40},
    {"name": "CDE D – DE già D1",                'id': 11, 'questions': 20},
    {"name": "Revisioni AM",                     'id': 60, 'questions': 20},
    {"name": "Revisioni A1 A2 A B1 B BE",        'id': 61, 'questions': 30},
    {"name": "Revisioni C1 (97), C1E (97)",      'id': 62, 'questions': 30},
    {"name": "Revisioni C1 C1E C CE",            'id': 63, 'questions': 30},
    {"name": "Revisioni D1 D1E D DE",            'id': 64, 'questions': 30},
    {"name": "Revisioni CQC merci",              'id': 65, 'questions': 40},
    {"name": "Revisioni CQC persone",            'id': 66, 'questions': 40}
]
QUIZ_TYPES = []
QUIZ_TYPES += list(range(4, 12)) + list(range(60, 65)) + [50]

class UserMigration(object):

    def _get_missing_companies(self):
        """Get list of missing records from quiz_api.
        """
        QuizApiUser = quiz_api.classes.user_user
        Company = accounts.classes.companies
        User = legacy.classes.users
        ids = [u.school_id for u in legacy.session.query(User).all()]

        schools = [s.email for s in quiz_api.session.query(QuizApiUser).filter(QuizApiUser.email != '').all()]
        companies = [c.mail for c in accounts.session.query(Company).filter(Company.mail != '', Company.id.in_(ids)).all()]
        # missing are schools existing in accounts and missing from quiz_api
        missing = list(sorted(set(companies) - set(schools)))
        added = list(sorted(set(schools) - set(companies)))
        records = [u for email in missing for u in accounts.session.query(Company).filter_by(mail=email)]
        return records

    def _get_companies(self):
        """Return the list of companies existing in quiz legacy.
        """
        Company = accounts.classes.companies
        User = legacy.classes.users

        ids = [u.school_id for u in legacy.session.query(User).all()]
        companies = accounts.session.query(Company).filter(Company.id.in_(ids)).all()
        return companies

    def create_companies(self):
        """Create companies from list of companies in quiz_api.
            - get list of ids in legacy_quiz.users
            - get list of companies from accounts.companies (where id exists in legacy_quiz.users)
            - create a school in quiz_api
        """
        companies = self._get_companies()

        QuizApiUser = quiz_api.classes.user_user
        Company = accounts.classes.companies
        ids = [c.id for c in companies]

        for candidate in accounts.session.query(Company).filter(Company.id.in_(ids)).all():
            hashed = hashlib.pbkdf2_hmac('sha256', candidate.name.encode(), b'salt', 12000)
            password = base64.b64encode(hashed).decode('ascii').strip()
            school = QuizApiUser(is_superuser=False,
                        email=candidate.mail,
                        password=password, # remove this
                        is_staff=False,
                        is_active=True,
                        date_joined=datetime.today(),
                        is_school=True,
                        school_name=candidate.name,
                        is_student=False,
                        facebook_login_present=False,
                        google_login_present=False,
                        is_teacher=False,
                        user_tracking=False
                    )
            quiz_api.session.add(school)
            try:
                quiz_api.session.commit()
            except IntegrityError as e:
                print(e)
                quiz_api.session.rollback()
                continue

    def _get_students(self, company):
        """Get students of a company.
        Using school_id (legacy_quiz.users), look for student with company_id = company.id in accounts.companies
        """
        Account = accounts.classes.customers
        User = legacy.classes.users
        ids = [u.id for u in accounts.session.query(Account).all()]

        records = legacy.session.query(User).filter(
            User.school_id == company.id,
            User.id.in_(ids)).all()
        return records

    def _create_students(self, company):
        """Given a company (accounts.companies)
        create students in quiz_api
        """
        QuizApiUser = quiz_api.classes.user_user
        User = legacy.classes.users
        # make sure the parent exists in quiz_api
        parent = quiz_api.session.query(QuizApiUser).filter(QuizApiUser.email == company.mail, QuizApiUser.is_school == True).all()
        if not parent or len(parent) != 1:
            return False

        students = self._get_students(company)
        ids = [u.id for u in students]
        students = accounts.session.query(accounts.classes.customers).filter(
            accounts.classes.customers.id.in_(ids)
        ).all()
        for student in students:
            hashed = hashlib.pbkdf2_hmac('sha256', b'student', b'salt', 12000)
            password = base64.b64encode(hashed).decode('ascii').strip()
            active = student.is_student
            record = QuizApiUser(
                    email=student.email,
                    password=password, # remove this
                    is_staff=False,
                    is_superuser=False,
                    first_name=student.name,
                    last_name=student.surname,
                    parent_id=parent[0].id,
                    is_active=active,
                    date_joined=datetime.today(),
                    is_school=False,
                    username=student.login,
                    is_student=student.is_student,
                    facebook_login_present=False,
                    google_login_present=False,
                    is_teacher=False,
                    user_tracking=False
            )
            quiz_api.session.add(record)
            try:
                quiz_api.session.commit()
            except IntegrityError as e:
                print(e)
                quiz_api.session.rollback()
                continue

    @classmethod
    def all_students(cls):
        """Return all students,
        records presents in legacy.users and accounts.customers
        """
        Account = accounts.classes.customers
        User = legacy.classes.users
        ids = [u.id for u in accounts.session.query(Account).all()]

        records = legacy.session.query(User).filter(
            User.id.in_(ids)).all()
        return records

    def create_students(self):
        companies = self._get_companies()
        for company in companies:
            self._create_students(company)

    @classmethod
    def check_parent(cls, student, quiz_students, quiz_schools):
        """Checks if the parent child relationship is ok.
        Takes a student object (legacy.users)
        """
        School = accounts.classes.companies
        Student = accounts.classes.customers
        QuizApiUser = quiz_api.classes.user_user

        school = accounts.session.query(School).filter(
            School.id == student.school_id,
            School.name.in_(quiz_schools)
        ).all()
        account = accounts.session.query(Student).filter(
            Student.id == student.id,
            Student.login.in_(quiz_students)
        ).all()

        if len(account) != 1:
            return

        account = account[0]
        quiz_api_student = quiz_api.session.query(QuizApiUser).filter_by(username=account.login).all()
        if len(school) != 1 or len(quiz_api_student) != 1:
            print(f'Error: school {school}, quiz_api_student {quiz_api_student}')
            return
        quiz_api_student = quiz_api_student[0]
        school = school[0]
        quiz_api_school = quiz_api.session.query(QuizApiUser).filter_by(id=quiz_api_student.parent_id).first()
        assert quiz_api_school.school_name == school.name


def get_exam_length(quiz_api_quiz):
    """Return number of question for a quiz
    """
    QuizApiExamSet = quiz_api.classes.quiz_data_examquestionset
    QuizApiExamSequence = quiz_api.classes.quiz_data_examsetquestionsequence
    quiz_api_examset = quiz_api.session.query(QuizApiExamSet).filter_by(quiz_id=quiz_api_quiz.id).first()
    quiz_api_examsequence = quiz_api.session.query(QuizApiExamSequence).filter_by(exam_set_id=quiz_api_examset).all()

    return len(quiz_api_examsequence)


def create_exams(limit=None, quizes=[]):
    """Create exams for students.
    Loop through every exam in legacy_quiz.exams,
    get the the user_id, look for the login in accounts.customers,
    make sure the user exists in quiz_api,
    get the quiz from quiz_api.quiz_data_quizmodel using QUIZ_MAP (legacy.quiz_type),
    create an exam using data from the quiz_api.quiz_data_quizmodel and legacy.exam
    """
    results = []
    if not quizes:
        quizes = QUIZ_TYPES

    Account = accounts.classes.customers
    LegacyExam = legacy.classes.exams
    QuizApiQuiz = quiz_api.classes.quiz_data_quizmodel
    QuizApiUser = quiz_api.classes.user_user
    QuizApiExam = quiz_api.classes.quiz_data_exammodel
    QuizApiExamSet = quiz_api.classes.quiz_data_examquestionset
    QuizApiExamSequence = quiz_api.classes.quiz_data_examsetquestionsequence

    exams = legacy.session.query(LegacyExam).filter(
        LegacyExam.quiz_type.in_(quizes),
        LegacyExam.user_id.in_(
            [u.id for u in accounts.session.query(accounts.classes.customers).all()]
        )
    )
    if limit:
        exams = exams.limit(limit)
    else:
        exams = exams.all()

    for exam in exams:
        obtained = 0
        students = []
        # get the account info using user_id
        students = accounts.session.query(Account).filter(
            Account.id == exam.user_id,
            Account.is_student == True
        ).all()
        if len(students) != 1:
            continue
        student = students[0]
        # find the student in quiz_api using student.login (unique)
        quiz_api_students = quiz_api.session.query(QuizApiUser).filter(
            QuizApiUser.username == student.login,
            QuizApiUser.is_student == True
        ).all()
        if len(quiz_api_students) != 1:
            continue
        quiz_api_student = quiz_api_students[0]
        for obj in QUIZ_MAP:
            if obj['id'] == exam.quiz_type:
                quiz_dict = obj
        # print(len(quiz_api_examsequence))
        quiz_api_quiz = quiz_api.session.query(QuizApiQuiz).filter_by(title=quiz_dict['name']).first()

        obtained = quiz_dict['questions'] - exam.err_count
        passed = "Failed"
        if obtained >= quiz_api_quiz.passing_marks:
            passed = "Passed"

        quiz_exam = QuizApiExam(
            created_at=exam.start_time,
            updated_at=exam.start_time,
            # do not change exam_id !
            exam_id=exam.id,
            result=passed,
            passing_marks=quiz_api_quiz.passing_marks,
            obtained_marks=obtained,
            student_id=quiz_api_student.id,
            quiz_id=quiz_api_quiz.id,
            is_over=True
        )
        quiz_api.session.add(quiz_exam)

        try:
            quiz_api.session.commit()
        except IntegrityError as e:
            print(e)
            quiz_api.session.rollback()
            continue

        results.append(quiz_exam.exam_id)

    return results


def create_answers(answers=[], exam=None):
    """Takes a quiz_api exam and creates its answers using legacy_quiz.exam_answers
    """
    def get_question(question):
        """Return the matching question from quiz_api
        """
        Question = quiz_api.classes.quiz_data_questionmodel
        # make sure the question matches (text matches and category matches)
        topic = legacy.session.query(legacy.classes.topics).filter(
            legacy.classes.topics.id == question.topic_id,
            legacy.classes.topics.quiz_type == question.quiz_type
        ).all()
        if len(topic) != 1:
            print(f'topic : {topic}')
            return
        topic = topic[0]
        quiz_name, name = get_quiz_lookup(topic.quiz_type)
        category = get_category(topic, prefix=name)
        if not category:
            print(f'category : {category}')
            return
        q = quiz_api.session.query(Question).filter(
            Question.text == question.text,
            Question.category_id == category.id
        ).all()
        if len(q) != 1:
            print( f'question : {question.text, topic.id}, {q}')
            if len(q) == 0:
                return
        q = q[0]
        return q

    def get_student(answer):
        """Return the matching student from quiz_api.
        """
        exam = legacy.session.query(legacy.classes.exams).filter_by(id=answer.exam_id).all()
        if len(exam) != 1:
            print('err')
            return
        exam = exam[0]
        # FIXME: this is redundunt, exam.id is quiz_exam.exam_id, look directly by qnswer.exam_id
        quiz_exam = quiz_api.session.query(quiz_api.classes.quiz_data_exammodel).filter_by(exam_id=exam.id).all()
        if len(quiz_exam) != 1:
            print(f'quiz_exam : {quiz_exam}')
            return
        quiz_exam = quiz_exam[0]
        user = quiz_api.session.query(quiz_api.classes.user_user).filter_by(id=quiz_exam.student_id).all()
        if len(user) != 1:
            print(f'user : {user}')
            return
        return user[0], quiz_exam

    ExamAnswers = legacy.classes.exam_answers
    Answers = legacy.classes.answers
    QuizApiAnswer = quiz_api.classes.quiz_data_answermodel
    if exam and not answers:
        answers = legacy.session.query(ExamAnswers).filter_by(exam_id=exam.exam_id).all()

    for answer in answers:
        question = legacy.session.query(legacy.classes.questions).filter_by(
            id=answer.question_id,
            quiz_type=answer.quiz_type
        ).all()
        if len(question) != 1:
            continue
        question = question[0]
        q = get_question(question)
        if exam:
            u, quiz_api_exam = get_student(answer)
            created_at = datetime.today()
        else:
            account = accounts.session.query(accounts.classes.customers).filter_by(id=answer.user_id).all()
            if len(account) != 1:
                continue
            account = account[0]
            u = quiz_api.session.query(quiz_api.classes.user_user).filter_by(username=account.login).all()
            if len(u) != 1:
                continue
            u = u[0]
            if hasattr(answer, 'ts') and answer.ts is not None:
                created_at = answer.ts
            else:
                created_at = datetime.today()

        quiz_api_a = quiz_api.classes.quiz_data_answermodel(
            is_correct=answer.is_correct,
            question_id=q.id,
            student_id=u.id,
            updated_at=created_at,
            created_at=created_at
        )
        if answer.is_correct == 1:
            given = q.right_answers
            for c in ['[', '\"', ']']:
                given = given.replace(c, '')
        elif answer.is_correct == 0:
            given = q.right_answers
            for c in ['[', '\"', ']']:
                given = given.replace(c, '')
            if given == '1': given = 2
            elif given == '2': given = 1
        quiz_api_a.given_answer = given
        if exam:
            quiz_api_a.exam_id = quiz_api_exam.exam_id

        quiz_api.session.add(quiz_api_a)
        quiz_api.session.commit()


def get_quiz_lookup(quiz_type):
    quiz_name = None
    for obj in QUIZ_MAP:
        if obj['id'] == quiz_type:
            name = obj['name']
            if obj.get('prefix'):
                quiz_name = obj['name']
                name = obj['prefix']
    return quiz_name, name


def create_categories(quiz_type):
    """Creates categories in quiz_api.
    """
    missing = []
    quiz_name, name = get_quiz_lookup(quiz_type)
    lookup = quiz_name or name
    topics = legacy.session.query(legacy.classes.topics).filter_by(quiz_type=quiz_type).all()
    chapters = legacy.session.query(legacy.classes.chapters).filter_by(quiz_type=quiz_type).all()
    quiz = quiz_api.session.query(quiz_api.classes.quiz_data_quizmodel).filter_by(title=lookup).all()[0]

    for chapter in chapters:
        category = get_category(chapter, prefix=name)
        if not category:
            category = quiz_api.classes.quiz_data_questioncategorymodel(
                title=f'({name}) {chapter.text}',
                created_at=datetime.today(),
                updated_at=datetime.today(),
                color='None'
            )
            quiz_api.session.add(category)
            quiz_api.session.commit()
        try:
            quiz_category_relation = quiz_api.classes.quiz_data_quizmodel_categories(
                quizmodel_id=quiz.id,
                questioncategorymodel_id=category.id
            )
            quiz_api.session.add(quiz_category_relation)
            quiz_api.session.commit()
        except IntegrityError:
            quiz_api.session.rollback()
            pass

        # create child categories
        topics = legacy.session.query(legacy.classes.topics).filter(
            legacy.classes.topics.chapter_id == chapter.id,
            legacy.classes.topics.quiz_type == quiz_type
        ).all()
        for topic in topics:
            child = quiz_api.session.query(quiz_api.classes.quiz_data_questioncategorymodel).filter(
                quiz_api.classes.quiz_data_questioncategorymodel.title == f'({name}) {topic.text}'
            ).all()
            if not child:
                missing.append(topic.text)
                quiz_child = quiz_api.classes.quiz_data_questioncategorymodel(
                    title=f'({name}) {topic.text}',
                    created_at=datetime.today(),
                    updated_at=datetime.today(),
                    color='None',
                    category_id=category.id
                )
                quiz_api.session.add(quiz_child)
                quiz_api.session.commit()

    return missing


def get_category(topic, prefix='', parent_id=None):
    """Return quiz_api category.
    """
    lookup = f"({prefix}) {topic.text}"
    category = quiz_api.session.query(quiz_api.classes.quiz_data_questioncategorymodel).filter(
        quiz_api.classes.quiz_data_questioncategorymodel.title.like(lookup),
        # quiz_api.classes.quiz_data_questioncategorymodel.category_id == parent_id
    ).all()
    if len(category) != 1:
        return None
    category = category[0]
    return category

def create_questions(quiz_type):
    """Creates questions in quiz_api.
    """
    missing_questions = []
    quiz_name, name = get_quiz_lookup(quiz_type)
    lookup = quiz_name or name
    chapters = legacy.session.query(legacy.classes.chapters).filter_by(quiz_type=quiz_type).all()
    quiz = quiz_api.session.query(quiz_api.classes.quiz_data_quizmodel).filter_by(title=lookup).all()[0]
    topics = legacy.session.query(legacy.classes.topics).filter_by(quiz_type=quiz_type).all()

    for topic in topics:
        category = get_category(topic, prefix=name)
        questions = legacy.session.query(legacy.classes.questions).filter(
            legacy.classes.questions.quiz_type == quiz_type,
            legacy.classes.questions.topic_id == topic.id
        ).all()
        # create quiz_api question
        for q in questions:
            found = quiz_api.session.query(quiz_api.classes.quiz_data_questionmodel).filter(
                quiz_api.classes.quiz_data_questionmodel.category_id == category.id,
                quiz_api.classes.quiz_data_questionmodel.text == q.text
            ).all()
            if not found:
                missing_questions.append(q.text)
                # Create the question in quiz_api
                quiz_api_q = quiz_api.classes.quiz_data_questionmodel(
                    created_at=datetime.today(),
                    updated_at=datetime.today(),
                    text=q.text,
                    image=q.image,
                    answer_choices='{"1": "V", "2": "F"}',
                    right_answers=f"[\"{q.answer}\"]",
                    category_id=category.id,
                    explanation=q.explanation
                )
                quiz_api.session.add(quiz_api_q)
                quiz_api.session.commit()

    return missing_questions



if __name__ == '__main__':
    migrator = UserMigration()
    parser = argparse.ArgumentParser(description='Migration')
    parser.add_argument('target')
    args = parser.parse_args()
    target = args.target

    if target == 'schools':
        quiz_schools = [s.school_name for s in quiz_api.session.query(quiz_api.classes.user_user).filter_by(is_school=True)]
        print('creating companies ...')
        migrator.create_companies()
    elif target == 'students':
        quiz_students = [u.username for u in quiz_api.session.query(quiz_api.classes.user_user).filter_by(is_student=True)]
        print('creating students ...')
        migrator.create_students()
    elif target == 'categories':
        print('creating categories ...')
        for q in QUIZ_TYPES:
            create_categories(q)
    elif target == 'questions':
        print('creating questions ...')
        for q in QUIZ_TYPES:
            create_questions(q)
    elif target == 'exams':
        print('creating exams ...')
        results = []
        for i in QUIZ_TYPES:
            results += create_exams(quizes=[i])
        print('creating exam answers ...')
        exams = quiz_api.session.query(quiz_api.classes.quiz_data_exammodel).filter(
            quiz_api.classes.quiz_data_exammodel.exam_id.in_(results)
        ).all()
        for exam in exams:
            create_answers(exam=exam)
    elif target == 'quiz_answers':
        print('creating quiz_anwsers ...')
        for q in QUIZ_TYPES:
            answers = legacy.session.query(legacy.classes.quiz_answers).filter_by(quiz_type=q).limit(2)
            create_answers(answers=answers)
    elif target == 'answers':
        print('creating answers ...')
        for q in QUIZ_TYPES:
            answers = legacy.session.query(legacy.classes.answers).filter_by(quiz_type=q).all()
            create_answers(answers=answers)