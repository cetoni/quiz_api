from datetime import datetime

from session import declarative_base
from sqlalchemy import Column, Integer, String, TEXT, DateTime

Base = declarative_base()


class QuizQuestion(Base):
    __tablename__ = "quiz_data_questionmodel"

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    text = Column("text", TEXT, nullable=False)
    image = Column("image", String(200), nullable=True)
    answer_choices = Column("answer_choices", TEXT, nullable=False)
    right_answers = Column("right_answers", String(100), nullable=False)
    category_id = Column("category_id", Integer, nullable=True)
    explanation = Column("explanation", TEXT, nullable=True)
    created_at = Column(
        "created_at", DateTime, nullable=False, default=datetime.today()
    )
    updated_at = Column(
        "updated_at", DateTime, nullable=False, default=datetime.today()
    )

class QuestionCategory(Base):
    __tablename__ = "quiz_data_questioncategorymodel"

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    title = Column("title", String(500), nullable=False)
    category_id = Column("category_id", Integer, nullable=True)
    description = Column("description", TEXT, nullable=True)
    level = Column("level", String(20), nullable=False)
    rank = Column("rank", Integer, nullable=False)
    color = Column("color", String(10), nullable=False)
    created_at = Column(
        "created_at", DateTime, nullable=False, default=datetime.today()
    )
    updated_at = Column(
        "updated_at", DateTime, nullable=False, default=datetime.today()
    )
