from abc import ABC, abstractmethod
import itertools
import json

from session import Session
from parser import data_from_excel
from models import QuizQuestion


class Seeder(ABC):
    def __init__(self):
        self.session = Session().get()

    @abstractmethod
    def run(self) -> None:
        pass


def seeder(seeder: Seeder):
    seeder.run()


class QuestionSeeder(Seeder):
    def run(self) -> None:
        self.session.bulk_save_objects(
            [
                QuizQuestion(
                    text=str(data['question']),
                    image=data["image"],
                    answer_choices='{"1": "V", "2": "F"}',
                    right_answers=json.dumps(data['answer']),
                    category_id=0,
                )
                for data in data_from_excel()
            ]
        )
        self.session.commit()


if __name__ == "__main__":
    seeder(QuestionSeeder())
