from configparser import ConfigParser
from pathlib import Path

import unittest
import os


class TestConfig(unittest.TestCase):
    _config = ConfigParser()
    _config.read(os.path.join(os.path.dirname(os.getcwd()), "config.ini"))

    def test_config_file_user(self):
        self.assertEqual("root", self._config["database"]["user"])

    def test_config_db_name(self):
        self.assertEqual("quiz", self._config["database"]["db"])

    def test_config_host_name(self):
        self.assertEqual("localhost", self._config["database"]["host"])


if __name__ == "__main__":
    unittest.main()
