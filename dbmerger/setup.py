from setuptools import setup

setup(
    python_requires=">=3.6.0",
    install_requires=[
        "alembic==1.4.0",
        "black==19.10b0",
        "mysql-connector-python==8.0.19",
        "PyMySQL==0.9.3",
        "SQLAlchemy==1.3.13",
    ],
    include_package_data=True,
    py_modules=["migration"],
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],
)
