# pull official base image
FROM python:3.7-alpine

# set work directory
#WORKDIR /usr/src/project

RUN mkdir /src
WORKDIR /src
RUN mkdir assets
RUN mkdir media
RUN mkdir static

# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV HOME /root

# install psycopg2
RUN apk update \
    && apk add --virtual build-deps python3-dev musl-dev \
    && apk add jpeg-dev zlib-dev  \
    && apk add --update g++ libxslt-dev libc-dev \
    && apk add --update gcc libffi-dev openssl-dev cargo git

RUN apk add mysql mysql-client libffi-dev openssl-dev mariadb-connector-c-dev autoconf automake libtool nasm\
    && apk del build-deps \
    && rm -fr /var/cache/apk

# install dependencies
ADD ./src /src
RUN pip install --upgrade pip
RUN pip install -r requirements/base.txt

#https://github.com/infoscout/django-cache-utils/issues/12
RUN pip install git+https://github.com/infoscout/django-cache-utils.git@2.0.0

# RUN pip install git+https://github.com/areski/django-admin-tools-stats
# run entrypoint.sh
CMD python manage.py collectstatic --no-input; python manage.py migrate; gunicorn -c python:quiz_api.config quiz_api.wsgi