build:
	docker-compose build

up:
	docker-compose up -d

up-non-daemon:
	docker-compose up

start:
	docker-compose start

stop:
	docker-compose stop

restart:
	docker-compose stop && docker-compose start

shell-nginx:
	docker exec -ti nginx_taxi /bin/sh

shell-web:
	docker exec -ti django_taxi /bin/sh

shell-db:
	docker exec -ti postgres_taxi /bin/sh

log-nginx:
	docker-compose logs nginx  

log-web:
	docker-compose logs web  

log-db:
	docker-compose logs db

collectstatic:
	docker exec django_taxi /bin/sh -c "python manage.py collectstatic --noinput"
